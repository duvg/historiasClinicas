<?php
/**
 * Created by Duviel Garcia.
 * Date: 15/11/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Afiliado;
use AppBundle\Form\AfiliadoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;


/**
 * Class DefaultController
 * @Route("/admin/afiliados")
 */
class AfiliadosController extends Controller
{
    /**
     * @Route("/{tipo}", defaults={"tipo" = "afiliado"}, name="listar_afiliados")
     */
    public function indexAction(Request $request, $tipo = 'afiliado')
    {

        //return all clients
        $em = $this->getDoctrine()->getManager();
        $afiliado = $em->getRepository('AppBundle:Afiliado')->findAfiliadosEnabled();

        return $this->render('AppBundle:Afiliados:index.html.twig', array(
            'afiliados' => $afiliado,
            'tipo' => $tipo
        ));

    }

    /**
     * @param Request $request
     * @Route("/crear/{tipo}/", defaults={"tipo" = "afiliado"}, name="crear_afiliado")
     */
    public function afiliadoCrearAction(Request $request, $tipo = 'afiliado'){
        $tipo_persona = $tipo;
        $afiliado = new Afiliado();
        $form = $this->createForm(AfiliadoType::class, $afiliado);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            $date = new \DateTime($request->request->get('fechaNacimiento'));
            $afiliado->setFechaNacimiento($date);
            $em = $this->getDoctrine()->getManager();
            $em->persist($afiliado);
            $em->flush();

            $this->addFlash('success-save', 'Afiliado creado satisfactoriamente!');

            return $this->redirectToRoute('listar_afiliados');
        }

        return $this->render('AppBundle:Afiliados:form.html.twig', array(
            'form' => $form->createView(),
            'tipo' => $tipo_persona
        ));


    }

    /**
     * @param Request $request
     * @Route("/editar/{id}/{tipo}", defaults={"tipo" = "afiliado"},name="editar_afiliado")
     */
    public function editarAfiliadoAction(Request $request, $id, $tipo = 'afiliado')
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $afiliadoRepo = $em->getRepository(Afiliado::class)->find($id);
        if(!$afiliadoRepo){
            throw $this->createNotFoundException("No existe ningun afiliado relacionado con el id ".$id);
        }

        $form = $this->createForm(AfiliadoType::class, $afiliadoRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {

            $em->flush();

            $this->addFlash('success_update', 'Afiliado actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_afiliados');
        }


        return $this->render('AppBundle:Afiliados:form.html.twig', array(
            'afiliado' => $afiliadoRepo,
            'form' => $form->createView(),
            'tipo' => $tipo
        ));

    }


    /**
     * Eliminar el afiliado, cambia el estado del afiliado para deshabilitarlo
     * @Route("/eliminar/{id}", name="eliminar_afiliado")
     */
    public function eliminarAfiliado($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $afiliadoRepo = $em->getRepository(Afiliado::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$afiliadoRepo){
            throw $this->createNotFoundException("No existe ningun afiliado relacionado con el id ".$id);
        }

        $afiliadoRepo->setEnabled(false);

        $em->flush();
        return $this->redirectToRoute('listar_afiliados');

    }

    /**
     * @Route("/afiliacion/{id}", name="imprimir_afiliado")
     */
    public function imprimirAfiliacionAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $afiliadoRepo = $em->getRepository(Afiliado::class)->find($id);
        if(!$afiliadoRepo){
            throw $this->createNotFoundException("No existe ningún afiliado relacionado con el id ".$id);
        }
        $html = $this->renderView('AppBundle:Afiliados:afiliadopdf.html.twig', array(
            'afiliado' => $afiliadoRepo,
            'rootDir' => $this->get('kernel')->getRootDir().'/..'
        ));

        /*return $this->render('AppBundle:Afiliados:afiliadopdf.html.twig', array(
            'afiliado'=>$afiliadoRepo
        ));*/
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            $afiliadoRepo->getNdocumento().'.pdf'
        );
    }
}