<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Paciente;
use AppBundle\Entity\Procedimiento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\Procedimiento1Type;

/**
 * Class AtenderProcedimientosController
 * @Route("/admin/atender")
 */
class AtenderProcedimientosController extends Controller
{
    /**
     * @param $name
     * @Route("/", name="solicitudes_pendietes")
     */
    public function indexAction()
    {
        //obtenemos el id del usuario
        $idUser = $this->getUser()->getId();
        $userTr = $this->getUser()->getTipoTratamiento();
        $em = $this->getDoctrine()->getManager();
        if($userTr == null){
            $pacientes = $em->getRepository(Paciente::class)->findPacientesTipoProcedimientoAll();

        }else{
            $pacientes = $em->getRepository(Paciente::class)->findPacientesTipoProcedimiento($idUser);
            return new JsonResponse("ok no");
        }

        return $this->render("AppBundle:Pacientes:pacientesEspera.html.twig", array(
            "pacientes" => $pacientes
        ));

    }

    /**
     * @param $id
     * @Route("/procedimientos/{id}", name="procedimientos_paciente_espera")
     */
    public function procedimientosPaciente($id){

        $em = $this->getDoctrine()->getManager();
        $procedimientos = $em->getRepository(Procedimiento::class)->findProcedimientosPaciente($id);

        
        if(count($procedimientos) == 0){
            return $this->redirectToRoute('solicitudes_pendietes');
        }

        return $this->render("AppBundle:Pacientes:procedimientos.html.twig", array(
            'idPaciente' => $procedimientos[0]->getPaciente()->getDocumento(),
            'procedimientos' => $procedimientos
        ));
    }

    /**
     * @Route("/procedimiento/anteder/{id}", name="atender_procedimiento")
     */
    public function atenderProcedimientoAction(Request $request, $id){
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $procedimientoRepo = $em->getRepository(Procedimiento::class)->findOneBy(array('id' => $id));


        if(!$procedimientoRepo){
            throw $this->createNotFoundException("No existe ningun procedimiento relacionado con el id ".$id);
        }

        $form = $this->createForm(Procedimiento1Type::class, $procedimientoRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $procedimientoRepo->setEnabled(false);
            $em->flush();

            $this->addFlash('success_update', 'Procedimiento atendido satisfactoriamente!');


            return $this->redirect($this->generateUrl('procedimientos_paciente_espera', array(
                'id' => $procedimientoRepo->getPaciente()->getId(),
            )));
        }


        return $this->render('AppBundle:Pacientes:formProcedimiento.html.twig', array(
            'procedimiento' => $procedimientoRepo,
            'form' => $form->createView()
        ));
    }
}
