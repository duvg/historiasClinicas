<?php
/**
 * Created by Duviel Garcia.
 * Date: 15/11/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Cliente;
use AppBundle\Form\ClienteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class DefaultController
 * @Route("/clientes")
 */
class ClientesController extends Controller
{
    /**
     * @Route("/", name="listar_cliente")
     */
    public function indexAction(Request $request)
    {
        //return all clients
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository('AppBundle:Cliente')->findClientesEnabled();

        return $this->render('AppBundle:Clientes:index.html.twig', array(
            'clientes' => $clientes
        ));

    }

    /**
     * @param Request $request
     * @Route("/crear", name="crear_cliente")
     */
    public function clienteCrearAction(Request $request){

        $cliente = new Cliente();
        $form = $this->createForm(ClienteType::class, $cliente);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            $date = new \DateTime($request->request->get('fechaNacimiento'));
            $cliente->setFechaNacimiento($date);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();

            $this->addFlash('success-save', 'Cliente creado satisfactoriamente!');

            return $this->redirectToRoute('listar_cliente');
        }

        return $this->render('AppBundle:Clientes:form.html.twig', array(
            'form' => $form->createView()
        ));


    }

    /**
     * @param Request $request
     * @Route("/editar/{id}", name="editar_cliente")
     */
    public function editarClienteAction(Request $request, $id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $clienteRepo = $em->getRepository(Cliente::class)->find($id);
        if(!$clienteRepo){
            throw $this->createNotFoundException("No existe ningun cliente relacionado con el id ".$id);
        }

        $form = $this->createForm(ClienteType::class, $clienteRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {

            $em->flush();

            $this->addFlash('success_update', 'Cliente actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_cliente');
        }


        return $this->render('AppBundle:Clientes:form.html.twig', array(
            'cliente' => $clienteRepo,
            'form' => $form->createView()
        ));

    }


    /**
     * Eliminar el cliente, cambia el estado del cliente para deshabilitarlo
     * @Route("/eliminar/{id}", name="eliminar_cliente")
     */
    public function eliminarCliente($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $clienteRepo = $em->getRepository(Cliente::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$clienteRepo){
            throw $this->createNotFoundException("No existe ningun cliente relacionado con el id ".$id);
        }

        $clienteRepo->setEnabled(false);

        $em->flush();
        return $this->redirectToRoute('listar_cliente');

    }
}