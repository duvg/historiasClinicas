<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class ConsentimientosController
 * @Route("/admin/consentimientos")
 */
class ConsentimientosController extends Controller
{
    /**
     * @param $name
     * @Route("/bioplasma")
     */
    public function indexAction()
    {

        $html = $this->renderView('AppBundle:Consentimientos:CSBiosplasma.html.twig', array(
            'rootDir' => $this->get('kernel')->getRootDir().'/..'
        ));

        //return $this->render('AppBundle:Consentimientos:CSBiosplasma.html.twig');



        /*$footer = $this->renderView('AcmeBundle:Pdf:footer.html.twig', array(
            //Variables for the template
        ));*/

        $footer = "pie de pagina XD XD";

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'page-size' => 'A4',
                'footer-html' => $footer,
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'documen.pdf'
        );
    }
}
