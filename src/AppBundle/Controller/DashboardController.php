<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DashboardController
 * @Route("/admin")
 */
class DashboardController extends Controller
{
    /**
     * @Route("/dashboard/", name="dashboard")
     */
    public function indexAction()
    {
        return $this->redirectToRoute("listar_solicitudes");
    }
}
