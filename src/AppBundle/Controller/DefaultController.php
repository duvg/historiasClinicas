<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Encuesta;
use AppBundle\Form\EncuestaType;
use AppBundle\Form\ContactType;
use AppBundle\Entity\Cliente;
use AppBundle\Form\ClienteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class DefaultController
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="login_home")
     */
    /*public function indexAction()
    {
        return $this->redirectToRoute('fos_user_security_login'); //for logout  fos_user_security_logout
    }*/

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $noticias = $em->getRepository("AppBundle:Post")->findPostsEnabled();

        return $this->render('default/index.html.twig', array('noticias' => $noticias));
    }

    /**
     * @Route("/noticias", name="noticias")
     */
    public function noticiasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $noticias = $em->getRepository("AppBundle:Post")->findPostsEnabled();

        return $this->render('default/noticias.html.twig', array('noticias' => $noticias));
    }

    /**
     * @Route("/quienessomos", name="quienessomos")
     */
    public  function quinessomosAction()
    {
        return $this->render('default/misionvision.html.twig');
    }

    /**
     * @Route("/servicios", name="servicios")
     */
    public function serviciosAction()
    {
        return $this->render('default/servicios.html.twig');
    }

    /**
     * @Route("/productos", name="productos")
     */
    public function productosAction()
    {
        return $this->render('default/productos.html.twig');
    }

    /**
     * @Route("/construccion", name="construccion")
     */
    public  function construccionAction()
    {
        return $this->render('default/construccion.html.twig');
    }

    /**
     * @Route("/contactar", name="contactar")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if($request->isMethod('POST'))
        {
            $form->handleRequest($request);

            if($form->isValid())
            {
                /* send mail by smtp
                $mailer = $this->get('mailer');
                $message = $mailer->createMessage()
                    ->setSubject('Nuevo mensaje de '.$form->get('nombre')->getData())
                    ->setFrom("system@casamayorga.com")
                    ->setTo('contactenos@casamayorga.com')
                    ->setBody(
                        $this->renderView(
                            'default/mail/contactar.html.twig',
                            array(
                                'nombre' => $request->get('nombre'),
                                'apellidos' => $request->get('epallidos'),
                                'email' => $request->get('email'),
                                'telefono' => $request->get('telefono'),
                                'mensaje' => $request->get('mensaje')
                            )
                        )
                    );

                $mailer->send($message);
                */

                $para      = 'contactenos@casamayorga.com';
                $titulo    = 'Nuevo mensaje de contacto - '.$request->get('nombre');
                $mensaje   = $request->get('nombre')." ".
                    $request->get('epallidos')."\nTelefono: ".$request->get('telefono').
                    "\nEmail: ".$request->get('email')."\n \n".
                    $request->get('mensaje');


                $cabeceras = 'From: webmaster@casamayorga.com' . "\r\n" .
                    'Reply-To: ' .$request->get('email'). "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                mail($para, $titulo, $mensaje, $cabeceras);

                return $this->redirect($this->generateUrl('success_contact'));

            }
        }


     

        return $this->render('default/contactar.html.twig',  array('form' => $form->createView()));
    }

    /**
     * @Route("/enviado", name="success_contact")
     */
    public function successAction()
    {
        return $this->render('default/success.html.twig');
    }

    /**
     * @Route("/encuesta", name="encuesta_satisfaccion")
     */
    public function encuestaAction(Request $request)
    {
        $encuesta = new Encuesta();
        $form = $this->createForm(EncuestaType::class, $encuesta);

        $form->handleRequest($request);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($encuesta);
            $em->flush();

            return $this->render('default/success.html.twig', array('success' => 'Gracias por evaluar nuestro servicio!'));
        }

        return $this->render('application/encuesta.html.twig', array('form' => $form->createView()));



    }





}
