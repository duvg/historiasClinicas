<?php
/**
 * Created by PhpStorm.
 * User: duviel
 * Date: 18/11/17
 * Time: 7:27 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Factura;
use AppBundle\Form\FacturaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FacturasController
 * @Route("/facturas")
 */
class FacturasController extends Controller
{
    /**
     * @Route("/", name="listado_facturas")
     */
    public function indexAction(){

    }


    /**
     * @Route("/nueva", name="nueva_factura")
     */
    public function nuevaAction(Request $request)
    {
        $factura = new Factura();
        $form = $this->createForm(FacturaType::class, $factura);

        $form->handleRequest($request);

        if($form->isValid()){

        }

        return $this->render('AppBundle:Facturas:crear.html.twig', array(
            'form' => $form->createView()
        ));
    }

}