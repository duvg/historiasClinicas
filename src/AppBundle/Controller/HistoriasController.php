<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Notadevolucion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Paciente;
use AppBundle\Entity\Historia;
use AppBundle\Entity\historiasnumber;
use AppBundle\Form\PacienteType;
use AppBundle\Form\HistoriaType;
use AppBundle\Form\NotadevolucionType;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class HistoriasController
 * @Route("/admin/historias")
 */
class HistoriasController extends Controller
{
    /**
     * @Route("/", name="listado_historias")
     */
    public function indexAction()
    {

        //return all history
        $em = $this->getDoctrine()->getManager();
        $historias = $em->getRepository(Historia::class)
            ->findHistoriasEnabled();


        return $this->render('AppBundle:Historias:index.html.twig', array(
            'historias' => $historias,
            'fechaActual' => new \DateTime()
        ));
    }

    /**
     * @Route("/historia/{doucumento}", name="paciente_historias")
     */
    public function historiaPacienteAction($documento)
    {
        //return all history
        $em = $this->getDoctrine()->getManager();
        $historias = $em->getRepository('Historia')
            ->findHistoriaPaciente($documento);

        return $this->render('AppBundle:Historias:detalle.html.twig', array(
            'historias' => $historias
        ));
    }

    /**
     * @Route("/crear", name="crear_historia")
     */
    public function crearHistoriaAction(Request $request)
    {
        //imc = peso / estatuda ^ 2
        $em = $this->getDoctrine()->getManager();

        //obtenemos el consecuvito
        //se obtiene todos los numeros de las historias en un array
        $historiasex = $em->getRepository('AppBundle:Historia')->findHistorias();

        //se genera un numero unico para la historia
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $unique = $today . $rand;
        //se buscar si el numero de la histria ya existe
        while (in_array($unique, $historiasex)){
            //si extiste se genera uno nuevo
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        }
        //si no existe se procede con ese numero y se almacena para que no se repita
        $historian = new historiasnumber();
        $historian->setHistorian($unique);





        $historia = new Historia();
        $historia->setConsecutivo($historian->getHistorian());
        $form = $this->createForm(HistoriaType::class, $historia);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em->persist($historian);
            $em->persist($historia);
            $em->flush();

            $this->addFlash('success-save', 'Historia creado satisfactoriamente!');

            $html = $this->renderView('AppBundle:Historias:pdf.html.twig', array(
                'historia' => $historia,
                'rootDir' => $this->get('kernel')->getRootDir().'/..'
            ));

            /*return $this->render('AppBundle:Historias:pdf.html.twig', array(
                'historia'=>$historiaRepo
            ));*/
            return new PdfResponse(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                $historia->getConsecutivo().'.pdf'
            );
            return $this->redirectToRoute('listado_historias',array('id'));
        }

        return $this->render('AppBundle:Historias:form.html.twig', array(
            'form' => $form->createView()

        ));
    }

    /**
     * @param Request $request
     * @Route("/editar/{id}", name="editar_historia")
     */
    public function editarPacienteAction(Request $request, $id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(Historia::class)->find($id);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }

        $form = $this->createForm(HistoriaType::class, $historiaRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {

            $em->flush();

            $this->addFlash('success_update', 'Historia actualizada satisfactoriamente!');
            return $this->redirectToRoute('listado_historias');
        }


        return $this->render('AppBundle:Historias:form.html.twig', array(
            'historia' => $historiaRepo,
            'form' => $form->createView()
        ));

    }


    /**
     * Eliminar el paciente, cambia el estado del paciente para deshabilitarlo
     * @Route("/eliminar/{id}", name="eliminar_historia")
     */
    public function eliminarHistoriaAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(Historia::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionada con el id ".$id);
        }

        $historiaRepo->setEnabled(false);

        $em->flush();
        return $this->redirectToRoute('listado_historias');

    }

    /**
     * @Route("/printpdf/{id}", name="imprimir_historia")
     */
    public function generatePdfHistoria($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(Historia::class)->find($id);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }
        $html = $this->renderView('AppBundle:Historias:pdf.html.twig', array(
            'historia' => $historiaRepo,
            'rootDir' => $this->get('kernel')->getRootDir().'/..'
        ));

        /*return $this->render('AppBundle:Historias:pdf.html.twig', array(
            'historia'=>$historiaRepo
        ));*/
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            $historiaRepo->getConsecutivo().'.pdf'
        );
    }

    /**
     * @Route("/notasdeevolucion/{id}", name="nostas_evolucion")
     */
    public function notasDeEvlocionAction(Request $request, $id){
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(Historia::class)->find($id);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }

        $notadevolcion = new Notadevolucion();
        $form = $this->createForm(NotadevolucionType::class, $notadevolcion);
        $form->handleRequest($request);
        $notadevolcion->setHistoria($historiaRepo);
        $notadevolcion->setFecha(new \DateTime());
        if($form->isValid() && $form->isSubmitted()) {

            $em->persist($notadevolcion);
            $em->flush();

            $this->addFlash('success-save', 'Nota de evolución creada satisfactoriamente!');

            return $this->redirectToRoute('listado_historias', array('id'));
        }

        return $this->render('AppBundle:Notasdevolucion:form.html.twig', array(
            'form' => $form->createView(),
            'historia' => $historiaRepo
        ));
    }

    /**
     * @Route("/idx/{codigo}", name="buscar_idx")
     */
    public function idxAction($codigo)
    {
        $em = $this->getDoctrine()->getManager();

        $idx = $em->getRepository('AppBundle:Cie10')->findIdx($codigo);
        if(empty($idx)){
            return new JsonResponse(array('data' => 0));
        }
        return new JsonResponse(array('data' => $idx));
    }


}
