<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Notadevolucionfisio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Paciente;
use AppBundle\Entity\historiaFisioterapia;
use AppBundle\Entity\historiasfnumber;
use AppBundle\Form\PacienteType;
use AppBundle\Form\historiaFisioterapiaType;
use AppBundle\Form\NotadevolucionFisioType;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class HistoriasFisioController
 * @Route("/admin/historiasfisioterapia")
 */
class HistoriasFisioController extends Controller
{
    /**
     * @Route("/", name="listado_historiasfisio")
     */
    public function indexAction()
    {

        //return all history
        $em = $this->getDoctrine()->getManager();
        $historias = $em->getRepository(historiaFisioterapia::class)
            ->findHistoriasEnabled();


        return $this->render('AppBundle:HistoriasFisio:index.html.twig', array(
            'historias' => $historias,
            'fechaActual' => new \DateTime()
        ));
    }

    /**
     * @Route("/historia/{doucumento}", name="paciente_historiasfisio")
     */
    public function historiaPacienteAction($documento)
    {
        //return all history
        $em = $this->getDoctrine()->getManager();
        $historias = $em->getRepository('historiaFisioterapia')
            ->findHistoriaPaciente($documento);

        return $this->render('AppBundle:HistoriasFisio:detalle.html.twig', array(
            'historias' => $historias
        ));
    }

    /**
     * @Route("/crear/{idPaciente}", name="crear_historiafisio", defaults={"idPaciente" = null})
     */
    public function crearHistoriaAction(Request $request, $idPaciente)
    {


        //imc = peso / estatuda ^ 2
        $em = $this->getDoctrine()->getManager();

        //obtenemos el consecuvito
        //se obtiene todos los numeros de las historias en un array
        $historiasex = $em->getRepository('AppBundle:historiaFisioterapia')->findHistorias();

        //se genera un numero unico para la historia
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $unique = $today . $rand;
        //se buscar si el numero de la histria ya existe
        while (in_array($unique, $historiasex)){
            //si extiste se genera uno nuevo
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        }
        //si no existe se procede con ese numero y se almacena para que no se repita
        $historian = new historiasfnumber();
        $historian->setHistorian($unique);





        $historia = new historiaFisioterapia();
        $historia->setConsecutivo($historian->getHistorian());
        $form = $this->createForm(historiaFisioterapiaType::class, $historia);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em->persist($historian);
            $em->persist($historia);
            $em->flush();

            $this->addFlash('success-save', 'Historia creada satisfactoriamente!');

            $html = $this->renderView('AppBundle:HistoriasFisio:pdf.html.twig', array(
                'historia' => $historia,
                'rootDir' => $this->get('kernel')->getRootDir().'/..'
            ));

            /*return $this->render('AppBundle:HistoriasFisio:pdf.html.twig', array(
                'historia'=>$historiaRepo
            ));*/
            return new PdfResponse(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
                $historia->getConsecutivo().'.pdf'
            );
            return $this->redirectToRoute('listado_historiasfisio',array('id'));
        }

        return $this->render('AppBundle:HistoriasFisio:form.html.twig', array(
            'form' => $form->createView(),
            'idPaciente' => $idPaciente
        ));
    }

    /**
     * @param Request $request
     * @Route("/editar/{id}", name="editar_historiafisio")
     */
    public function editarPacienteAction(Request $request, $id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(HistoriaFisioterapia::class)->find($id);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }

        $form = $this->createForm(historiaFisioterapiaType::class, $historiaRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {

            $em->flush();

            $this->addFlash('success_update', 'Historia actualizada satisfactoriamente!');
            return $this->redirectToRoute('listado_historiasfisio');
        }


        return $this->render('AppBundle:HistoriasFisio:form.html.twig', array(
            'historia' => $historiaRepo,
            'idPaciente' => $id,
            'form' => $form->createView()
        ));

    }


    /**
     * Eliminar el paciente, cambia el estado del paciente para deshabilitarlo
     * @Route("/eliminar/{id}", name="eliminar_historia")
     */
    public function eliminarHistoriaAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(HistoriaFisioterapia::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionada con el id ".$id);
        }

        $historiaRepo->setEnabled(false);

        $em->flush();
        return $this->redirectToRoute('listado_historiasfisio');

    }

    /**
     * @Route("/printpdf/{id}", name="imprimir_historiafisio")
     */
    public function generatePdfHistoria($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(HistoriaFisioterapia::class)->find($id);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }

        //si se encuentra una historia se buscar las notas de evolucion las notas de evolucion
        $notasDeEvolucionRepo = $em->getRepository(Notadevolucionfisio::class)
			->findByHistoria($historiaRepo->getId());


		
        



        $html = $this->renderView('AppBundle:HistoriasFisio:pdf.html.twig', array(
            'historia' => $historiaRepo,
			"notas" => $notasDeEvolucionRepo,
            'consecutivo' =>$historiaRepo->getConsecutivo(),
            'rootDir' => $this->get('kernel')->getRootDir().'/..'
        ));
        
        /*
        return $this->render('AppBundle:HistoriasFisio:pdf.html.twig', array(
            'historia'=>$historiaRepo,
			'notas' => $notasDeEvolucionRepo
        ));*/

        $today = date("Ymd");
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="historia'.
                $historiaRepo->getPaciente()->getNombre().'_'.
                $historiaRepo->getPaciente()->getApellidos().'_'.
                    $today
                    .'.pdf"'
            )
        );
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            $historiaRepo->getConsecutivo().'.pdf'
        );
    }

    /**
     * @Route("/verhistoria/{idPaciente}", name="ver_historiafisio",defaults={"idPaciente" = null})
     */
    public function verPdfHistoria($idPaciente)
    {
        if(!$idPaciente){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(HistoriaFisioterapia::class)->findByPaciente($idPaciente);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }


        //si se encuentra una historia se buscar las notas de evolucion las notas de evolucion
        $notasDeEvolucionRepo = $em->getRepository(Notadevolucionfisio::class)
            ->findByHistoria($historiaRepo[0]->getId());


        $html = $this->renderView('AppBundle:HistoriasFisio:pdf.html.twig', array(
            'historia' => $historiaRepo[0],
            "notas" => $notasDeEvolucionRepo,
            'consecutivo' =>$historiaRepo[0]->getConsecutivo(),
            'rootDir' => $this->get('kernel')->getRootDir().'/..'
        ));
        
        /*
        return $this->render('AppBundle:HistoriasFisio:pdf.html.twig', array(
            'historia'=>$historiaRepo,
            'notas' => $notasDeEvolucionRepo
        ));*/

        $today = date("Ymd");
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="historia'.
                $historiaRepo[0]->getPaciente()->getNombre().'_'.
                $historiaRepo[0]->getPaciente()->getApellidos().'_'.
                    $today
                    .'.pdf"'
            )
        );
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            $historiaRepo[0]->getConsecutivo().'.pdf'
        );
    }

    /**
     * @Route("/notasdeevolucion/{id}", name="nostas_evolucionfiosio")
     */
    public function notasDeEvlocionAction(Request $request, $id){
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $historiaRepo = $em->getRepository(HistoriaFisioterapia::class)->find($id);
        if(!$historiaRepo){
            throw $this->createNotFoundException("No existe ninguna historia relacionado con el id ".$id);
        }

        $notadevolcion = new Notadevolucionfisio();
        $form = $this->createForm(NotadevolucionFisioType::class, $notadevolcion);
        $form->handleRequest($request);
        $notadevolcion->setHistoria($historiaRepo);
        $notadevolcion->setFecha(new \DateTime());
        if($form->isValid() && $form->isSubmitted()) {

            $em->persist($notadevolcion);
            $em->flush();

            $this->addFlash('success-save', 'Nota de evolución creada satisfactoriamente!');

            return $this->redirectToRoute('listado_historiasfisio', array('id'));
        }

        return $this->render('AppBundle:Notasdevolucion:form.html.twig', array(
            'form' => $form->createView(),
            'historia' => $historiaRepo
        ));
    }

    /**
     * @Route("/idx/{codigo}", name="buscar_idx")
     */
    public function idxAction($codigo)
    {
        $em = $this->getDoctrine()->getManager();

        $idx = $em->getRepository('AppBundle:Cie10')->findIdx($codigo);
        if(empty($idx)){
            return new JsonResponse(array('data' => 0));
        }
        return new JsonResponse(array('data' => $idx));
    }


}
