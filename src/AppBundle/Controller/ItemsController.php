<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Items;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ItemsController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @Route("/getItem/{code}", name="get_item")
     */
    public function getItem($code){
        if(!$code){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }

        $em = $this->getDoctrine()->getManager();
        $itemRepo = $em->getRepository('AppBundle:Items')->findBy(array(
            'code' => $code
        ));

        if(!$itemRepo){
            throw $this->createNotFoundException("No existe ningun registro relacionado zas");
        }


        $encoders = array(new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());


        $serializer = new Serializer($normalizers, $encoders);
        $jsonResponse = $serializer->serialize($itemRepo, "json");

        return new JsonResponse($itemRepo);

    }
}
