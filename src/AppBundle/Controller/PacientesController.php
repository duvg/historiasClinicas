<?php
/**
 * Created by Duviel Garcia.
 * Date: 15/11/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Paciente;
use AppBundle\Form\PacienteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\StreamedResponse;



/**
 * Class DefaultController
 * @Route("/admin/pacientes")
 */
class PacientesController extends Controller
{
    /**
     * @Route("/{tipo}", name="listar_paciente")
     */
    public function indexAction(Request $request, $tipo)
    {
        //return all clients
        $em = $this->getDoctrine()->getManager();
        $pacientes = $em->getRepository('AppBundle:Paciente')->findPacientesEnabled($tipo);

        return $this->render('AppBundle:Pacientes:index.html.twig', array(
            'pacientes' => $pacientes,
            'tipo' => $tipo
        ));

    }

    /**
     * @param Request $request
     * @Route("/crear/{tipo}", name="crear_paciente")
     */
    public function pacienteCrearAction(Request $request, $tipo){

        $paciente = new Paciente();
    
        $form = $this->createForm(PacienteType::class, $paciente);

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            $date = new \DateTime($request->request->get('fechaNacimiento'));
            $paciente->setFechaNacimiento($date);
            $em = $this->getDoctrine()->getManager();
            $em->persist($paciente);
            $em->flush();

            $this->addFlash('success-save', "$tipo creado satisfactoriamente!");

            //return $this->redirectToRoute('listar_paciente');
            return $this->redirect($this->generateUrl('listar_paciente', array('tipo' => $tipo)));
        }

        return $this->render('AppBundle:Pacientes:form.html.twig', array(
            'form' => $form->createView(),
            'tipo' => $tipo
        ));


    }

    /**
     * @param Request $request
     * @Route("/editar/{id}/{tipo}", name="editar_paciente")
     */
    public function editarPacienteAction(Request $request, $id, $tipo)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $pacienteRepo = $em->getRepository(Paciente::class)->find($id);
        if(!$pacienteRepo){
            throw $this->createNotFoundException("No existe ningun paciente relacionado con el id ".$id);
        }

        $form = $this->createForm(PacienteType::class, $pacienteRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {

            $em->flush();

            $this->addFlash('success_update', "$tipo actualizado satisfactoriamente!");
            return $this->redirect($this->generateUrl('listar_paciente', array('tipo' => $tipo)));
        }


        return $this->render('AppBundle:Pacientes:form.html.twig', array(
            'paciente' => $pacienteRepo,
            'form' => $form->createView(),
            'tipo' => $tipo
        ));

    }


    /**
     * Eliminar el paciente, cambia el estado del paciente para deshabilitarlo
     * @Route("/eliminar/{id}", name="eliminar_paciente")
     */
    public function eliminarPaciente($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $pacienteRepo = $em->getRepository(Paciente::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$pacienteRepo){
            throw $this->createNotFoundException("No existe ningun paciente relacionado con el id ".$id);
        }

        $pacienteRepo->setEnabled(false);

        $em->flush();

        $this->addFlash('success_update', "Registro eliminado satisfactoriamente!");

        return $this->redirectToRoute('listar_paciente', array('tipo' => 'pacientes'));
        return $this->redirect($this->generateUrl('listar_paciente', array('tipo' => 'pacientes')));

    }

    /**
     * @Route("/pac/{cedula}", name="buscar_paciente")
     */
    public function pacAction($cedula)
    {
        $em = $this->getDoctrine()->getManager();

        $servicios = $em->getRepository('AppBundle:Paciente')->findBy(array(
            'documento' => $cedula
        ));

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array($normalizer);


        $serializer = new Serializer($normalizers, $encoders);


        $jsonContent = $serializer->serialize($servicios[0], 'json');     
        

        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }



    /**
     * @Route("/exportar/excel", name="pacientes_exportar")
     */
    public function exportAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pacientes = $em->getRepository('AppBundle:Paciente')->findAll();

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);





        //$response = new StreamedResponse();
        $responses = new StreamedResponse();
        $responses->setCallback(
            function () use ($pacientes) {
                $handle = fopen('php://output', 'w+');
                fputcsv(
                    $handle, 
                    [
                        'id', 
                        'Tipo Documento', 
                        '# Documento ',
                        'Nombre', 
                        'Apellidos', 
                        'Email', 
                        'Departamento',
                        'Municipio',
                        'Direccion', 
                        'Telefono', 
                        'Sexo', 
                        'Edad', 
                        'Fecha de Nacimiento',
                        'Ocupacion',
                        'Saldo',
                        'Plan'
                    ],
                    ';');
                $handle = fopen('php://output', 'r+');
                foreach ($pacientes as $row) {
                    //array list fields you need to export
                    $data = array(
                        $row->getId(),
                        $row->getTipoDocumento(),
                        $row->getDocumento(),
                        $row->getNombre(),
                        $row->getApellidos(),
                        $row->getEmail(),
                        $row->getDepartamento(),
                        $row->getMunicipio(),
                        $row->getDireccion(),
                        $row->getTelefono(),
                        $row->getSexo(),
                        $row->getEdad(),
                        $row->getFechaNacimiento()->format('Y/m/d'),
                        $row->getOcupacion(),
                        $row->getSaldo(),
                        $row->getPlan()->getNombre()
                    );
                    fputcsv($handle, $data, ';');
                }
                fclose($handle);
            }
        );

        $responses->headers->set('Content-Type', 'application/octet-stream');
        $responses->headers->set('Content-Disposition', 'attachment; filename=pacientesyafilaidos.csv');

        return $responses;
        
    }




}