<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Planes;
use AppBundle\Form\PlanType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Class PlanController
 * @Route("/admin/planes", name="planes")
 */
class PlanController extends Controller
{
    /**
     * @Route("/", name="listar_planes")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $planes = $em->getRepository('AppBundle:Planes')->findAll();

        return $this->render('AppBundle:Planes:index.html.twig', array(
            'planes' => $planes
        ));
    }

    /**
     * @Route("/crear", name="crear_plan")
     */
    public function crearAction(Request $request)
    {
        $plan = new Planes();
        $form = $this->createForm(PlanType::class, $plan);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($plan);
            $em->flush();

            $this->addFlash('success-save', 'Plan creado satisfactoriamente!');

            return $this->redirectToRoute('listar_planes');
        }

        return $this->render('AppBundle:Planes:form.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/editar/{id}", name="editar_plan")
     */
    public function editarAction(Request $request, $id )
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $planRepo = $em->getRepository(Planes::class)->find($id);

        if(!$planRepo){
            throw $this->createNotFoundException("No existe ningun plan relacionado con el id ".$id);
        }


        $form = $this->createForm(PlanType::class, $planRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $em->flush();

            $this->addFlash('success_update', 'Plan actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_planes');
        }


        return $this->render('AppBundle:Planes:form.html.twig', array(
            'plan' => $planRepo,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_plan")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $planRepo = $em->getRepository(Planes::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$planRepo){
            throw $this->createNotFoundException("No existe ningun plan relacionado con el id ".$id);
        }

        $em->remove($planRepo);

        $em->flush();
        return $this->redirectToRoute('listar_planes');

    }

    /**
     * @Route("/search/{id}", name="buscar_plan")
     */
    public function searchAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $planes = $em->getRepository('AppBundle:Planes')->find($id);

        return new JsonResponse(array('data' => $planes->getValor()));
    }

}
