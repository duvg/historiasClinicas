<?php

namespace AppBundle\Controller;

use AppBundle\Entity\historiasnumber;
use AppBundle\Entity\Post;
use AppBundle\Entity\Servicio;
use AppBundle\Entity\Test;
use AppBundle\Form\PostType;
use AppBundle\Form\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\FileUploader;
/**
 * Class PostController
 * @Route("/admin/posts", name="posts")
 */
class PostController extends Controller
{
    /**
     * @Route("/", name="listar_posts")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $post = $em->getRepository('AppBundle:Post')->findPostsEnabled();

        return $this->render('AppBundle:Posts:index.html.twig', array(
            'posts' => $post
        ));
    }

    /**
     * @Route("/crear", name="crear_posts")
     */
    public function crearAction(Request $request, FileUploader $fileUploader)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $file = $post->getImagen();

            if(is_null($file))
            {
                $post->setImagen("default.png");
            }
            else{
                //nombre unico para la imagen a subir
                $filename = $fileUploader->upload($file);

                //subir la imagen y asignar el nombre del archivo a la entidad
                $post->setImagen($filename);    
            }
            

            $em->persist($post);
            $em->flush();

            $this->addFlash('success-save', 'Noticia creada satisfactoriamente!');

            return $this->redirectToRoute('listar_posts');
        }

        return $this->render('AppBundle:Posts:form.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/editar/{id}", name="editar_posts")
     */
    public function editarAction(Request $request, FileUploader $fileUploader, $id )
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $postRepo = $em->getRepository(Post::class)->find($id);
        $imagen = $postRepo->getImagen();

        if(!$postRepo){
            throw $this->createNotFoundException("No existe ningun servicio relacionado con el id ".$id);
        }

        $postRepo->setImagen(
            new File($this->getParameter('servicios_directory').'/'.$postRepo->getImagen())
        );

        $form = $this->createForm(PostType::class, $postRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            if(is_null($postRepo->getImagen()))
            {
                if(is_null($imagen))
                {
                    $postRepo->setImagen("default.png");
                }
                else{
                    $postRepo->setImagen($imagen);
                }
            }
            else{
                $file = $postRepo->getImagen();
                //nombre unico para la imagen a subir
                $filename = $fileUploader->upload($file);

                //subir la imagen y asignar el nombre del archivo a la entidad
                $postRepo->setImagen($filename);
            }

            $em->flush();

            $this->addFlash('success_update', 'Post actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_posts');
        }

        return $this->render('AppBundle:Posts:form.html.twig', array(
            'post' => $postRepo,
            'imagen' => $imagen,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_post")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $postRepo = $em->getRepository(Post::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$postRepo){
            throw $this->createNotFoundException("No existe ningun post relacionado con el id ".$id);
        }

        $em->remove($postRepo);

        $em->flush();
        return $this->redirectToRoute('listar_posts');

    }


    /**
     * @Route("/creartest", name="crear_test")
     */
    public function creartestAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //$number = md5(uniqid(rand(), true));
        //echo $number."<br><br><br>";
        /*

        die();*/

        //se obtiene todos los numeros de las historias en un array
        $historiasex = $em->getRepository('AppBundle:Test')->findHistorias();

        //se genera un numero unico para la historia
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $unique = $today . $rand;
        //se buscar si el numero de la histria ya existe
        while (in_array($unique, $historiasex)){
            //si extiste se genera uno nuevo
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        }
        //si no existe se procede con ese numero y se almacena para que no se repita
        $historian = new historiasnumber();
        $historian->setHistorian($unique);
        $em->persist($historian);
        $em->flush();


        //echo $unique."<br>";
        //print_r($historiasex);
        //die();
        $post = new Test();
        $post->setHistorian(10);
        $form = $this->createForm(TestType::class, $post);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            $em->persist($post);
            $em->flush();

            $this->addFlash('success-save', 'Test creado satisfactoriamente!');

            return $this->redirectToRoute('listar_servicios');
        }

        return $this->render('AppBundle:Servicios:formtest.html.twig', array(
            "form" => $form->createView()
        ));
    }
}
