<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Pqrs;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\PqrsType;

class PqrsController extends Controller
{
    /**
     * @Route("/pqrs", name="pqrs")
     */
    public function indexAction(Request $request)
    {
        $pqrs = new Pqrs();
        $form = $this->createForm(PqrsType::class, $pqrs);

        $form->handleRequest($request);

        if( $form->isValid()){
            //save data, persist and flush
            $em = $this->getDoctrine()->getManager();
            $em->persist($pqrs);
            $em->flush();

            return $this->redirect($this->generateUrl('success_contact'));
        }

        return $this->render('application/pqrs.html.twig', array('form' => $form->createView()));
    }
}