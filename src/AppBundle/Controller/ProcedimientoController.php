<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Paciente;
use AppBundle\Entity\Procedimiento;
use AppBundle\Entity\TipoTratamiento;
use AppBundle\Entity\Tratamiento;
use AppBundle\Form\ProcedimientoType;
use AppBundle\Form\PacienteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProcedimientoController
 * @Route("/admin/procedimientos")
 */
class ProcedimientoController extends Controller
{
    /**
     * @Route("/", name="listar_procedimientos")
     */
    public function indexAction()
    {
        //return all history
        $em = $this->getDoctrine()->getManager();
        $procedimientos = $em->getRepository(Procedimiento::class)
            ->findProcedimientosEnabled();


        return $this->render('AppBundle:Procedimientos:index.html.twig', array(
            'procedimientos' => $procedimientos
        ));
    }

    /**
     * @Route("/crear", name="crear_procedimiento")
     */
    public function crearAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //buscar departamento
        $dpto = $em->getRepository("AppBundle:Departamento")->find(11);
        $muni = $em->getRepository("AppBundle:Municipio")->find(455);


        $procedimiento = new Procedimiento();
        $form = $this->createForm(ProcedimientoType::class, $procedimiento);
        $paciente = new Paciente();
        $paciente->setDepartamento($dpto);
        $paciente->setMunicipio($muni);
        $form2 = $this->createForm(PacienteType::class, $paciente);



        $form->handleRequest($request);

        if($form->isValid() && $request->isMethod())
        {
            //$date = new \DateTime($request->request->get('fechaNacimiento'));
            //$procedimiento->setFechaNacimiento($date);

            $em->persist($procedimiento);
            $em->flush();

            $this->addFlash('success-save', 'Procedimiento registrado satisfactoriamente!');

            return $this->redirectToRoute('listar_procedimiento');
        }

        $form2->handleRequest($request);

        if($form2->isValid())
        {
            try
            {
                //$date = new \DateTime($request->request->get('fechaNacimiento'));
                //$procedimiento->setFechaNacimiento($date);
                $em->persist($paciente);
                $em->flush();

                $this->addFlash('success-save', 'Paciente registrado satisfactoriamente!');

                return $this->redirectToRoute('crear_procedimiento');
                }
            catch(\Doctrine\ORM\ORMException $e){
                return $e->getMessage();
            } catch(\Exception $e){
                // other exceptions
                // flash
                // logger
                // redirection
                return $e->getMessage();
              }
            
        }



        return $this->render('AppBundle:Procedimientos:form.html.twig', array(
            'form' => $form->createView(),
            'form2' => $form2->createView()
        ));

    }

    /**
     * @Route("/save", name="save_procedure")
     */
    public function guardarAction(Request $request)
    {

        $request->get("data");

        $content = json_decode($request->getContent());



        if(count($content) <= 0){
            return  new JsonResponse(0);
        }
        $em = $this->getDoctrine()->getManager();
        //se optiene al paciente
        $data = $content[0];
        $paciente = $em->getRepository(Paciente::class)->find($data->paciente);
        $procedimiento  = new Procedimiento();
        $procedimiento->setPaciente($paciente);



        /*$dataa = array();

        for($i= 1; $i < count($content); $i++){
            $obj = $content[$i];
            $dataa[] = $i;
        }

        return new JsonResponse($dataa);

        $tipoTratamiento = $em->getRepository(TipoTratamiento::class)->find(1);

        //busqueda del tratamiento
        $tratamiento = $em->getRepository(Tratamiento::class)->find(1);
        $procedimiento->setTipotratamiento($tipoTratamiento);
        $procedimiento->setTratamiento($tratamiento);*/


        $saldoPaciente = $paciente->getSaldo();


        $valorNeto = 0;

        $valorDescuento = 0;
        $valorTotal = 0;
        $totalPagar = 0;
        $nuevoSaldo = $saldoPaciente;

        $arrProcedimientos = array();



        try
        {

        for($i = 2; $i < count($content); $i++){
            $procedimiento  = new Procedimiento();
            $procedimiento->setPaciente($paciente);
            $obj = $content[$i];
            //busqueda del tipo de tratamiento
            $tipoTratamiento = $em->getRepository(TipoTratamiento::class)->find($obj->tipoTratamiento);

            //busqueda del tratamiento
            $tratamiento = $em->getRepository(Tratamiento::class)->find($obj->tratamiento);

            $procedimiento->setTipotratamiento($tipoTratamiento);
            $procedimiento->setTratamiento($tratamiento);
            $procedimiento->setFecha(new \DateTime());
            $procedimiento->setEnabled(true);


            //se calcula el precio del total a pagar por cada procedimeinto agregado
            $valorNeto = $valorNeto + $obj->valor;
            //calculamos el descuento
            if($paciente->getPlan() != "no")
            {
                $valorDescuento = ($valorNeto * 5) / 100;
            }

            $valorTotal = $valorNeto - $valorDescuento;
            if($valorTotal > $saldoPaciente)
            {
                $nuevoSaldo = 0;
                $totalPagar = $valorTotal - $saldoPaciente;
            }
            else
            {
                $nuevoSaldo = $saldoPaciente - $valorTotal;
                $totalPagar = 0;

            }

            $arrProcedimientos[] = $procedimiento;
            //$em->persist($procedimiento);
        }
        }
        catch (\Exception $ex)
        {
            return new JsonResponse($ex->getMessage());
        }


        //actualizar saldo del paciente, de acuerdo al nuevo saldo
        $paciente->setSaldo($nuevoSaldo);
        $em->persist($paciente);

        $em->flush();




        $this->addFlash('success-save', 'Procedimiento creado satisfactoriamente!');


        //retornamos el pdf a la funcion ajax

        $fecha = new \DateTime();
        $today = date("Ymd");



        //se buscan lso procedimientos de ese paciente al dia de hoy y se imprime el consentimiento

        $html = $this->renderView('AppBundle:Procedimientos:prefactura.html.twig', array(
            'rootDir' => $this->get('kernel')->getRootDir().'/..',
            'paciente' => $paciente,
            'fecha' => $fecha,
            'saldo' => intval($saldoPaciente),
            'procedimientos' => $arrProcedimientos,
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'page-size' => 'A4',
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="prefactura_'.
                    $paciente->getNombre().'_'.
                    $paciente->getApellidos().'_'.
                    $today
                    .'.pdf"'
            )
        );

        //return new JsonResponse($paciente->getId());


    }

    /**
     * @Route("/consentimientos", name="consentimientos_informados", methods="POST")
     */
    public function consentimientosAction(Request $request)
    {
        $content = json_decode($request->getContent());
        if(count($content) <= 0){
            return  new JsonResponse(0);
        }
        $tratamientos = array();
        $datos = [];
        $datosR = array();
        $em = $this->getDoctrine()->getManager();
        for($i = 2; $i< count($content); $i++) {
            $obj = $content[$i];
            $datos[] = $i;
            $datosR[] = $obj->tratamiento;
            //busqueda del tratamiento
            //$tratamientos[] = $em->getRepository(Tratamiento::class)->find($obj->tratamiento);
        }
        $tratamientosList = $em->getRepository(Tratamiento::class)->findById($datosR);

        $data = $content[0];

        //return new JsonResponse($tratamientosList);
        $paciente = $em->getRepository(Paciente::class)->find($data->paciente);

        $fecha = new \DateTime();
        $today = date("Ymd");



        $html = $this->renderView('AppBundle:Consentimientos:CSBiosplasma.html.twig', array(
            'rootDir' => $this->get('kernel')->getRootDir().'/..',
            'paciente' => $paciente,
            'fecha' => $fecha,
            'procedimientos' => $tratamientosList,
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="consentimiento_'.
                    $paciente->getNombre().'_'.
                    $paciente->getApellidos().'_'.
                    $today
                    .'.pdf"'
            )
        );


    }

    /**
     * @Route("/editar/{id}", name="editar_procedimiento")
     */
    public function actualizarAction(Request $request, $id)
    {

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_procedimiento")
     */
    public function eliminarAction($id)
    {

        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $procedimientoRepo = $em->getRepository(Procedimiento::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$procedimientoRepo){
            throw $this->createNotFoundException("No existe ningun procedimiento relacionado con el id ".$id);
        }

        $em->remove($procedimientoRepo);

        $em->flush();
        return $this->redirectToRoute('listar_procedimientos');

    }

    /**
     * @param $id
     * @Route("/consentimiento/{id}", name="imprimir_consentimientopdf")
     */
    public function getDataAction($id){
        $em = $this->getDoctrine()->getManager();
        //se buscan lso procedimientos de ese paciente al dia de hoy y se imprime el consentimiento
        $proPaciente = $em->getRepository(Procedimiento::class)->findProcedimientosPaciente(
            $id
        );

        $fecha = new \DateTime();
        $today = date("Ymd");

        //se buscan lso procedimientos de ese paciente al dia de hoy y se imprime el consentimiento
        $paciente = $em->getRepository(Paciente::class)->find($id);
        $html = $this->renderView('AppBundle:Consentimientos:CSBiosplasma.html.twig', array(
            'rootDir' => $this->get('kernel')->getRootDir().'/..',
            'paciente' => $paciente,
            'fecha' => $fecha,
            'procedimientos' => $proPaciente,
        ));


        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="consentimiento_'.
                    $paciente->getNombre().'_'.
                    $paciente->getApellidos().'_'.
                    $today
                    .'.pdf"'
            )
        );

    }

    /**
     * @Route("/prefactura/{id}/{saldopaciente}", name="imprimir_prefactura")
     */
    public function preafacturaAction($id, $saldopaciente){
        $em = $this->getDoctrine()->getManager();

        //se buscan lso procedimientos de ese paciente al dia de hoy y se imprime el consentimiento
        $proPaciente = $em->getRepository(Procedimiento::class)->findProcedimientosPaciente(
            $id
        );

        $fecha = new \DateTime();
        $today = date("Ymd");

        //se buscan lso procedimientos de ese paciente al dia de hoy y se imprime el consentimiento
        $paciente = $em->getRepository(Paciente::class)->find($id);
        $html = $this->renderView('AppBundle:Procedimientos:prefactura.html.twig', array(
            'rootDir' => $this->get('kernel')->getRootDir().'/..',
            'paciente' => $paciente,
            'fecha' => $fecha,
            'saldo' => $saldopaciente,
            'procedimientos' => $proPaciente
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'page-size' => 'A4',
                'encoding' => 'utf-8',
                'margin-top' => 10,
                'margin-bottom' => 10,

            )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="prefactura_'.
                    $paciente->getNombre().'_'.
                    $paciente->getApellidos().'_'.
                    $today
                    .'.pdf"'
            )
        );

    }

    /**
     * recarga
     * @Route("/recargar", name="recargar")
     */
    public function recargaAction(Request $request)
    {

        $content = json_decode($request->getContent());
        if(empty($content)){
            $data = array(
                'data' => "error",
                'msg' => "paciente no encontrado"
            );
        }
        else
        {
            $em = $this->getDoctrine()->getManager();
            $pacienteRespo = $em->getRepository('AppBundle:Paciente')->find($content->id);

            $pacienteRespo->setSaldo($content->valorRecarga);

            $em->flush();

            $data = array(
                'data' => "success",
                'msg' => "recarga realizada satisfactoriamente"
            );
        }

        return new JsonResponse(
            $data
        );

    }





}
