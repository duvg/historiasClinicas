<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Paciente;
use AppBundle\Form\PacienteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 * @Route("/")
 */
class PruebasController extends Controller {
    /**
     * @Route("/email", name="send_email")
     */
    public function sendEmailAction(\Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('send@example.com')
        ->setTo('saisbear-90@hotmail.com')
        ->setBody(
           'Hola desde el servidor sara', 'text'
        )
        /*
         * If you also want to include a plaintext version of the message
        ->addPart(
            $this->renderView(
                'Emails/registration.txt.twig',
                array('name' => $name)
            ),
            'text/plain'
        )
        */
    ;

    $det = $mailer->send($message);

        
        return new JsonResponse(array("asdsad" => $det));
    }
}
