<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Afiliado;
use AppBundle\Entity\Paciente;
use AppBundle\Entity\Procedimiento;
use AppBundle\Entity\SolicitudServicio;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

/**
 * Class ReportesController
 * @Route("/admin/reportes")
 */
class ReportesController extends Controller
{
    /**
     * @Route("/", name="listado_reportes")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $afiliadoRepo = $em->getRepository(Afiliado::class)->getCantidadAfiliados();
        $pacienteRepo = $em->getRepository(Paciente::class)->getCantidadPacientes();
        $procedimientoRepo = $em->getRepository(Procedimiento::class)->getCantidadProcedimientos();
        $solicitudesRepo = $em->getRepository(SolicitudServicio::class)->getCantidadSolicitudes();

        return $this->render("AppBundle:Reportes:index.html.twig", array(
            'afiliados'      => $afiliadoRepo,
            'pacientes'      => $pacienteRepo,
            'procedimientos' => $procedimientoRepo,
            'solicitudes'    =>$solicitudesRepo
        ));
    }

    /**
     * @Route("/afiliados", name="reporte_afiliados")
     */
    public  function afiliadosAction()
    {
        //retorna todos los afiliados
        $em = $this->getDoctrine()->getManager();
        $afiliadoRepo = $em->getRepository(Afiliado::class)->findAfiliadosEnabled();
        $fecha = new \DateTime();
        $html = $this->renderView('AppBundle:Reportes:listaAfiliados.html.twig', array(
            'afiliados' => $afiliadoRepo,
            'fecha' => $fecha,
            'rootDir' => $this->get('kernel')->getRootDir().'/..'
        ));

        /*return $this->render('AppBundle:Reportes:listaAfiliados.html.twig', array(
            'afiliados'=>$afiliadoRepo,
            'fecha' => $fecha,
        ));*/
        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'ListadoAfiliados.pdf'
        );
    }


}
