<?php

namespace AppBundle\Controller;

use AppBundle\Entity\historiasnumber;
use AppBundle\Entity\Servicio;
use AppBundle\Entity\Test;
use AppBundle\Form\ServicioType;
use AppBundle\Form\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\FileUploader;
/**
 * Class ServicioController
 * @Route("/admin/servicios", name="servicios")
 */
class ServicioController extends Controller
{
    /**
     * @Route("/", name="listar_servicios")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $servicios = $em->getRepository('AppBundle:Servicio')->findServiciosEnabled();

        return $this->render('AppBundle:Servicios:index.html.twig', array(
            'servicios' => $servicios
        ));
    }

    /**
     * @Route("/crear", name="crear_servicio")
     */
    public function crearAction(Request $request, FileUploader $fileUploader)
    {
        $servicio = new Servicio();
        $form = $this->createForm(ServicioType::class, $servicio);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $file = $servicio->getImagen();

            if(is_null($file))
            {
                $servicio->setImagen("imgdefault.png");
            }
            else{
                //nombre unico para la imagen a subir
                $filename = $fileUploader->upload($file);

                //subir la imagen y asignar el nombre del archivo a la entidad
                $servicio->setImagen($filename);    
            }



            $em->persist($servicio);
            $em->flush();

            $this->addFlash('success-save', 'Servicio creado satisfactoriamente!');

            return $this->redirectToRoute('listar_servicios');
        }

        return $this->render('AppBundle:Servicios:form.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/editar/{id}", name="editar_servicio")
     */
    public function editarAction(Request $request, FileUploader $fileUploader, $id )
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $servicioRepo = $em->getRepository(Servicio::class)->find($id);
        $imagen = $servicioRepo->getImagen();

        if(!$servicioRepo){
            throw $this->createNotFoundException("No existe ningun servicio relacionado con el id ".$id);
        }

        $servicioRepo->setImagen(
            new File($this->getParameter('servicios_directory').'/'.$servicioRepo->getImagen())
        );

        $form = $this->createForm(ServicioType::class, $servicioRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            if(is_null($servicioRepo->getImagen()))
            {
                if(is_null($imagen))
                {
                    $servicioRepo->setImagen("default.png");
                }
                else{
                    $servicioRepo->setImagen($imagen);
                }
            }
            else{
                $file = $servicioRepo->getImagen();
                //nombre unico para la imagen a subir
                $filename = $fileUploader->upload($file);

                //subir la imagen y asignar el nombre del archivo a la entidad
                $servicioRepo->setImagen($filename);
            }




            $em->flush();

            $this->addFlash('success_update', 'Servicio actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_servicios');
        }


        return $this->render('AppBundle:Servicios:form.html.twig', array(
            'servicio' => $servicioRepo,
            'imagen' => $imagen,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_servicio")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $servicioRepo = $em->getRepository(Servicio::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$servicioRepo){
            throw $this->createNotFoundException("No existe ningun servicio relacionado con el id ".$id);
        }

        $em->remove($servicioRepo);

        $em->flush();
        return $this->redirectToRoute('listar_servicios');

    }


    /**
    * @Route("/creartest", name="crear_test")
    */
    public function creartestAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //$number = md5(uniqid(rand(), true));
        //echo $number."<br><br><br>";
        /*

        die();*/

        //se obtiene todos los numeros de las historias en un array
        $historiasex = $em->getRepository('AppBundle:Test')->findHistorias();

        //se genera un numero unico para la historia
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        $unique = $today . $rand;
        //se buscar si el numero de la histria ya existe
        while (in_array($unique, $historiasex)){
            //si extiste se genera uno nuevo
            $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        }
        //si no existe se procede con ese numero y se almacena para que no se repita
        $historian = new historiasnumber();
        $historian->setHistorian($unique);
        $em->persist($historian);
        $em->flush();


        //echo $unique."<br>";
        //print_r($historiasex);
        //die();
        $servicio = new Test();
        $servicio->setHistorian(10);
        $form = $this->createForm(TestType::class, $servicio);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {






            $em->persist($servicio);
            $em->flush();

            $this->addFlash('success-save', 'Test creado satisfactoriamente!');

            return $this->redirectToRoute('listar_servicios');
        }

        return $this->render('AppBundle:Servicios:formtest.html.twig', array(
            "form" => $form->createView()
        ));
    }





}
