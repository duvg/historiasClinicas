<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Servicio;
use AppBundle\Entity\SolicitudServicio;
use AppBundle\Entity\User;
use AppBundle\Form\ServicioType;
use AppBundle\Form\SolicitudServicioType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ServicioController
 * @Route("/admin/solicitudes", name="servicios")
 */
class SolicitudServicioController extends Controller
{
    /**
     * @Route("/", name="listar_solicitudes")
     */
    public function indexAction(Request $request)
    {
        //$logger = $this->get('logger');
        //$logger->userinfo("listado de las solicitudes del sistema");


        $em = $this->getDoctrine()->getManager();
        $solicitud = new SolicitudServicio();

        $form = $this->createForm(SolicitudServicioType::class, $solicitud);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {

            $user = $em->getRepository(User::class)->find(
                $this->getUser()
            );
            $solicitud->setUsuario($user);

            $em->persist($solicitud);
            $em->flush();

            $this->addFlash('success-save', 'Nueva Solicitud generada satisfactoriamente!');


            return $this->redirectToRoute('listar_solicitudes');
        }


        //servicios disponibles
        $servicios = $em->getRepository('AppBundle:Servicio')->findServiciosEnabled();

        $solicitudServicios = $em->getRepository('AppBundle:SolicitudServicio')->findServiciosEnabled();

        return $this->render('AppBundle:SolicitudServicios:index.html.twig', array(
            'solicitudes' => $solicitudServicios,
            'servicios' => $servicios,
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/crear", name="crear_solicitudes")
     */
    public function crearAction(Request $request)
    {

        $solicitud = new SolicitudServicio();

        $form = $this->createForm(SolicitudServicioType::class, $solicitud);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->find(
                $this->getUser()
            );
            $solicitud->setUsuario($user);

            $em->persist($solicitud);
            $em->flush();

            $this->addFlash('success-save', 'Nueva Solicitud generada satisfactoriamente!');

            return $this->redirectToRoute('listar_solicitudes');
        }

        return $this->render('AppBundle:SolicitudServicios:form.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/editar/{id}", name="editar_solicitud")
     */
    public function editarAction(Request $request, $id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $servicioRepo = $em->getRepository(SolicitudServicio::class)->find($id);
        if(!$servicioRepo){
            throw $this->createNotFoundException("No existe ningun servicio relacionado con el id ".$id);
        }

        $form = $this->createForm(SolicitudServicioType::class, $servicioRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $servicioRepo->setEnabled(false);
            $em->flush();

            $this->addFlash('success_update', 'Servicio actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_solicitudes');
        }


        return $this->render('AppBundle:SolicitudServicios:form.html.twig', array(
            'solicitudes' => $servicioRepo,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_solicitud")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $servicioRepo = $em->getRepository(SolicitudServicio::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$servicioRepo){
            throw $this->createNotFoundException("No existe ninguna solicitud relacionado con el id ".$id);
        }

        $servicioRepo->setEnabled(false);

        $em->flush();
        return $this->redirectToRoute('listar_solicitudes');

    }
}



    

    

   

    


      