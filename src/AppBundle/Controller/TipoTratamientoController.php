<?php

namespace AppBundle\Controller;

use AppBundle\Entity\historiasnumber;
use AppBundle\Entity\Servicio;
use AppBundle\Entity\Test;
use AppBundle\Entity\TipoTratamiento;
use AppBundle\Form\TipoTratamientoType;
use AppBundle\Form\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\FileUploader;
/**
 * Class TipoTratamientoController
 * @Route("/admin/tipotratamiento", name="tipotratamientos")
 */
class TipoTratamientoController extends Controller
{
    /**
     * @Route("/", name="listar_tipotratamientos")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tratamientos = $em->getRepository('AppBundle:TipoTratamiento')->findTipoTratamientosEnabled();
        

        return $this->render('AppBundle:TipoTratamientos:index.html.twig', array(
            'tipotratamientos' => $tratamientos
        ));
    }

    /**
     * @Route("/crear", name="crear_tipotratamiento")
     */
    public function crearAction(Request $request)
    {
        $tipotratamiento = new TipoTratamiento();
        $form = $this->createForm(TipoTratamientoType::class, $tipotratamiento);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($tipotratamiento);
            $em->flush();

            $this->addFlash('success-save', 'Tipo de Servicio creado satisfactoriamente!');

            return $this->redirectToRoute('listar_tipotratamientos');
        }

        return $this->render('AppBundle:TipoTratamientos:form.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/editar/{id}", name="editar_tipotratamiento")
     */
    public function editarAction(Request $request, $id )
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $tipotratamientoRepo = $em->getRepository(TipoTratamiento::class)->find($id);

        if(!$tipotratamientoRepo){
            throw $this->createNotFoundException("No existe ningun tipo de servicio relacionado con el id ".$id);
        }

        $form = $this->createForm(TipoTratamientoType::class, $tipotratamientoRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $em->flush();

            $this->addFlash('success_update', 'Tipo de Servicio actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_tipotratamientos');
        }


        return $this->render('AppBundle:TipoTratamientos:form.html.twig', array(
            'tipotratamiento' => $tipotratamientoRepo,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_tipotratamiento")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $tratamientoRepo = $em->getRepository(TipoTratamiento::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$tratamientoRepo){
            throw $this->createNotFoundException("No existe ningun tipo de servicio relacionado con el id ".$id);
        }

        $em->remove($tratamientoRepo);

        $em->flush();
        return $this->redirectToRoute('listar_tipotratamientos');

    }

}
