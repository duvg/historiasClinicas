<?php

namespace AppBundle\Controller;

use AppBundle\Entity\historiasnumber;
use AppBundle\Entity\Servicio;
use AppBundle\Entity\Test;
use AppBundle\Entity\Tratamiento;
use AppBundle\Entity\TipoTratamiento;
use AppBundle\Form\TratamientoType;
use AppBundle\Form\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Service\FileUploader;
/**
 * Class TratamientoController
 * @Route("/admin/tratamientos", name="tratamientos")
 */
class TratamientoController extends Controller
{
    /**
     * @Route("/", name="listar_tratamientos")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tratamientos = $em->getRepository('AppBundle:Tratamiento')->findTratamientosEnabled();

        return $this->render('AppBundle:Tratamientos:index.html.twig', array(
            'tratamientos' => $tratamientos
        ));
    }

    /**
     * @Route("/crear", name="crear_tratamiento")
     */
    public function crearAction(Request $request)
    {
        $tratamiento = new Tratamiento();
        $form = $this->createForm(TratamientoType::class, $tratamiento);


        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($tratamiento);
            $em->flush();

            $this->addFlash('success-save', 'Servicio creado satisfactoriamente!');

            return $this->redirectToRoute('listar_tratamientos');
        }

        return $this->render('AppBundle:Tratamientos:form.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
     * @Route("/editar/{id}", name="editar_tratamiento")
     */
    public function editarAction(Request $request, $id )
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $tratamientoRepo = $em->getRepository(Tratamiento::class)->find($id);

        if(!$tratamientoRepo){
            throw $this->createNotFoundException("No existe ningun servicio relacionado con el id ".$id);
        }

        $form = $this->createForm(TratamientoType::class, $tratamientoRepo);

        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted())
        {
            $em->flush();

            $this->addFlash('success_update', 'Servicio actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_tratamientos');
        }


        return $this->render('AppBundle:Tratamientos:form.html.twig', array(
            'servicio' => $tratamientoRepo,
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/eliminar/{id}", name="eliminar_tratamiento")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        //busqueda del regitro
        $em = $this->getDoctrine()->getManager();
        $tratamientoRepo = $em->getRepository(Tratamiento::class)->findOneBy(array(
            'id' => $id
        ));
        if(!$tratamientoRepo){
            throw $this->createNotFoundException("No existe ningun servicio relacionado con el id ".$id);
        }

        $em->remove($tratamientoRepo);

        $em->flush();
        return $this->redirectToRoute('listar_tratamientos');

    }


    /**
     * retorna el valor del tratamiento
     * @Route("/valor", name="valor_tratamiento")
     */
    public function valorAction(Request $request)
    {
        $id = $request->get("id");
        //entity manager
        $em = $this->getDoctrine()->getManager();
        $tratamientoRepo = $em->getRepository('AppBundle:Tratamiento')->find($id);

        return new Response($tratamientoRepo->getPrecio());
    }

}
