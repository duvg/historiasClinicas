<?php
/**
 * Created by Duviel Garcia.
 * Date: 15/11/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\EditType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="listar_usuarios")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('AppBundle:User')->findAllEnabled();
        return $this->render("AppBundle:Users:index.html.twig", array(
            'usuarios' => $userRepo
        ));
    }

    public function exitAction(){	

    	die();
    	
    }
    public function generateUrl($route, $parameters = array(), $absolute = false)
    {
        return $this->container->get('router')->generate($route, $parameters, $absolute);
    }

    /**
     * @Route("/editar/{id}", name="editar_usuario")
     */
    public function editarAction(Request $request, $id)
    {

        $user = new User();
        $userManager = $this->get('fos_user.user_manager');

        $userRepo = $userManager->findUserBy(array('id'=>$id));
        $form = $this->createForm(EditType::class, $userRepo );

        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted())
        {
            $em = $this->getDoctrine()->getManager();

            $userManager->updateUser($userRepo);

            $this->addFlash('success-save', 'Usuario actualizado satisfactoriamente!');
            return $this->redirectToRoute('listar_usuarios');
        }


        return $this->render("AppBundle:Users:EditFormUsers.html.twig", array(
            'form' => $form->createView()
        ));

    }


    /**
     * @Route("/eliminar/{id}", name="eliminar_usuario")
     */
    public function eliminarAction($id)
    {
        if(!$id){
            throw $this->createNotFoundException('Error!, No se encuetra un registro relacionado');
        }
        $userManager = $this->get('fos_user.user_manager');

        $userRepo = $userManager->findUserBy(array('id'=>$id));

        if(!$userRepo){
            throw $this->createNotFoundException("No existe ningun usuario relacionado con el id ".$id);
        }

        $userRepo->setEnabled(false);
        $userManager->updateUser($userRepo);
        $this->addFlash('success_update', 'Usuario eliminado satisfactoriamente!');
        return $this->redirectToRoute('listar_usuarios');
    }
}