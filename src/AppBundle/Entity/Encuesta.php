<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Encuesta
 *
 * @ORM\Table(name="encuesta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EncuestaRepository")
 */
class Encuesta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=80)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=100)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=12)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta1", type="text")
     */
    private $respuesta1;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta2", type="text")
     */
    private $respuesta2;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta3", type="text")
     */
    private $respuesta3;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta4", type="string", length=20)
     */
    private $respuesta4;

    /**
     * @var string
     *
     * @ORM\Column(name="respuesta5", type="string", length=30)
     */
    private $respuesta5;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Encuesta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Encuesta
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Encuesta
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Encuesta
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set respuesta1
     *
     * @param string $respuesta1
     *
     * @return Encuesta
     */
    public function setRespuesta1($respuesta1)
    {
        $this->respuesta1 = $respuesta1;

        return $this;
    }

    /**
     * Get respuesta1
     *
     * @return string
     */
    public function getRespuesta1()
    {
        return $this->respuesta1;
    }

    /**
     * Set respuesta2
     *
     * @param string $respuesta2
     *
     * @return Encuesta
     */
    public function setRespuesta2($respuesta2)
    {
        $this->respuesta2 = $respuesta2;

        return $this;
    }

    /**
     * Get respuesta2
     *
     * @return string
     */
    public function getRespuesta2()
    {
        return $this->respuesta2;
    }

    /**
     * Set respuesta3
     *
     * @param string $respuesta3
     *
     * @return Encuesta
     */
    public function setRespuesta3($respuesta3)
    {
        $this->respuesta3 = $respuesta3;

        return $this;
    }

    /**
     * Get respuesta3
     *
     * @return string
     */
    public function getRespuesta3()
    {
        return $this->respuesta3;
    }

    /**
     * Set respuesta4
     *
     * @param string $respuesta4
     *
     * @return Encuesta
     */
    public function setRespuesta4($respuesta4)
    {
        $this->respuesta4 = $respuesta4;

        return $this;
    }

    /**
     * Get respuesta4
     *
     * @return string
     */
    public function getRespuesta4()
    {
        return $this->respuesta4;
    }

    /**
     * Set respuesta5
     *
     * @param string $respuesta5
     *
     * @return Encuesta
     */
    public function setRespuesta5($respuesta5)
    {
        $this->respuesta5 = $respuesta5;

        return $this;
    }

    /**
     * Get respuesta5
     *
     * @return string
     */
    public function getRespuesta5()
    {
        return $this->respuesta5;
    }
}
