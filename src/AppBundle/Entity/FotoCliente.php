<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotoCliente
 *
 * @ORM\Table(name="foto_cliente")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FotoClienteRepository")
 */
class FotoCliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cliente", type="string", length=255)
     */
    private $cliente;

    /**
     * @var string
     *
     * @ORM\Column(name="rutaFoto", type="string", length=255)
     */
    private $rutaFoto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFoto", type="datetime")
     */
    private $fechaFoto;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cliente
     *
     * @param string $cliente
     *
     * @return FotoCliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return string
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set rutaFoto
     *
     * @param string $rutaFoto
     *
     * @return FotoCliente
     */
    public function setRutaFoto($rutaFoto)
    {
        $this->rutaFoto = $rutaFoto;

        return $this;
    }

    /**
     * Get rutaFoto
     *
     * @return string
     */
    public function getRutaFoto()
    {
        return $this->rutaFoto;
    }

    /**
     * Set fechaFoto
     *
     * @param \DateTime $fechaFoto
     *
     * @return FotoCliente
     */
    public function setFechaFoto($fechaFoto)
    {
        $this->fechaFoto = $fechaFoto;

        return $this;
    }

    /**
     * Get fechaFoto
     *
     * @return \DateTime
     */
    public function getFechaFoto()
    {
        return $this->fechaFoto;
    }
}
