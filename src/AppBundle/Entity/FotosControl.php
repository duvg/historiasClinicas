<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotosControl
 *
 * @ORM\Table(name="fotos_control")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FotosControlRepository")
 */
class FotosControl
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="control", type="string", length=255)
     */
    private $control;

    /**
     * @var string
     *
     * @ORM\Column(name="rutaFoto", type="text")
     */
    private $rutaFoto;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set control
     *
     * @param string $control
     *
     * @return FotosControl
     */
    public function setControl($control)
    {
        $this->control = $control;

        return $this;
    }

    /**
     * Get control
     *
     * @return string
     */
    public function getControl()
    {
        return $this->control;
    }

    /**
     * Set rutaFoto
     *
     * @param string $rutaFoto
     *
     * @return FotosControl
     */
    public function setRutaFoto($rutaFoto)
    {
        $this->rutaFoto = $rutaFoto;

        return $this;
    }

    /**
     * Get rutaFoto
     *
     * @return string
     */
    public function getRutaFoto()
    {
        return $this->rutaFoto;
    }
}
