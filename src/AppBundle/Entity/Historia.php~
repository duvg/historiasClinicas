<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historia
 *
 * @ORM\Table(name="historias")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HistoriaRepository")
 */
class Historia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="consecutivo", type="string", length=12)
     */
    private $consecutivo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaControl", type="datetime")
     */
    private $fechaControl;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Paciente", inversedBy="historias")
     * @ORM\JoinColumn(name="paciente_id",referencedColumnName="id", onDelete="NO ACTION")
     */

    private $paciente;

    /**
     * @var string
     *
     * @ORM\Column(name="motivoconsulta", type="text")
     */
    private $motivoconsulta;

    /**
     * @var string
     *
     * @ORM\Column(name="enfermedadactual", type="text")
     */
    private $enfermedadactual;

    /**
     * @var bool
     *
     * @ORM\Column(name="antalcohol", type="boolean")
     */
    private $antalcohol;

    /**
     * @var bool
     *
     * @ORM\Column(name="antcigarrillos", type="boolean")
     */
    private $antcigarrillos;

    /**
     * @var string
     *
     * @ORM\Column(name="otros", type="string", length=255)
     */
    private $otros;

    /**
     * @var string
     *
     * @ORM\Column(name="patologicos", type="string", length=255)
     */
    private $patologicos;

    /**
     * @var string
     *
     * @ORM\Column(name="ginecologicos", type="string", length=2)
     */
    private $ginecologicos;

    /**
     * @var string
     *
     * @ORM\Column(name="quirurgicos", type="string", length=255)
     */
    private $quirurgicos;

    /**
     * @var string
     *
     * @ORM\Column(name="alergicos", type="string", length=255)
     */
    private $alergicos;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="menarquia", type="datetime")
     */
    private $menarquia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fum", type="datetime")
     */
    private $fum;

    /**
     * @var string
     *
     * @ORM\Column(name="TA", type="datetime")
     */
    private $tA;

    /**
     * @var string
     *
     * @ORM\Column(name="fcardiaca", type="string", length=5)
     */
    private $fcardiaca;

    /**
     * @var string
     *
     * @ORM\Column(name="frespiratoria", type="string", length=5)
     */
    private $frespiratoria;

    /**
     * @var string
     *
     * @ORM\Column(name="temperaturac", type="string", length=3)
     */
    private $temperaturac;

    /**
     * @var int
     *
     * @ORM\Column(name="peso", type="string", length=10)
     */
    private $peso;

    /**
     * @var int
     *
     * @ORM\Column(name="talla", type="string", length=10)
     */
    private $talla;

    /**
     * @var string
     *
     * @ORM\Column(name="imc", type="string", length=20)
     */
    private $imc;

    /**
     * @var string
     *
     * @ORM\Column(name="estadogeneral", type="string", length=255)
     */
    private $estadogeneral;

    /**
     * @var string
     *
     * @ORM\Column(name="cabeza", type="string", length=255)
     */
    private $cabeza;

    /**
     * @var string
     *
     * @ORM\Column(name="ojos", type="string", length=255)
     */
    private $ojos;

    /**
     * @var string
     *
     * @ORM\Column(name="boca", type="string", length=255)
     */
    private $boca;

    /**
     * @var string
     *
     * @ORM\Column(name="cuello", type="string", length=255)
     */
    private $cuello;

    /**
     * @var string
     *
     * @ORM\Column(name="torax", type="string", length=255)
     */
    private $torax;

    /**
     * @var string
     *
     * @ORM\Column(name="cardiovascular", type="string", length=255)
     */
    private $cardiovascular;

    /**
     * @var string
     *
     * @ORM\Column(name="abdomen", type="string", length=255)
     */
    private $abdomen;

    /**
     * @var string
     *
     * @ORM\Column(name="extremidades", type="string", length=255)
     */
    private $extremidades;

    /**
     * @var string
     *
     * @ORM\Column(name="g_u", type="string", length=255)
     */
    private $gU;

    /**
     * @var string
     *
     * @ORM\Column(name="neurologico", type="string", length=255)
     */
    private $neurologico;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cie10")
     */
    private $iDX1;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cie10")
     */
    private $iDX2;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="Notadevolucion", mappedBy="historia")
     */
    private $notasdevolucion;

    /**
     * @var string
     *
     * @ORM\Column(name="plantratamiento", type="text")
     */
    private $plantratamiento;

    /**
     * @var bool
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado = true;

    public function __construct()
    {
        $this->fecha = new \DateTime();

    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consecutivo
     *
     * @param string $consecutivo
     *
     * @return Historia
     */
    public function setConsecutivo($consecutivo)
    {
        $this->consecutivo = $consecutivo;

        return $this;
    }

    /**
     * Get consecutivo
     *
     * @return string
     */
    public function getConsecutivo()
    {
        return $this->consecutivo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Historia
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set motivoconsulta
     *
     * @param string $motivoconsulta
     *
     * @return Historia
     */
    public function setMotivoconsulta($motivoconsulta)
    {
        $this->motivoconsulta = $motivoconsulta;

        return $this;
    }

    /**
     * Get motivoconsulta
     *
     * @return string
     */
    public function getMotivoconsulta()
    {
        return $this->motivoconsulta;
    }

    /**
     * Set enfermedadactual
     *
     * @param string $enfermedadactual
     *
     * @return Historia
     */
    public function setEnfermedadactual($enfermedadactual)
    {
        $this->enfermedadactual = $enfermedadactual;

        return $this;
    }

    /**
     * Get enfermedadactual
     *
     * @return string
     */
    public function getEnfermedadactual()
    {
        return $this->enfermedadactual;
    }

    /**
     * Set antalcohol
     *
     * @param boolean $antalcohol
     *
     * @return Historia
     */
    public function setAntalcohol($antalcohol)
    {
        $this->antalcohol = $antalcohol;

        return $this;
    }

    /**
     * Get antalcohol
     *
     * @return boolean
     */
    public function getAntalcohol()
    {
        return $this->antalcohol;
    }

    /**
     * Set antcigarrillos
     *
     * @param boolean $antcigarrillos
     *
     * @return Historia
     */
    public function setAntcigarrillos($antcigarrillos)
    {
        $this->antcigarrillos = $antcigarrillos;

        return $this;
    }

    /**
     * Get antcigarrillos
     *
     * @return boolean
     */
    public function getAntcigarrillos()
    {
        return $this->antcigarrillos;
    }

    /**
     * Set otros
     *
     * @param string $otros
     *
     * @return Historia
     */
    public function setOtros($otros)
    {
        $this->otros = $otros;

        return $this;
    }

    /**
     * Get otros
     *
     * @return string
     */
    public function getOtros()
    {
        return $this->otros;
    }

    /**
     * Set patologicos
     *
     * @param string $patologicos
     *
     * @return Historia
     */
    public function setPatologicos($patologicos)
    {
        $this->patologicos = $patologicos;

        return $this;
    }

    /**
     * Get patologicos
     *
     * @return string
     */
    public function getPatologicos()
    {
        return $this->patologicos;
    }

    /**
     * Set ginecologicos
     *
     * @param string $ginecologicos
     *
     * @return Historia
     */
    public function setGinecologicos($ginecologicos)
    {
        $this->ginecologicos = $ginecologicos;

        return $this;
    }

    /**
     * Get ginecologicos
     *
     * @return string
     */
    public function getGinecologicos()
    {
        return $this->ginecologicos;
    }

    /**
     * Set quirurgicos
     *
     * @param string $quirurgicos
     *
     * @return Historia
     */
    public function setQuirurgicos($quirurgicos)
    {
        $this->quirurgicos = $quirurgicos;

        return $this;
    }

    /**
     * Get quirurgicos
     *
     * @return string
     */
    public function getQuirurgicos()
    {
        return $this->quirurgicos;
    }

    /**
     * Set alergicos
     *
     * @param string $alergicos
     *
     * @return Historia
     */
    public function setAlergicos($alergicos)
    {
        $this->alergicos = $alergicos;

        return $this;
    }

    /**
     * Get alergicos
     *
     * @return string
     */
    public function getAlergicos()
    {
        return $this->alergicos;
    }

    /**
     * Set menarquia
     *
     * @param \DateTime $menarquia
     *
     * @return Historia
     */
    public function setMenarquia($menarquia)
    {
        $this->menarquia = $menarquia;

        return $this;
    }

    /**
     * Get menarquia
     *
     * @return \DateTime
     */
    public function getMenarquia()
    {
        return $this->menarquia;
    }

    /**
     * Set fum
     *
     * @param \DateTime $fum
     *
     * @return Historia
     */
    public function setFum($fum)
    {
        $this->fum = $fum;

        return $this;
    }

    /**
     * Get fum
     *
     * @return \DateTime
     */
    public function getFum()
    {
        return $this->fum;
    }

    /**
     * Set tA
     *
     * @param \DateTime $tA
     *
     * @return Historia
     */
    public function setTA($tA)
    {
        $this->tA = $tA;

        return $this;
    }

    /**
     * Get tA
     *
     * @return \DateTime
     */
    public function getTA()
    {
        return $this->tA;
    }

    /**
     * Set fcardiaca
     *
     * @param string $fcardiaca
     *
     * @return Historia
     */
    public function setFcardiaca($fcardiaca)
    {
        $this->fcardiaca = $fcardiaca;

        return $this;
    }

    /**
     * Get fcardiaca
     *
     * @return string
     */
    public function getFcardiaca()
    {
        return $this->fcardiaca;
    }

    /**
     * Set frespiratoria
     *
     * @param string $frespiratoria
     *
     * @return Historia
     */
    public function setFrespiratoria($frespiratoria)
    {
        $this->frespiratoria = $frespiratoria;

        return $this;
    }

    /**
     * Get frespiratoria
     *
     * @return string
     */
    public function getFrespiratoria()
    {
        return $this->frespiratoria;
    }

    /**
     * Set temperaturac
     *
     * @param string $temperaturac
     *
     * @return Historia
     */
    public function setTemperaturac($temperaturac)
    {
        $this->temperaturac = $temperaturac;

        return $this;
    }

    /**
     * Get temperaturac
     *
     * @return string
     */
    public function getTemperaturac()
    {
        return $this->temperaturac;
    }

    /**
     * Set peso
     *
     * @param string $peso
     *
     * @return Historia
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return string
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set talla
     *
     * @param string $talla
     *
     * @return Historia
     */
    public function setTalla($talla)
    {
        $this->talla = $talla;

        return $this;
    }

    /**
     * Get talla
     *
     * @return string
     */
    public function getTalla()
    {
        return $this->talla;
    }

    /**
     * Set imc
     *
     * @param string $imc
     *
     * @return Historia
     */
    public function setImc($imc)
    {
        $this->imc = $imc;

        return $this;
    }

    /**
     * Get imc
     *
     * @return string
     */
    public function getImc()
    {
        return $this->imc;
    }

    /**
     * Set estadogeneral
     *
     * @param string $estadogeneral
     *
     * @return Historia
     */
    public function setEstadogeneral($estadogeneral)
    {
        $this->estadogeneral = $estadogeneral;

        return $this;
    }

    /**
     * Get estadogeneral
     *
     * @return string
     */
    public function getEstadogeneral()
    {
        return $this->estadogeneral;
    }

    /**
     * Set cabeza
     *
     * @param string $cabeza
     *
     * @return Historia
     */
    public function setCabeza($cabeza)
    {
        $this->cabeza = $cabeza;

        return $this;
    }

    /**
     * Get cabeza
     *
     * @return string
     */
    public function getCabeza()
    {
        return $this->cabeza;
    }

    /**
     * Set ojos
     *
     * @param string $ojos
     *
     * @return Historia
     */
    public function setOjos($ojos)
    {
        $this->ojos = $ojos;

        return $this;
    }

    /**
     * Get ojos
     *
     * @return string
     */
    public function getOjos()
    {
        return $this->ojos;
    }

    /**
     * Set boca
     *
     * @param string $boca
     *
     * @return Historia
     */
    public function setBoca($boca)
    {
        $this->boca = $boca;

        return $this;
    }

    /**
     * Get boca
     *
     * @return string
     */
    public function getBoca()
    {
        return $this->boca;
    }

    /**
     * Set cuello
     *
     * @param string $cuello
     *
     * @return Historia
     */
    public function setCuello($cuello)
    {
        $this->cuello = $cuello;

        return $this;
    }

    /**
     * Get cuello
     *
     * @return string
     */
    public function getCuello()
    {
        return $this->cuello;
    }

    /**
     * Set torax
     *
     * @param string $torax
     *
     * @return Historia
     */
    public function setTorax($torax)
    {
        $this->torax = $torax;

        return $this;
    }

    /**
     * Get torax
     *
     * @return string
     */
    public function getTorax()
    {
        return $this->torax;
    }

    /**
     * Set cardiovascular
     *
     * @param string $cardiovascular
     *
     * @return Historia
     */
    public function setCardiovascular($cardiovascular)
    {
        $this->cardiovascular = $cardiovascular;

        return $this;
    }

    /**
     * Get cardiovascular
     *
     * @return string
     */
    public function getCardiovascular()
    {
        return $this->cardiovascular;
    }

    /**
     * Set abdomen
     *
     * @param string $abdomen
     *
     * @return Historia
     */
    public function setAbdomen($abdomen)
    {
        $this->abdomen = $abdomen;

        return $this;
    }

    /**
     * Get abdomen
     *
     * @return string
     */
    public function getAbdomen()
    {
        return $this->abdomen;
    }

    /**
     * Set extremidades
     *
     * @param string $extremidades
     *
     * @return Historia
     */
    public function setExtremidades($extremidades)
    {
        $this->extremidades = $extremidades;

        return $this;
    }

    /**
     * Get extremidades
     *
     * @return string
     */
    public function getExtremidades()
    {
        return $this->extremidades;
    }

    /**
     * Set gU
     *
     * @param string $gU
     *
     * @return Historia
     */
    public function setGU($gU)
    {
        $this->gU = $gU;

        return $this;
    }

    /**
     * Get gU
     *
     * @return string
     */
    public function getGU()
    {
        return $this->gU;
    }

    /**
     * Set neurologico
     *
     * @param string $neurologico
     *
     * @return Historia
     */
    public function setNeurologico($neurologico)
    {
        $this->neurologico = $neurologico;

        return $this;
    }

    /**
     * Get neurologico
     *
     * @return string
     */
    public function getNeurologico()
    {
        return $this->neurologico;
    }

    /**
     * Set plantratamiento
     *
     * @param string $plantratamiento
     *
     * @return Historia
     */
    public function setPlantratamiento($plantratamiento)
    {
        $this->plantratamiento = $plantratamiento;

        return $this;
    }

    /**
     * Get plantratamiento
     *
     * @return string
     */
    public function getPlantratamiento()
    {
        return $this->plantratamiento;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Historia
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set paciente
     *
     * @param \AppBundle\Entity\Paciente $paciente
     *
     * @return Historia
     */
    public function setPaciente(\AppBundle\Entity\Paciente $paciente = null)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \AppBundle\Entity\Paciente
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set iDX1
     *
     * @param \AppBundle\Entity\Cie10 $iDX1
     *
     * @return Historia
     */
    public function setIDX1(\AppBundle\Entity\Cie10 $iDX1 = null)
    {
        $this->iDX1 = $iDX1;

        return $this;
    }

    /**
     * Get iDX1
     *
     * @return \AppBundle\Entity\Cie10
     */
    public function getIDX1()
    {
        return $this->iDX1;
    }

    /**
     * Set iDX2
     *
     * @param \AppBundle\Entity\Cie10 $iDX2
     *
     * @return Historia
     */
    public function setIDX2(\AppBundle\Entity\Cie10 $iDX2 = null)
    {
        $this->iDX2 = $iDX2;

        return $this;
    }

    /**
     * Get iDX2
     *
     * @return \AppBundle\Entity\Cie10
     */
    public function getIDX2()
    {
        return $this->iDX2;
    }



    /**
     * Add notasdevolucion
     *
     * @param \AppBundle\Entity\Notadevolucion $notasdevolucion
     *
     * @return Historia
     */
    public function addNotasdevolucion(\AppBundle\Entity\Notadevolucion $notasdevolucion)
    {
        $this->notasdevolucion[] = $notasdevolucion;

        return $this;
    }

    /**
     * Remove notasdevolucion
     *
     * @param \AppBundle\Entity\Notadevolucion $notasdevolucion
     */
    public function removeNotasdevolucion(\AppBundle\Entity\Notadevolucion $notasdevolucion)
    {
        $this->notasdevolucion->removeElement($notasdevolucion);
    }

    /**
     * Get notasdevolucion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotasdevolucion()
    {
        return $this->notasdevolucion;
    }

    public function __toString()
    {
        return $this->consecutivo;
    }

    /**
     * Set fechaControl
     *
     * @param \DateTime $fechaControl
     *
     * @return Historia
     */
    public function setFechaControl($fechaControl)
    {
        $this->fechaControl = $fechaControl;

        return $this;
    }

    /**
     * Get fechaControl
     *
     * @return \DateTime
     */
    public function getFechaControl()
    {
        return $this->fechaControl;
    }
}
