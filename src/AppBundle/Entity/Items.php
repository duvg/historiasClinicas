<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoTratamiento
 *
 * @ORM\Table(name="items")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemsRepository")
 */
class Items
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=120)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2)
     */
    private $precio;

    /**
     * @var integer
     * @ORM\Column(name="code", type="integer")
     */
    private $code;

    /**
     * @var integer
     * @ORM\Column(name="cantidad", type="integer", nullable=true)
     */
    private $cantidad;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidadSesiones", type="integer")
     */
    private $cantidadSesiones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    private $fechaInicio;




    /**
     * @var integer
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Factura", inversedBy="items")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $facturas;


    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoItem", inversedBy="items")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $tipoitem;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->facturas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoTratamiento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return TipoTratamiento
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set cantidadSesiones
     *
     * @param integer $cantidadSesiones
     *
     * @return TipoTratamiento
     */
    public function setCantidadSesiones($cantidadSesiones)
    {
        $this->cantidadSesiones = $cantidadSesiones;

        return $this;
    }

    /**
     * Get cantidadSesiones
     *
     * @return integer
     */
    public function getCantidadSesiones()
    {
        return $this->cantidadSesiones;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     *
     * @return TipoTratamiento
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Add factura
     *
     * @param \AppBundle\Entity\Factura $factura
     *
     * @return TipoTratamiento
     */
    public function addFactura(\AppBundle\Entity\Factura $factura)
    {
        $this->facturas[] = $factura;

        return $this;
    }

    /**
     * Remove factura
     *
     * @param \AppBundle\Entity\Factura $factura
     */
    public function removeFactura(\AppBundle\Entity\Factura $factura)
    {
        $this->facturas->removeElement($factura);
    }

    /**
     * Get facturas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacturas()
    {
        return $this->facturas;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Items
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set tipoitem
     *
     * @param \AppBundle\Entity\TipoItem $tipoitem
     *
     * @return Items
     */
    public function setTipoitem(\AppBundle\Entity\TipoItem $tipoitem = null)
    {
        $this->tipoitem = $tipoitem;

        return $this;
    }

    /**
     * Get tipoitem
     *
     * @return \AppBundle\Entity\TipoItem
     */
    public function getTipoitem()
    {
        return $this->tipoitem;
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return Items
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }
}
