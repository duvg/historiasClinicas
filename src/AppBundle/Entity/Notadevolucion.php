<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notadevelucion
 *
 * @ORM\Table(name="notadevolucion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotadevolucionRepository")
 */
class Notadevolucion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Historia", inversedBy="notasdevolucion")
     * @ORM\JoinColumn(referencedColumnName="id", name="historia_id")
     */
    private $historia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="notas", type="text")
     */
    private $notas;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }





    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Notadevolucion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set notas
     *
     * @param string $notas
     *
     * @return Notadevolucion
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;

        return $this;
    }

    /**
     * Get notas
     *
     * @return string
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * Set historia
     *
     * @param \AppBundle\Entity\Historia $historia
     *
     * @return Notadevolucion
     */
    public function setHistoria(\AppBundle\Entity\Historia $historia = null)
    {
        $this->historia = $historia;

        return $this;
    }

    /**
     * Get historia
     *
     * @return \AppBundle\Entity\Historia
     */
    public function getHistoria()
    {
        return $this->historia;
    }
}
