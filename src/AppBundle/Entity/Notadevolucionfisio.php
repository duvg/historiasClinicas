<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notadevelucion
 *
 * @ORM\Table(name="notadevolucionfisio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotadevolucionfisioRepository")
 */
class Notadevolucionfisio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\HistoriaFisioterapia", inversedBy="notasdevolucion")
     * @ORM\JoinColumn(referencedColumnName="id", name="historia_id")
     */
    private $historia;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="notas", type="text")
     */
    private $notas;


    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Notadevolucionfisio
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set notas
     *
     * @param string $notas
     *
     * @return Notadevolucionfisio
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;

        return $this;
    }

    /**
     * Get notas
     *
     * @return string
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * Set historia
     *
     * @param \AppBundle\Entity\HistoriaFisioterapia $historia
     *
     * @return Notadevolucionfisio
     */
    public function setHistoria(\AppBundle\Entity\HistoriaFisioterapia $historia = null)
    {
        $this->historia = $historia;

        return $this;
    }

    /**
     * Get historia
     *
     * @return \AppBundle\Entity\HistoriaFisioterapia
     */
    public function getHistoria()
    {
        return $this->historia;
    }
}
