<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planes
 *
 * @ORM\Table(name="planes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlanesRepository")
 */
class Planes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="string", length=20)
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="descuento", type="string", length=3)
     */
    private $descuento;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Planes", mappedBy="plan")
     */
    private $afiliados;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Paciente", mappedBy="plan")
     */
    private $pacientes;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Planes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set valor
     *
     * @param string $valor
     *
     * @return Planes
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set descuento
     *
     * @param string $descuento
     *
     * @return Planes
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return string
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    public function __toString()
    {
        return $this->nombre.' $'.$this->valor;
    }
}

