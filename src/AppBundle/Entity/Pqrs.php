<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pqrs
 *
 * @ORM\Table(name="pqrs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PqrsRepository")
 */
class Pqrs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=200)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoDocuemnto", type="string", length=4)
     */
    private $tipoDocuemnto;

    /**
     * @var string
     *
     * @ORM\Column(name="nidenficacion", type="string", length=20)
     */
    private $nidenficacion;

    /**
     * @var bool
     *
     * @ORM\Column(name="afiliado", type="boolean")
     */
    private $afiliado;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Departamento")
     * @ORM\JoinColumn(name="departamento_id", referencedColumnName="id")
     */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Municipio")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id")
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=15, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="celular", type="string", length=15)
     */
    private $celular;

    /**
     * @var string
     *
     * @ORM\Column(name="lugarServicio", type="string", length=200)
     */
    private $lugarServicio;

    /**
     * @var string
     *
     * @ORM\Column(name="funcionario", type="string", length=200, nullable=true)
     */
    private $funcionario;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="TipoPqrs")
     * @ORM\JoinColumn(name="tipopqrs_id", referencedColumnName="id")
     */
    private $tiposolicitud;

    /**
     * @var string
     *
     * @ORM\Column(name="asunto", type="string", length=200)
     */
    private $asunto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;




    /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="text")
     */
    private $mensaje;


   


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Pqrs
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipoDocuemnto
     *
     * @param string $tipoDocuemnto
     *
     * @return Pqrs
     */
    public function setTipoDocuemnto($tipoDocuemnto)
    {
        $this->tipoDocuemnto = $tipoDocuemnto;

        return $this;
    }

    /**
     * Get tipoDocuemnto
     *
     * @return string
     */
    public function getTipoDocuemnto()
    {
        return $this->tipoDocuemnto;
    }

    /**
     * Set nidenficacion
     *
     * @param string $nidenficacion
     *
     * @return Pqrs
     */
    public function setNidenficacion($nidenficacion)
    {
        $this->nidenficacion = $nidenficacion;

        return $this;
    }

    /**
     * Get nidenficacion
     *
     * @return string
     */
    public function getNidenficacion()
    {
        return $this->nidenficacion;
    }

    /**
     * Set afiliado
     *
     * @param boolean $afiliado
     *
     * @return Pqrs
     */
    public function setAfiliado($afiliado)
    {
        $this->afiliado = $afiliado;

        return $this;
    }

    /**
     * Get afiliado
     *
     * @return boolean
     */
    public function getAfiliado()
    {
        return $this->afiliado;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Pqrs
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Pqrs
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Pqrs
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set celular
     *
     * @param string $celular
     *
     * @return Pqrs
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set lugarServicio
     *
     * @param string $lugarServicio
     *
     * @return Pqrs
     */
    public function setLugarServicio($lugarServicio)
    {
        $this->lugarServicio = $lugarServicio;

        return $this;
    }

    /**
     * Get lugarServicio
     *
     * @return string
     */
    public function getLugarServicio()
    {
        return $this->lugarServicio;
    }

    /**
     * Set funcionario
     *
     * @param string $funcionario
     *
     * @return Pqrs
     */
    public function setFuncionario($funcionario)
    {
        $this->funcionario = $funcionario;

        return $this;
    }

    /**
     * Get funcionario
     *
     * @return string
     */
    public function getFuncionario()
    {
        return $this->funcionario;
    }

    /**
     * Set asunto
     *
     * @param string $asunto
     *
     * @return Pqrs
     */
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;

        return $this;
    }

    /**
     * Get asunto
     *
     * @return string
     */
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Pqrs
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     *
     * @return Pqrs
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;

        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set departamento
     *
     * @param \AppBundle\Entity\Departamento $departamento
     *
     * @return Pqrs
     */
    public function setDepartamento(\AppBundle\Entity\Departamento $departamento = null)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return \AppBundle\Entity\Departamento
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set municipio
     *
     * @param \AppBundle\Entity\Municipio $municipio
     *
     * @return Pqrs
     */
    public function setMunicipio(\AppBundle\Entity\Municipio $municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return \AppBundle\Entity\Municipio
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set tiposolicitud
     *
     * @param \AppBundle\Entity\TipoPqrs $tiposolicitud
     *
     * @return Pqrs
     */
    public function setTiposolicitud(\AppBundle\Entity\TipoPqrs $tiposolicitud = null)
    {
        $this->tiposolicitud = $tiposolicitud;

        return $this;
    }

    /**
     * Get tiposolicitud
     *
     * @return \AppBundle\Entity\TipoPqrs
     */
    public function getTiposolicitud()
    {
        return $this->tiposolicitud;
    }
}
