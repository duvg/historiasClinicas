<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procedimiento
 *
 * @ORM\Table(name="procedimiento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProcedimientoRepository")
 */
class Procedimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Paciente")
     * @ORM\JoinColumn(name="paciente_id", referencedColumnName="id")
     */
    private $paciente;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoTratamiento")
     * @ORM\JoinColumn(name="tipo_tratamiento_id", referencedColumnName="id")
     */
    private $tipoTratamiento;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tratamiento")
     * @ORM\JoinColumn(name="tratamiento_id", referencedColumnName="id")
     */
    private $Tratamiento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCtrl", type="datetime", nullable=true)
     */
    private $fechaCtrl;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = true;

    /**
     * @var string
     *
     * @ORM\Column(name="notas", type="text", nullable=true)
     */
    private $notas;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="procedimientos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $usuario;





    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Procedimiento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set fechaCtrl
     *
     * @param \DateTime $fechaCtrl
     *
     * @return Procedimiento
     */
    public function setFechaCtrl($fechaCtrl)
    {
        $this->fechaCtrl = $fechaCtrl;

        return $this;
    }

    /**
     * Get fechaCtrl
     *
     * @return \DateTime
     */
    public function getFechaCtrl()
    {
        return $this->fechaCtrl;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Procedimiento
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set paciente
     *
     * @param \AppBundle\Entity\Paciente $paciente
     *
     * @return Procedimiento
     */
    public function setPaciente(\AppBundle\Entity\Paciente $paciente = null)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \AppBundle\Entity\Paciente
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set tratamiento
     *
     * @param \AppBundle\Entity\Tratamiento $tratamiento
     *
     * @return Procedimiento
     */
    public function setTratamiento(\AppBundle\Entity\Tratamiento $tratamiento = null)
    {
        $this->Tratamiento = $tratamiento;

        return $this;
    }

    /**
     * Get tratamiento
     *
     * @return \AppBundle\Entity\Tratamiento
     */
    public function getTratamiento()
    {
        return $this->Tratamiento;
    }

    /**
     * Set tipotratamiento
     *
     * @param \AppBundle\Entity\TipoTratamiento $tipotratamiento
     *
     * @return Procedimiento
     */
    public function setTipotratamiento(\AppBundle\Entity\TipoTratamiento $tipotratamiento = null)
    {
        $this->tipoTratamiento = $tipotratamiento;

        return $this;
    }

    /**
     * Get tipotratamiento
     *
     * @return \AppBundle\Entity\TipoTratamiento
     */
    public function getTipotratamiento()
    {
        return $this->tipoTratamiento;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\User $usuario
     *
     * @return Procedimiento
     */
    public function setUsuario(\AppBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set notas
     *
     * @param string $notas
     *
     * @return Procedimiento
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;

        return $this;
    }

    /**
     * Get notas
     *
     * @return string
     */
    public function getNotas()
    {
        return $this->notas;
    }
}
