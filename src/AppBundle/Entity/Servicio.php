<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Servicios
 *
 * @ORM\Table(name="servicios")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiciosRepository")
 */
class Servicio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Image()
     */
    private $imagen;


    /**
     * @var boolean
     *
     * @ORM\Column(name="estado", type="boolean")
     */
    private $estado = true;

    /**
     * @var integer
     * @ORM\OneToMany(targetEntity="SolicitudServicio", mappedBy="servicio")
     */
    protected $solicitudServicio;

    public function __construct()
    {
        $this->solicitudServicio =  new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Servicios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Servicios
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     *
     * @return Servicios
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add solicitudServicio
     *
     * @param \AppBundle\Entity\SolicitudServicio $solicitudServicio
     *
     * @return Servicio
     */
    public function addSolicitudServicio(\AppBundle\Entity\SolicitudServicio $solicitudServicio)
    {
        $this->solicitudServicio[] = $solicitudServicio;

        return $this;
    }

    /**
     * Remove solicitudServicio
     *
     * @param \AppBundle\Entity\SolicitudServicio $solicitudServicio
     */
    public function removeSolicitudServicio(\AppBundle\Entity\SolicitudServicio $solicitudServicio)
    {
        $this->solicitudServicio->removeElement($solicitudServicio);
    }

    /**
     * Get solicitudServicio
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSolicitudServicio()
    {
        return $this->solicitudServicio;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
        return $this;
    }
}
