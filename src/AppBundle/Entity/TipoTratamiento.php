<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tratamientos
 *
 * @ORM\Table(name="tipoTratamientos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TipoTratamientoRepository")
 */
class TipoTratamiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Tratamiento", mappedBy="tipoTratamiento")
     */
    private $tratamientos;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Tratamiento", mappedBy="tipoTratamiento")
     */
    private $usuarios;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = true;


    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return TipoTratamiento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return TipoTratamiento
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function __toString()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tratamientos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return TipoTratamiento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add tratamiento
     *
     * @param \AppBundle\Entity\Tratamiento $tratamiento
     *
     * @return TipoTratamiento
     */
    public function addTratamiento(\AppBundle\Entity\Tratamiento $tratamiento)
    {
        $this->tratamientos[] = $tratamiento;

        return $this;
    }

    /**
     * Remove tratamiento
     *
     * @param \AppBundle\Entity\Tratamiento $tratamiento
     */
    public function removeTratamiento(\AppBundle\Entity\Tratamiento $tratamiento)
    {
        $this->tratamientos->removeElement($tratamiento);
    }

    /**
     * Get tratamientos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTratamientos()
    {
        return $this->tratamientos;
    }



    /**
     * Add usuario
     *
     * @param \AppBundle\Entity\Tratamiento $usuario
     *
     * @return TipoTratamiento
     */
    public function addUsuario(\AppBundle\Entity\Tratamiento $usuario)
    {
        $this->usuarios[] = $usuario;

        return $this;
    }

    /**
     * Remove usuario
     *
     * @param \AppBundle\Entity\Tratamiento $usuario
     */
    public function removeUsuario(\AppBundle\Entity\Tratamiento $usuario)
    {
        $this->usuarios->removeElement($usuario);
    }

    /**
     * Get usuarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarios()
    {
        return $this->usuarios;
    }
}
