<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=80, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=80, nullable=true)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="cedula", type="string", length=16, nullable=true)
     */
    private $cedula;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=14, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=80, nullable=true)
     */
    private $area;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Procedimiento", mappedBy="usuario")
     */
    private $procedimientos;



    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TipoTratamiento", inversedBy="usuarios")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $tipoTratamiento;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Afiliado", mappedBy="usuario")
     */
    private $afiliados;

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return User
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set cedula
     *
     * @param string $cedula
     *
     * @return User
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return string
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return User
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return User
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    

   
    /**
     * Add procedimiento
     *
     * @param \AppBundle\Entity\Procedimiento $procedimiento
     *
     * @return User
     */
    public function addProcedimiento(\AppBundle\Entity\Procedimiento $procedimiento)
    {
        $this->procedimientos[] = $procedimiento;

        return $this;
    }

    /**
     * Remove procedimiento
     *
     * @param \AppBundle\Entity\Procedimiento $procedimiento
     */
    public function removeProcedimiento(\AppBundle\Entity\Procedimiento $procedimiento)
    {
        $this->procedimientos->removeElement($procedimiento);
    }

    /**
     * Get procedimientos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProcedimientos()
    {
        return $this->procedimientos;
    }

    /**
     * Set tipoTratamiento
     *
     * @param \AppBundle\Entity\TipoTratamiento $tipoTratamiento
     *
     * @return User
     */
    public function setTipoTratamiento(\AppBundle\Entity\TipoTratamiento $tipoTratamiento = null)
    {
        $this->tipoTratamiento = $tipoTratamiento;

        return $this;
    }

    /**
     * Get tipoTratamiento
     *
     * @return \AppBundle\Entity\TipoTratamiento
     */
    public function getTipoTratamiento()
    {
        return $this->tipoTratamiento;
    }

    /**
     * Add afiliado
     *
     * @param \AppBundle\Entity\Afiliado $afiliado
     *
     * @return User
     */
    public function addAfiliado(\AppBundle\Entity\Afiliado $afiliado)
    {
        $this->afiliados[] = $afiliado;

        return $this;
    }

    /**
     * Remove afiliado
     *
     * @param \AppBundle\Entity\Afiliado $afiliado
     */
    public function removeAfiliado(\AppBundle\Entity\Afiliado $afiliado)
    {
        $this->afiliados->removeElement($afiliado);
    }

    /**
     * Get afiliados
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAfiliados()
    {
        return $this->afiliados;
    }
}
