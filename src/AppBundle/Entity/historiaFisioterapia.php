<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * historiaFisioterapia
 *
 * @ORM\Table(name="historia_fisioterapia")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\historiaFisioterapiaRepository")
 */
class historiaFisioterapia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Paciente", inversedBy="historias")
     * @ORM\JoinColumn(name="paciente_id",referencedColumnName="id", onDelete="NO ACTION")
     */

    private $paciente;

    /**
     * @var string
     *
     * @ORM\Column(name="consecutivo", type="string", length=255)
     */
    private $consecutivo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaControl", type="datetime", nullable=true )
     */
    private $fechaControl;


    /**
     * @var string
     *
     * @ORM\Column(name="nombreResponsable", type="string", length=170, nullable=true)
     */
    private $nombreResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="telefonoResponsable", type="string", length=12, nullable=true)
     */
    private $telefonoResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="eapb", type="string", length=40, nullable=true)
     */
    private $eapb;

    /**
     * @var string
     *
     * @ORM\Column(name="prepagada", type="string", length=40, nullable=true)
     */
    private $prepagada;

    /**
     * @var string
     *
     * @ORM\Column(name="planComplementarios", type="string", length=10, nullable=true)
     */
    private $planComplementarios;

    /**
     * @var string
     *
     * @ORM\Column(name="aseguradoras", type="string", length=40, nullable=true)
     */
    private $aseguradoras;

    /**
     * @var bool
     *
     * @ORM\Column(name="postquirurgico", type="boolean", nullable=true)
     */
    private $postquirurgico;

    /**
     * @var bool
     *
     * @ORM\Column(name="estetico", type="boolean", nullable=true)
     */
    private $estetico;

    /**
     * @var bool
     *
     * @ORM\Column(name="tonificar", type="boolean", nullable=true)
     */
    private $tonificar;

    /**
     * @var bool
     *
     * @ORM\Column(name="salud", type="boolean", nullable=true)
     */
    private $salud;

    /**
     * @var bool
     *
     * @ORM\Column(name="deportivo", type="boolean", nullable=true)
     */
    private $deportivo;

    /**
     * @var bool
     *
     * @ORM\Column(name="rehabilitacion", type="boolean", nullable=true)
     */
    private $rehabilitacion;

    /**
     * @var string
     *
     * @ORM\Column(name="patologicos", type="string", length=100, nullable=true)
     */
    private $patologicos;

    /**
     * @var string
     *
     * @ORM\Column(name="quirurgicos", type="string", length=100, nullable=true)
     */
    private $quirurgicos;

    /**
     * @var string
     *
     * @ORM\Column(name="gineFum", type="string", length=20, nullable=true)
     */
    private $gineFum;

    /**
     * @var bool
     *
     * @ORM\Column(name="gineG", type="boolean", nullable=true)
     */
    private $gineG;

    /**
     * @var bool
     *
     * @ORM\Column(name="gineP", type="boolean", nullable=true)
     */
    private $gineP;

    /**
     * @var bool
     *
     * @ORM\Column(name="gineC", type="boolean", nullable=true)
     */
    private $gineC;

    /**
     * @var bool
     *
     * @ORM\Column(name="gineV", type="boolean", nullable=true)
     */
    private $gineV;

    /**
     * @var bool
     *
     * @ORM\Column(name="gineA", type="boolean", nullable=true)
     */
    private $gineA;

    /**
     * @var string
     *
     * @ORM\Column(name="farmacologicos", type="string", length=100, nullable=true)
     */
    private $farmacologicos;

    /**
     * @var string
     *
     * @ORM\Column(name="osteomusculares", type="string", length=100, nullable=true)
     */
    private $osteomusculares;

    /**
     * @var string
     *
     * @ORM\Column(name="antecedentesfamiliates", type="string", length=100, nullable=true)
     */
    private $antecedentesfamiliates;

    /**
     * @var bool
     *
     * @ORM\Column(name="sedentario", type="boolean", nullable=true)
     */
    private $sedentario;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoEjercicio", type="string", length=40, nullable=true)
     */
    private $tipoEjercicio;

    /**
     * @var string
     *
     * @ORM\Column(name="frEjercicio", type="string", length=10, nullable=true)
     */
    private $frEjercicio;

    /**
     * @var string
     *
     * @ORM\Column(name="duracionEjercicio", type="string", length=10, nullable=true)
     */
    private $duracionEjercicio;

    /**
     * @var string
     *
     * @ORM\Column(name="intensidadEjercicio", type="string", length=10, nullable=true)
     */
    private $intensidadEjercicio;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoDeporte", type="string", length=40, nullable=true)
     */
    private $tipoDeporte;

    /**
     * @var string
     *
     * @ORM\Column(name="frDeporte", type="string", length=10, nullable=true)
     */
    private $frDeporte;

    /**
     * @var string
     *
     * @ORM\Column(name="duracionDeporte", type="string", length=10, nullable=true)
     */
    private $duracionDeporte;

    /**
     * @var string
     *
     * @ORM\Column(name="intensidadDeporte", type="string", length=10, nullable=true)
     */
    private $intensidadDeporte;

    /**
     * @var bool
     *
     * @ORM\Column(name="pregunta1chk", type="boolean", nullable=true)
     */
    private $pregunta1chk;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta1rp", type="string", length=30, nullable=true)
     */
    private $pregunta1rp;

    /**
     * @var bool
     *
     * @ORM\Column(name="pregunta2chk", type="boolean", nullable=true)
     */
    private $pregunta2chk;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta2rp", type="string", length=30, nullable=true)
     */
    private $pregunta2rp;

    /**
     * @var bool
     *
     * @ORM\Column(name="pregunta3chk", type="boolean", nullable=true)
     */
    private $pregunta3chk;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta3rp", type="string", length=80, nullable=true)
     */
    private $pregunta3rp;

    /**
     * @var bool
     *
     * @ORM\Column(name="pregunta4chk", type="boolean", nullable=true)
     */
    private $pregunta4chk;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta4rp", type="string", length=30, nullable=true)
     */
    private $pregunta4rp;

    /**
     * @var bool
     *
     * @ORM\Column(name="pregunta5chk", type="boolean", nullable=true)
     */
    private $pregunta5chk;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta5rp", type="string", length=30, nullable=true)
     */
    private $pregunta5rp;

    /**
     * @var bool
     *
     * @ORM\Column(name="pregunta6chk", type="boolean", nullable=true)
     */
    private $pregunta6chk;

    /**
     * @var string
     *
     * @ORM\Column(name="pregunta6rp", type="string", length=30, nullable=true)
     */
    private $pregunta6rp;

    /**
     * @var string
     *
     * @ORM\Column(name="tensionArterial", type="string", length=20)
     */
    private $tensionArterial;

    /**
     * @var string
     *
     * @ORM\Column(name="frecuenciaCardiaca", type="string", length=20)
     */
    private $frecuenciaCardiaca;

    /**
     * @var string
     *
     * @ORM\Column(name="talla", type="string", length=4)
     */
    private $talla;

    /**
     * @var string
     *
     * @ORM\Column(name="pesoKg", type="string", length=4)
     */
    private $pesoKg;

    /**
     * @var string
     *
     * @ORM\Column(name="indiceMasaCorporal", type="string", length=10, nullable=true)
     */
    private $indiceMasaCorporal;

    /**
     * @var string
     *
     * @ORM\Column(name="perimetroAbdominal", type="string", length=10, nullable=true)
     */
    private $perimetroAbdominal;

    /**
     * @var string
     *
     * @ORM\Column(name="grasa", type="string", length=4, nullable=true)
     */
    private $grasa;

    /**
     * @var string
     *
     * @ORM\Column(name="muscular", type="string", length=4, nullable=true)
     */
    private $muscular;

    /**
     * @var string
     *
     * @ORM\Column(name="se", type="string", length=4, nullable=true)
     */
    private $se;

    /**
     * @var string
     *
     * @ORM\Column(name="t", type="string", length=4, nullable=true)
     */
    private $t;

    /**
     * @var string
     *
     * @ORM\Column(name="si", type="string", length=4, nullable=true)
     */
    private $si;

    /**
     * @var string
     *
     * @ORM\Column(name="abd", type="string", length=4, nullable=true)
     */
    private $abd;

    /**
     * @var string
     *
     * @ORM\Column(name="m", type="string", length=4, nullable=true)
     */
    private $m;

    /**
     * @var string
     *
     * @ORM\Column(name="p", type="string", length=4, nullable=true)
     */
    private $p;

    /**
     * @var string
     *
     * @ORM\Column(name="brder", type="string", length=4, nullable=true)
     */
    private $brder;

    /**
     * @var string
     *
     * @ORM\Column(name="brizq", type="string", length=4, nullable=true)
     */
    private $brizq;

    /**
     * @var string
     *
     * @ORM\Column(name="pec", type="string", length=4, nullable=true)
     */
    private $pec;

    /**
     * @var string
     *
     * @ORM\Column(name="cint", type="string", length=4, nullable=true)
     */
    private $cint;

    /**
     * @var string
     *
     * @ORM\Column(name="cad", type="string", length=4, nullable=true)
     */
    private $cad;

    /**
     * @var string
     *
     * @ORM\Column(name="musder", type="string", length=4, nullable=true)
     */
    private $musder;

    /**
     * @var string
     *
     * @ORM\Column(name="musizq", type="string", length=4, nullable=true)
     */
    private $musizq;

    /**
     * @var string
     *
     * @ORM\Column(name="piernader", type="string", length=4, nullable=true)
     */
    private $piernader;

    /**
     * @var string
     *
     * @ORM\Column(name="piernaizq", type="string", length=4, nullable=true)
     */
    private $piernaizq;

    /**
     * @var bool
     *
     * @ORM\Column(name="obesidad", type="boolean", nullable=true)
     */
    private $obesidad;

    /**
     * @var bool
     *
     * @ORM\Column(name="tabaquismo", type="boolean", nullable=true)
     */
    private $tabaquismo;

    /**
     * @var bool
     *
     * @ORM\Column(name="hipertension", type="boolean", nullable=true)
     */
    private $hipertension;

    /**
     * @var bool
     *
     * @ORM\Column(name="prediabetes", type="boolean", nullable=true)
     */
    private $prediabetes;

    /**
     * @var bool
     *
     * @ORM\Column(name="dislipidemia", type="boolean", nullable=true)
     */
    private $dislipidemia;

    /**
     * @var bool
     *
     * @ORM\Column(name="antecedentesFamiliares", type="boolean", nullable=true)
     */
    private $antecedentesFamiliares;

    /**
     * @var bool
     *
     * @ORM\Column(name="sedentarismo", type="boolean", nullable=true)
     */
    private $sedentarismo;

    /**
     * @var bool
     *
     * @ORM\Column(name="a1", type="boolean", nullable=true)
     */
    private $a1;

    /**
     * @var bool
     *
     * @ORM\Column(name="a2", type="boolean", nullable=true)
     */
    private $a2;

    /**
     * @var bool
     *
     * @ORM\Column(name="a3", type="boolean", nullable=true)
     */
    private $a3;

    /**
     * @var bool
     *
     * @ORM\Column(name="b", type="boolean", nullable=true)
     */
    private $b;

    /**
     * @var bool
     *
     * @ORM\Column(name="c", type="boolean", nullable=true)
     */
    private $c;

    /**
     * @var bool
     *
     * @ORM\Column(name="d", type="boolean", nullable=true)
     */
    private $d;

    /**
     * @var bool
     *
     * @ORM\Column(name="antepulsionCabeza", type="boolean", nullable=true)
     */
    private $antepulsionCabeza;

    /**
     * @var bool
     *
     * @ORM\Column(name="rectificacionCervical", type="boolean", nullable=true)
     */
    private $rectificacionCervical;

    /**
     * @var bool
     *
     * @ORM\Column(name="hiperlordosisCervical", type="boolean", nullable=true)
     */
    private $hiperlordosisCervical;

    /**
     * @var bool
     *
     * @ORM\Column(name="hipercifosis", type="boolean", nullable=true)
     */
    private $hipercifosis;

    /**
     * @var bool
     *
     * @ORM\Column(name="rectificacionDorsal", type="boolean", nullable=true)
     */
    private $rectificacionDorsal;

    /**
     * @var bool
     *
     * @ORM\Column(name="hiperlordosisLumbar", type="boolean", nullable=true)
     */
    private $hiperlordosisLumbar;

    /**
     * @var bool
     *
     * @ORM\Column(name="rectificacionLumbar", type="boolean", nullable=true)
     */
    private $rectificacionLumbar;

    /**
     * @var bool
     *
     * @ORM\Column(name="escoliosis", type="boolean", nullable=true)
     */
    private $escoliosis;

    /**
     * @var bool
     *
     * @ORM\Column(name="anteropulsionHombros", type="boolean", nullable=true)
     */
    private $anteropulsionHombros;

    /**
     * @var bool
     *
     * @ORM\Column(name="hombroAscendido", type="boolean", nullable=true)
     */
    private $hombroAscendido;

    /**
     * @var bool
     *
     * @ORM\Column(name="hiperextensionCodo", type="boolean", nullable=true)
     */
    private $hiperextensionCodo;

    /**
     * @var bool
     *
     * @ORM\Column(name="aumentoValgoCodo", type="boolean", nullable=true)
     */
    private $aumentoValgoCodo;

    /**
     * @var bool
     *
     * @ORM\Column(name="anteversionFemoral", type="boolean", nullable=true)
     */
    private $anteversionFemoral;

    /**
     * @var bool
     *
     * @ORM\Column(name="genuValgoNoFisiologico", type="boolean", nullable=true)
     */
    private $genuValgoNoFisiologico;

    /**
     * @var bool
     *
     * @ORM\Column(name="genuVaroNoFisiologico", type="boolean", nullable=true)
     */
    private $genuVaroNoFisiologico;

    /**
     * @var bool
     *
     * @ORM\Column(name="genuRecurvatum", type="boolean", nullable=true)
     */
    private $genuRecurvatum;

    /**
     * @var bool
     *
     * @ORM\Column(name="rotacionTibial", type="boolean", nullable=true)
     */
    private $rotacionTibial;

    /**
     * @var bool
     *
     * @ORM\Column(name="piePlano", type="boolean", nullable=true)
     */
    private $piePlano;

    /**
     * @var bool
     *
     * @ORM\Column(name="pieCavo", type="boolean", nullable=true)
     */
    private $pieCavo;

    /**
     * @var string
     *
     * @ORM\Column(name="otro", type="string", length=150, nullable=true)
     */
    private $otro;

    /**
     * @var string
     *
     * @ORM\Column(name="analisisPostura", type="text", nullable=true)
     */
    private $analisisPostura;

    /**
     * @var bool
     *
     * @ORM\Column(name="sentadillaProfunda", type="boolean", nullable=true)
     */
    private $sentadillaProfunda;

    /**
     * @var bool
     *
     * @ORM\Column(name="bisagra", type="boolean", nullable=true)
     */
    private $bisagra;

    /**
     * @var bool
     *
     * @ORM\Column(name="cadenaCineticaSuperior", type="boolean", nullable=true)
     */
    private $cadenaCineticaSuperior;

    /**
     * @var string
     *
     * @ORM\Column(name="abdomen", type="string", length=1, nullable=true)
     */
    private $abdomen;

    /**
     * @var string
     *
     * @ORM\Column(name="sentadilla", type="string", length=1, nullable=true)
     */
    private $sentadilla;

    /**
     * @var string
     *
     * @ORM\Column(name="cuadrupizq", type="string", length=1, nullable=true)
     */
    private $cuadrupizq;

    /**
     * @var string
     *
     * @ORM\Column(name="cuadrupder", type="string", length=1, nullable=true)
     */
    private $cuadrupder;

    /**
     * @var string
     *
     * @ORM\Column(name="apoyoUnipodalIzq", type="string", length=1, nullable=true)
     */
    private $apoyoUnipodalIzq;

    /**
     * @var string
     *
     * @ORM\Column(name="apoyoUnipodalDer", type="string", length=1, nullable=true)
     */
    private $apoyoUnipodalDer;

    /**
     * @var bool
     *
     * @ORM\Column(name="bajo", type="boolean", nullable=true)
     */
    private $bajo;

    /**
     * @var bool
     *
     * @ORM\Column(name="medio", type="boolean", nullable=true)
     */
    private $medio;

    /**
     * @var bool
     *
     * @ORM\Column(name="alto", type="boolean", nullable=true)
     */
    private $alto;

    /**
     * @var bool
     *
     * @ORM\Column(name="muyAlto", type="boolean", nullable=true)
     */
    private $muyAlto;

    /**
     * @var string
     *
     * @ORM\Column(name="prescFrecuencia", type="string", length=5, nullable=true)
     */
    private $prescFrecuencia;

    /**
     * @var string
     *
     * @ORM\Column(name="prescTiempo", type="string", length=5, nullable=true)
     */
    private $prescTiempo;

    /**
     * @var string
     *
     * @ORM\Column(name="entreeCardiovascular", type="text", nullable=true)
     */
    private $entreeCardiovascular;

    /**
     * @var string
     *
     * @ORM\Column(name="movilidadFlexibilidad", type="text", nullable=true)
     */
    private $movilidadFlexibilidad;

    /**
     * @var string
     *
     * @ORM\Column(name="calsesGrupales", type="text", nullable=true)
     */
    private $calsesGrupales;

    /**
     * @var string
     *
     * @ORM\Column(name="entreMuscular", type="text", nullable=true)
     */
    private $entreMuscular;

    /**
     * @var string
     *
     * @ORM\Column(name="conclusiones", type="text", nullable=true)
     */
    private $conclusiones;

    /**
     * @var bool
     *
     * @ORM\Column(name="medico", type="boolean", nullable=true)
     */
    private $medico;

    /**
     * @var bool
     *
     * @ORM\Column(name="fisioterapeuta", type="boolean", nullable=true)
     */
    private $fisioterapeuta;

    /**
     * @var bool
     *
     * @ORM\Column(name="nutricion", type="boolean", nullable=true)
     */
    private $nutricion;

    /**
     * @var bool
     *
     * @ORM\Column(name="entrePersonalizado", type="boolean", nullable=true)
     */
    private $entrePersonalizado;

    /**
     * @var bool
     *
     * @ORM\Column(name="esteticista", type="boolean", nullable=true)
     */
    private $esteticista;

    /**
     * @var bool
     *
     * @ORM\Column(name="masajeDeportivo", type="boolean", nullable=true)
     */
    private $masajeDeportivo;

    /**
     * @var bool
     *
     * @ORM\Column(name="acondiFuncional", type="boolean", nullable=true)
     */
    private $acondiFuncional;

    /**
     * @var bool
     *
     * @ORM\Column(name="rehabFuncional", type="boolean", nullable=true)
     */
    private $rehabFuncional;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="Notadevolucionfisio", mappedBy="historia")
     */
    private $notasdevolucion;

    public function __construct()
    {
        $this->fecha = new \DateTime();

    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set consecutivo
     *
     * @param string $consecutivo
     *
     * @return historiaFisioterapia
     */
    public function setConsecutivo($consecutivo)
    {
        $this->consecutivo = $consecutivo;

        return $this;
    }

    /**
     * Get consecutivo
     *
     * @return string
     */
    public function getConsecutivo()
    {
        return $this->consecutivo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return historiaFisioterapia
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set hora
     *
     * @param \DateTime $hora
     *
     * @return historiaFisioterapia
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \DateTime
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set nombreResponsable
     *
     * @param string $nombreResponsable
     *
     * @return historiaFisioterapia
     */
    public function setNombreResponsable($nombreResponsable)
    {
        $this->nombreResponsable = $nombreResponsable;

        return $this;
    }

    /**
     * Get nombreResponsable
     *
     * @return string
     */
    public function getNombreResponsable()
    {
        return $this->nombreResponsable;
    }

    /**
     * Set telefonoResponsable
     *
     * @param string $telefonoResponsable
     *
     * @return historiaFisioterapia
     */
    public function setTelefonoResponsable($telefonoResponsable)
    {
        $this->telefonoResponsable = $telefonoResponsable;

        return $this;
    }

    /**
     * Get telefonoResponsable
     *
     * @return string
     */
    public function getTelefonoResponsable()
    {
        return $this->telefonoResponsable;
    }

    /**
     * Set eapb
     *
     * @param string $eapb
     *
     * @return historiaFisioterapia
     */
    public function setEapb($eapb)
    {
        $this->eapb = $eapb;

        return $this;
    }

    /**
     * Get eapb
     *
     * @return string
     */
    public function getEapb()
    {
        return $this->eapb;
    }

    /**
     * Set prepagada
     *
     * @param string $prepagada
     *
     * @return historiaFisioterapia
     */
    public function setPrepagada($prepagada)
    {
        $this->prepagada = $prepagada;

        return $this;
    }

    /**
     * Get prepagada
     *
     * @return string
     */
    public function getPrepagada()
    {
        return $this->prepagada;
    }

    /**
     * Set planComplementarios
     *
     * @param string $planComplementarios
     *
     * @return historiaFisioterapia
     */
    public function setPlanComplementarios($planComplementarios)
    {
        $this->planComplementarios = $planComplementarios;

        return $this;
    }

    /**
     * Get planComplementarios
     *
     * @return string
     */
    public function getPlanComplementarios()
    {
        return $this->planComplementarios;
    }

    /**
     * Set aseguradoras
     *
     * @param string $aseguradoras
     *
     * @return historiaFisioterapia
     */
    public function setAseguradoras($aseguradoras)
    {
        $this->aseguradoras = $aseguradoras;

        return $this;
    }

    /**
     * Get aseguradoras
     *
     * @return string
     */
    public function getAseguradoras()
    {
        return $this->aseguradoras;
    }

    /**
     * Set postquirurgico
     *
     * @param boolean $postquirurgico
     *
     * @return historiaFisioterapia
     */
    public function setPostquirurgico($postquirurgico)
    {
        $this->postquirurgico = $postquirurgico;

        return $this;
    }

    /**
     * Get postquirurgico
     *
     * @return bool
     */
    public function getPostquirurgico()
    {
        return $this->postquirurgico;
    }

    /**
     * Set estetico
     *
     * @param boolean $estetico
     *
     * @return historiaFisioterapia
     */
    public function setEstetico($estetico)
    {
        $this->estetico = $estetico;

        return $this;
    }

    /**
     * Get estetico
     *
     * @return bool
     */
    public function getEstetico()
    {
        return $this->estetico;
    }

    /**
     * Set tonificar
     *
     * @param boolean $tonificar
     *
     * @return historiaFisioterapia
     */
    public function setTonificar($tonificar)
    {
        $this->tonificar = $tonificar;

        return $this;
    }

    /**
     * Get tonificar
     *
     * @return bool
     */
    public function getTonificar()
    {
        return $this->tonificar;
    }

    /**
     * Set salud
     *
     * @param boolean $salud
     *
     * @return historiaFisioterapia
     */
    public function setSalud($salud)
    {
        $this->salud = $salud;

        return $this;
    }

    /**
     * Get salud
     *
     * @return bool
     */
    public function getSalud()
    {
        return $this->salud;
    }

    /**
     * Set deportivo
     *
     * @param boolean $deportivo
     *
     * @return historiaFisioterapia
     */
    public function setDeportivo($deportivo)
    {
        $this->deportivo = $deportivo;

        return $this;
    }

    /**
     * Get deportivo
     *
     * @return bool
     */
    public function getDeportivo()
    {
        return $this->deportivo;
    }

    /**
     * Set rehabilitacion
     *
     * @param boolean $rehabilitacion
     *
     * @return historiaFisioterapia
     */
    public function setRehabilitacion($rehabilitacion)
    {
        $this->rehabilitacion = $rehabilitacion;

        return $this;
    }

    /**
     * Get rehabilitacion
     *
     * @return bool
     */
    public function getRehabilitacion()
    {
        return $this->rehabilitacion;
    }

    /**
     * Set patologicos
     *
     * @param string $patologicos
     *
     * @return historiaFisioterapia
     */
    public function setPatologicos($patologicos)
    {
        $this->patologicos = $patologicos;

        return $this;
    }

    /**
     * Get patologicos
     *
     * @return string
     */
    public function getPatologicos()
    {
        return $this->patologicos;
    }

    /**
     * Set quirurgicos
     *
     * @param string $quirurgicos
     *
     * @return historiaFisioterapia
     */
    public function setQuirurgicos($quirurgicos)
    {
        $this->quirurgicos = $quirurgicos;

        return $this;
    }

    /**
     * Get quirurgicos
     *
     * @return string
     */
    public function getQuirurgicos()
    {
        return $this->quirurgicos;
    }

    /**
     * Set gineFum
     *
     * @param string $gineFum
     *
     * @return historiaFisioterapia
     */
    public function setGineFum($gineFum)
    {
        $this->gineFum = $gineFum;

        return $this;
    }

    /**
     * Get gineFum
     *
     * @return string
     */
    public function getGineFum()
    {
        return $this->gineFum;
    }

    /**
     * Set gineG
     *
     * @param boolean $gineG
     *
     * @return historiaFisioterapia
     */
    public function setGineG($gineG)
    {
        $this->gineG = $gineG;

        return $this;
    }

    /**
     * Get gineG
     *
     * @return bool
     */
    public function getGineG()
    {
        return $this->gineG;
    }

    /**
     * Set gineP
     *
     * @param boolean $gineP
     *
     * @return historiaFisioterapia
     */
    public function setGineP($gineP)
    {
        $this->gineP = $gineP;

        return $this;
    }

    /**
     * Get gineP
     *
     * @return bool
     */
    public function getGineP()
    {
        return $this->gineP;
    }

    /**
     * Set gineC
     *
     * @param boolean $gineC
     *
     * @return historiaFisioterapia
     */
    public function setGineC($gineC)
    {
        $this->gineC = $gineC;

        return $this;
    }

    /**
     * Get gineC
     *
     * @return bool
     */
    public function getGineC()
    {
        return $this->gineC;
    }

    /**
     * Set gineV
     *
     * @param boolean $gineV
     *
     * @return historiaFisioterapia
     */
    public function setGineV($gineV)
    {
        $this->gineV = $gineV;

        return $this;
    }

    /**
     * Get gineV
     *
     * @return bool
     */
    public function getGineV()
    {
        return $this->gineV;
    }

    /**
     * Set gineA
     *
     * @param boolean $gineA
     *
     * @return historiaFisioterapia
     */
    public function setGineA($gineA)
    {
        $this->gineA = $gineA;

        return $this;
    }

    /**
     * Get gineA
     *
     * @return bool
     */
    public function getGineA()
    {
        return $this->gineA;
    }

    /**
     * Set farmacologicos
     *
     * @param string $farmacologicos
     *
     * @return historiaFisioterapia
     */
    public function setFarmacologicos($farmacologicos)
    {
        $this->farmacologicos = $farmacologicos;

        return $this;
    }

    /**
     * Get farmacologicos
     *
     * @return string
     */
    public function getFarmacologicos()
    {
        return $this->farmacologicos;
    }

    /**
     * Set osteomusculares
     *
     * @param string $osteomusculares
     *
     * @return historiaFisioterapia
     */
    public function setOsteomusculares($osteomusculares)
    {
        $this->osteomusculares = $osteomusculares;

        return $this;
    }

    /**
     * Get osteomusculares
     *
     * @return string
     */
    public function getOsteomusculares()
    {
        return $this->osteomusculares;
    }

    /**
     * Set antecedentesfamiliates
     *
     * @param string $antecedentesfamiliates
     *
     * @return historiaFisioterapia
     */
    public function setAntecedentesfamiliates($antecedentesfamiliates)
    {
        $this->antecedentesfamiliates = $antecedentesfamiliates;

        return $this;
    }

    /**
     * Get antecedentesfamiliates
     *
     * @return string
     */
    public function getAntecedentesfamiliates()
    {
        return $this->antecedentesfamiliates;
    }

    /**
     * Set sedentario
     *
     * @param boolean $sedentario
     *
     * @return historiaFisioterapia
     */
    public function setSedentario($sedentario)
    {
        $this->sedentario = $sedentario;

        return $this;
    }

    /**
     * Get sedentario
     *
     * @return bool
     */
    public function getSedentario()
    {
        return $this->sedentario;
    }

    /**
     * Set tipoEjercicio
     *
     * @param string $tipoEjercicio
     *
     * @return historiaFisioterapia
     */
    public function setTipoEjercicio($tipoEjercicio)
    {
        $this->tipoEjercicio = $tipoEjercicio;

        return $this;
    }

    /**
     * Get tipoEjercicio
     *
     * @return string
     */
    public function getTipoEjercicio()
    {
        return $this->tipoEjercicio;
    }

    /**
     * Set frEjercicio
     *
     * @param string $frEjercicio
     *
     * @return historiaFisioterapia
     */
    public function setFrEjercicio($frEjercicio)
    {
        $this->frEjercicio = $frEjercicio;

        return $this;
    }

    /**
     * Get frEjercicio
     *
     * @return string
     */
    public function getFrEjercicio()
    {
        return $this->frEjercicio;
    }

    /**
     * Set duracionEjercicio
     *
     * @param string $duracionEjercicio
     *
     * @return historiaFisioterapia
     */
    public function setDuracionEjercicio($duracionEjercicio)
    {
        $this->duracionEjercicio = $duracionEjercicio;

        return $this;
    }

    /**
     * Get duracionEjercicio
     *
     * @return string
     */
    public function getDuracionEjercicio()
    {
        return $this->duracionEjercicio;
    }

    /**
     * Set intensidadEjercicio
     *
     * @param string $intensidadEjercicio
     *
     * @return historiaFisioterapia
     */
    public function setIntensidadEjercicio($intensidadEjercicio)
    {
        $this->intensidadEjercicio = $intensidadEjercicio;

        return $this;
    }

    /**
     * Get intensidadEjercicio
     *
     * @return string
     */
    public function getIntensidadEjercicio()
    {
        return $this->intensidadEjercicio;
    }

    /**
     * Set tipoDeporte
     *
     * @param string $tipoDeporte
     *
     * @return historiaFisioterapia
     */
    public function setTipoDeporte($tipoDeporte)
    {
        $this->tipoDeporte = $tipoDeporte;

        return $this;
    }

    /**
     * Get tipoDeporte
     *
     * @return string
     */
    public function getTipoDeporte()
    {
        return $this->tipoDeporte;
    }

    /**
     * Set frDeporte
     *
     * @param string $frDeporte
     *
     * @return historiaFisioterapia
     */
    public function setFrDeporte($frDeporte)
    {
        $this->frDeporte = $frDeporte;

        return $this;
    }

    /**
     * Get frDeporte
     *
     * @return string
     */
    public function getFrDeporte()
    {
        return $this->frDeporte;
    }

    /**
     * Set duracionDeporte
     *
     * @param string $duracionDeporte
     *
     * @return historiaFisioterapia
     */
    public function setDuracionDeporte($duracionDeporte)
    {
        $this->duracionDeporte = $duracionDeporte;

        return $this;
    }

    /**
     * Get duracionDeporte
     *
     * @return string
     */
    public function getDuracionDeporte()
    {
        return $this->duracionDeporte;
    }

    /**
     * Set intensidadDeporte
     *
     * @param string $intensidadDeporte
     *
     * @return historiaFisioterapia
     */
    public function setIntensidadDeporte($intensidadDeporte)
    {
        $this->intensidadDeporte = $intensidadDeporte;

        return $this;
    }

    /**
     * Get intensidadDeporte
     *
     * @return string
     */
    public function getIntensidadDeporte()
    {
        return $this->intensidadDeporte;
    }

    /**
     * Set pregunta1chk
     *
     * @param boolean $pregunta1chk
     *
     * @return historiaFisioterapia
     */
    public function setPregunta1chk($pregunta1chk)
    {
        $this->pregunta1chk = $pregunta1chk;

        return $this;
    }

    /**
     * Get pregunta1chk
     *
     * @return bool
     */
    public function getPregunta1chk()
    {
        return $this->pregunta1chk;
    }

    /**
     * Set pregunta1rp
     *
     * @param string $pregunta1rp
     *
     * @return historiaFisioterapia
     */
    public function setPregunta1rp($pregunta1rp)
    {
        $this->pregunta1rp = $pregunta1rp;

        return $this;
    }

    /**
     * Get pregunta1rp
     *
     * @return string
     */
    public function getPregunta1rp()
    {
        return $this->pregunta1rp;
    }

    /**
     * Set pregunta2chk
     *
     * @param boolean $pregunta2chk
     *
     * @return historiaFisioterapia
     */
    public function setPregunta2chk($pregunta2chk)
    {
        $this->pregunta2chk = $pregunta2chk;

        return $this;
    }

    /**
     * Get pregunta2chk
     *
     * @return bool
     */
    public function getPregunta2chk()
    {
        return $this->pregunta2chk;
    }

    /**
     * Set pregunta2rp
     *
     * @param string $pregunta2rp
     *
     * @return historiaFisioterapia
     */
    public function setPregunta2rp($pregunta2rp)
    {
        $this->pregunta2rp = $pregunta2rp;

        return $this;
    }

    /**
     * Get pregunta2rp
     *
     * @return string
     */
    public function getPregunta2rp()
    {
        return $this->pregunta2rp;
    }

    /**
     * Set pregunta3chk
     *
     * @param boolean $pregunta3chk
     *
     * @return historiaFisioterapia
     */
    public function setPregunta3chk($pregunta3chk)
    {
        $this->pregunta3chk = $pregunta3chk;

        return $this;
    }

    /**
     * Get pregunta3chk
     *
     * @return bool
     */
    public function getPregunta3chk()
    {
        return $this->pregunta3chk;
    }

    /**
     * Set pregunta3rp
     *
     * @param string $pregunta3rp
     *
     * @return historiaFisioterapia
     */
    public function setPregunta3rp($pregunta3rp)
    {
        $this->pregunta3rp = $pregunta3rp;

        return $this;
    }

    /**
     * Get pregunta3rp
     *
     * @return string
     */
    public function getPregunta3rp()
    {
        return $this->pregunta3rp;
    }

    /**
     * Set pregunta4chk
     *
     * @param boolean $pregunta4chk
     *
     * @return historiaFisioterapia
     */
    public function setPregunta4chk($pregunta4chk)
    {
        $this->pregunta4chk = $pregunta4chk;

        return $this;
    }

    /**
     * Get pregunta4chk
     *
     * @return bool
     */
    public function getPregunta4chk()
    {
        return $this->pregunta4chk;
    }

    /**
     * Set pregunta4rp
     *
     * @param string $pregunta4rp
     *
     * @return historiaFisioterapia
     */
    public function setPregunta4rp($pregunta4rp)
    {
        $this->pregunta4rp = $pregunta4rp;

        return $this;
    }

    /**
     * Get pregunta4rp
     *
     * @return string
     */
    public function getPregunta4rp()
    {
        return $this->pregunta4rp;
    }

    /**
     * Set pregunta5chk
     *
     * @param boolean $pregunta5chk
     *
     * @return historiaFisioterapia
     */
    public function setPregunta5chk($pregunta5chk)
    {
        $this->pregunta5chk = $pregunta5chk;

        return $this;
    }

    /**
     * Get pregunta5chk
     *
     * @return bool
     */
    public function getPregunta5chk()
    {
        return $this->pregunta5chk;
    }

    /**
     * Set pregunta5rp
     *
     * @param string $pregunta5rp
     *
     * @return historiaFisioterapia
     */
    public function setPregunta5rp($pregunta5rp)
    {
        $this->pregunta5rp = $pregunta5rp;

        return $this;
    }

    /**
     * Get pregunta5rp
     *
     * @return string
     */
    public function getPregunta5rp()
    {
        return $this->pregunta5rp;
    }

    /**
     * Set pregunta6chk
     *
     * @param boolean $pregunta6chk
     *
     * @return historiaFisioterapia
     */
    public function setPregunta6chk($pregunta6chk)
    {
        $this->pregunta6chk = $pregunta6chk;

        return $this;
    }

    /**
     * Get pregunta6chk
     *
     * @return bool
     */
    public function getPregunta6chk()
    {
        return $this->pregunta6chk;
    }

    /**
     * Set pregunta6rp
     *
     * @param string $pregunta6rp
     *
     * @return historiaFisioterapia
     */
    public function setPregunta6rp($pregunta6rp)
    {
        $this->pregunta6rp = $pregunta6rp;

        return $this;
    }

    /**
     * Get pregunta6rp
     *
     * @return string
     */
    public function getPregunta6rp()
    {
        return $this->pregunta6rp;
    }

    /**
     * Set tensionArterial
     *
     * @param string $tensionArterial
     *
     * @return historiaFisioterapia
     */
    public function setTensionArterial($tensionArterial)
    {
        $this->tensionArterial = $tensionArterial;

        return $this;
    }

    /**
     * Get tensionArterial
     *
     * @return string
     */
    public function getTensionArterial()
    {
        return $this->tensionArterial;
    }

    /**
     * Set frecuenciaCardiaca
     *
     * @param string $frecuenciaCardiaca
     *
     * @return historiaFisioterapia
     */
    public function setFrecuenciaCardiaca($frecuenciaCardiaca)
    {
        $this->frecuenciaCardiaca = $frecuenciaCardiaca;

        return $this;
    }

    /**
     * Get frecuenciaCardiaca
     *
     * @return string
     */
    public function getFrecuenciaCardiaca()
    {
        return $this->frecuenciaCardiaca;
    }

    /**
     * Set talla
     *
     * @param string $talla
     *
     * @return historiaFisioterapia
     */
    public function setTalla($talla)
    {
        $this->talla = $talla;

        return $this;
    }

    /**
     * Get talla
     *
     * @return string
     */
    public function getTalla()
    {
        return $this->talla;
    }

    /**
     * Set pesoKg
     *
     * @param string $pesoKg
     *
     * @return historiaFisioterapia
     */
    public function setPesoKg($pesoKg)
    {
        $this->pesoKg = $pesoKg;

        return $this;
    }

    /**
     * Get pesoKg
     *
     * @return string
     */
    public function getPesoKg()
    {
        return $this->pesoKg;
    }

    /**
     * Set indiceMasaCorporal
     *
     * @param string $indiceMasaCorporal
     *
     * @return historiaFisioterapia
     */
    public function setIndiceMasaCorporal($indiceMasaCorporal)
    {
        $this->indiceMasaCorporal = $indiceMasaCorporal;

        return $this;
    }

    /**
     * Get indiceMasaCorporal
     *
     * @return string
     */
    public function getIndiceMasaCorporal()
    {
        return $this->indiceMasaCorporal;
    }

    /**
     * Set perimetroAbdominal
     *
     * @param string $perimetroAbdominal
     *
     * @return historiaFisioterapia
     */
    public function setPerimetroAbdominal($perimetroAbdominal)
    {
        $this->perimetroAbdominal = $perimetroAbdominal;

        return $this;
    }

    /**
     * Get perimetroAbdominal
     *
     * @return string
     */
    public function getPerimetroAbdominal()
    {
        return $this->perimetroAbdominal;
    }

    /**
     * Set grasa
     *
     * @param string $grasa
     *
     * @return historiaFisioterapia
     */
    public function setGrasa($grasa)
    {
        $this->grasa = $grasa;

        return $this;
    }

    /**
     * Get grasa
     *
     * @return string
     */
    public function getGrasa()
    {
        return $this->grasa;
    }

    /**
     * Set muscular
     *
     * @param string $muscular
     *
     * @return historiaFisioterapia
     */
    public function setMuscular($muscular)
    {
        $this->muscular = $muscular;

        return $this;
    }

    /**
     * Get muscular
     *
     * @return string
     */
    public function getMuscular()
    {
        return $this->muscular;
    }

    /**
     * Set se
     *
     * @param string $se
     *
     * @return historiaFisioterapia
     */
    public function setSe($se)
    {
        $this->se = $se;

        return $this;
    }

    /**
     * Get se
     *
     * @return string
     */
    public function getSe()
    {
        return $this->se;
    }

    /**
     * Set t
     *
     * @param string $t
     *
     * @return historiaFisioterapia
     */
    public function setT($t)
    {
        $this->t = $t;

        return $this;
    }

    /**
     * Get t
     *
     * @return string
     */
    public function getT()
    {
        return $this->t;
    }

    /**
     * Set si
     *
     * @param string $si
     *
     * @return historiaFisioterapia
     */
    public function setSi($si)
    {
        $this->si = $si;

        return $this;
    }

    /**
     * Get si
     *
     * @return string
     */
    public function getSi()
    {
        return $this->si;
    }

    /**
     * Set abd
     *
     * @param string $abd
     *
     * @return historiaFisioterapia
     */
    public function setAbd($abd)
    {
        $this->abd = $abd;

        return $this;
    }

    /**
     * Get abd
     *
     * @return string
     */
    public function getAbd()
    {
        return $this->abd;
    }

    /**
     * Set m
     *
     * @param string $m
     *
     * @return historiaFisioterapia
     */
    public function setM($m)
    {
        $this->m = $m;

        return $this;
    }

    /**
     * Get m
     *
     * @return string
     */
    public function getM()
    {
        return $this->m;
    }

    /**
     * Set p
     *
     * @param string $p
     *
     * @return historiaFisioterapia
     */
    public function setP($p)
    {
        $this->p = $p;

        return $this;
    }

    /**
     * Get p
     *
     * @return string
     */
    public function getP()
    {
        return $this->p;
    }

    /**
     * Set brder
     *
     * @param string $brder
     *
     * @return historiaFisioterapia
     */
    public function setBrder($brder)
    {
        $this->brder = $brder;

        return $this;
    }

    /**
     * Get brder
     *
     * @return string
     */
    public function getBrder()
    {
        return $this->brder;
    }

    /**
     * Set brizq
     *
     * @param string $brizq
     *
     * @return historiaFisioterapia
     */
    public function setBrizq($brizq)
    {
        $this->brizq = $brizq;

        return $this;
    }

    /**
     * Get brizq
     *
     * @return string
     */
    public function getBrizq()
    {
        return $this->brizq;
    }

    /**
     * Set pec
     *
     * @param string $pec
     *
     * @return historiaFisioterapia
     */
    public function setPec($pec)
    {
        $this->pec = $pec;

        return $this;
    }

    /**
     * Get pec
     *
     * @return string
     */
    public function getPec()
    {
        return $this->pec;
    }

    /**
     * Set cint
     *
     * @param string $cint
     *
     * @return historiaFisioterapia
     */
    public function setCint($cint)
    {
        $this->cint = $cint;

        return $this;
    }

    /**
     * Get cint
     *
     * @return string
     */
    public function getCint()
    {
        return $this->cint;
    }

    /**
     * Set cad
     *
     * @param string $cad
     *
     * @return historiaFisioterapia
     */
    public function setCad($cad)
    {
        $this->cad = $cad;

        return $this;
    }

    /**
     * Get cad
     *
     * @return string
     */
    public function getCad()
    {
        return $this->cad;
    }

    /**
     * Set musder
     *
     * @param string $musder
     *
     * @return historiaFisioterapia
     */
    public function setMusder($musder)
    {
        $this->musder = $musder;

        return $this;
    }

    /**
     * Get musder
     *
     * @return string
     */
    public function getMusder()
    {
        return $this->musder;
    }

    /**
     * Set musizq
     *
     * @param string $musizq
     *
     * @return historiaFisioterapia
     */
    public function setMusizq($musizq)
    {
        $this->musizq = $musizq;

        return $this;
    }

    /**
     * Get musizq
     *
     * @return string
     */
    public function getMusizq()
    {
        return $this->musizq;
    }

    /**
     * Set piernader
     *
     * @param string $piernader
     *
     * @return historiaFisioterapia
     */
    public function setPiernader($piernader)
    {
        $this->piernader = $piernader;

        return $this;
    }

    /**
     * Get piernader
     *
     * @return string
     */
    public function getPiernader()
    {
        return $this->piernader;
    }

    /**
     * Set piernaizq
     *
     * @param string $piernaizq
     *
     * @return historiaFisioterapia
     */
    public function setPiernaizq($piernaizq)
    {
        $this->piernaizq = $piernaizq;

        return $this;
    }

    /**
     * Get piernaizq
     *
     * @return string
     */
    public function getPiernaizq()
    {
        return $this->piernaizq;
    }

    /**
     * Set obesidad
     *
     * @param boolean $obesidad
     *
     * @return historiaFisioterapia
     */
    public function setObesidad($obesidad)
    {
        $this->obesidad = $obesidad;

        return $this;
    }

    /**
     * Get obesidad
     *
     * @return bool
     */
    public function getObesidad()
    {
        return $this->obesidad;
    }

    /**
     * Set tabaquismo
     *
     * @param boolean $tabaquismo
     *
     * @return historiaFisioterapia
     */
    public function setTabaquismo($tabaquismo)
    {
        $this->tabaquismo = $tabaquismo;

        return $this;
    }

    /**
     * Get tabaquismo
     *
     * @return bool
     */
    public function getTabaquismo()
    {
        return $this->tabaquismo;
    }

    /**
     * Set hipertension
     *
     * @param boolean $hipertension
     *
     * @return historiaFisioterapia
     */
    public function setHipertension($hipertension)
    {
        $this->hipertension = $hipertension;

        return $this;
    }

    /**
     * Get hipertension
     *
     * @return bool
     */
    public function getHipertension()
    {
        return $this->hipertension;
    }

    /**
     * Set prediabetes
     *
     * @param boolean $prediabetes
     *
     * @return historiaFisioterapia
     */
    public function setPrediabetes($prediabetes)
    {
        $this->prediabetes = $prediabetes;

        return $this;
    }

    /**
     * Get prediabetes
     *
     * @return bool
     */
    public function getPrediabetes()
    {
        return $this->prediabetes;
    }

    /**
     * Set dislipidemia
     *
     * @param boolean $dislipidemia
     *
     * @return historiaFisioterapia
     */
    public function setDislipidemia($dislipidemia)
    {
        $this->dislipidemia = $dislipidemia;

        return $this;
    }

    /**
     * Get dislipidemia
     *
     * @return bool
     */
    public function getDislipidemia()
    {
        return $this->dislipidemia;
    }

    /**
     * Set antecedentesFamiliares
     *
     * @param boolean $antecedentesFamiliares
     *
     * @return historiaFisioterapia
     */
    public function setAntecedentesFamiliares($antecedentesFamiliares)
    {
        $this->antecedentesFamiliares = $antecedentesFamiliares;

        return $this;
    }

    /**
     * Get antecedentesFamiliares
     *
     * @return bool
     */
    public function getAntecedentesFamiliares()
    {
        return $this->antecedentesFamiliares;
    }

    /**
     * Set sedentarismo
     *
     * @param boolean $sedentarismo
     *
     * @return historiaFisioterapia
     */
    public function setSedentarismo($sedentarismo)
    {
        $this->sedentarismo = $sedentarismo;

        return $this;
    }

    /**
     * Get sedentarismo
     *
     * @return bool
     */
    public function getSedentarismo()
    {
        return $this->sedentarismo;
    }

    /**
     * Set a1
     *
     * @param boolean $a1
     *
     * @return historiaFisioterapia
     */
    public function setA1($a1)
    {
        $this->a1 = $a1;

        return $this;
    }

    /**
     * Get a1
     *
     * @return bool
     */
    public function getA1()
    {
        return $this->a1;
    }

    /**
     * Set a2
     *
     * @param boolean $a2
     *
     * @return historiaFisioterapia
     */
    public function setA2($a2)
    {
        $this->a2 = $a2;

        return $this;
    }

    /**
     * Get a2
     *
     * @return bool
     */
    public function getA2()
    {
        return $this->a2;
    }

    /**
     * Set a3
     *
     * @param boolean $a3
     *
     * @return historiaFisioterapia
     */
    public function setA3($a3)
    {
        $this->a3 = $a3;

        return $this;
    }

    /**
     * Get a3
     *
     * @return bool
     */
    public function getA3()
    {
        return $this->a3;
    }

    /**
     * Set b
     *
     * @param boolean $b
     *
     * @return historiaFisioterapia
     */
    public function setB($b)
    {
        $this->b = $b;

        return $this;
    }

    /**
     * Get b
     *
     * @return bool
     */
    public function getB()
    {
        return $this->b;
    }

    /**
     * Set c
     *
     * @param boolean $c
     *
     * @return historiaFisioterapia
     */
    public function setC($c)
    {
        $this->c = $c;

        return $this;
    }

    /**
     * Get c
     *
     * @return bool
     */
    public function getC()
    {
        return $this->c;
    }

    /**
     * Set d
     *
     * @param boolean $d
     *
     * @return historiaFisioterapia
     */
    public function setD($d)
    {
        $this->d = $d;

        return $this;
    }

    /**
     * Get d
     *
     * @return bool
     */
    public function getD()
    {
        return $this->d;
    }

    /**
     * Set antepulsionCabeza
     *
     * @param boolean $antepulsionCabeza
     *
     * @return historiaFisioterapia
     */
    public function setAntepulsionCabeza($antepulsionCabeza)
    {
        $this->antepulsionCabeza = $antepulsionCabeza;

        return $this;
    }

    /**
     * Get antepulsionCabeza
     *
     * @return bool
     */
    public function getAntepulsionCabeza()
    {
        return $this->antepulsionCabeza;
    }

    /**
     * Set rectificacionCervical
     *
     * @param boolean $rectificacionCervical
     *
     * @return historiaFisioterapia
     */
    public function setRectificacionCervical($rectificacionCervical)
    {
        $this->rectificacionCervical = $rectificacionCervical;

        return $this;
    }

    /**
     * Get rectificacionCervical
     *
     * @return bool
     */
    public function getRectificacionCervical()
    {
        return $this->rectificacionCervical;
    }

    /**
     * Set hiperlordosisCervical
     *
     * @param boolean $hiperlordosisCervical
     *
     * @return historiaFisioterapia
     */
    public function setHiperlordosisCervical($hiperlordosisCervical)
    {
        $this->hiperlordosisCervical = $hiperlordosisCervical;

        return $this;
    }

    /**
     * Get hiperlordosisCervical
     *
     * @return bool
     */
    public function getHiperlordosisCervical()
    {
        return $this->hiperlordosisCervical;
    }

    /**
     * Set hipercifosis
     *
     * @param boolean $hipercifosis
     *
     * @return historiaFisioterapia
     */
    public function setHipercifosis($hipercifosis)
    {
        $this->hipercifosis = $hipercifosis;

        return $this;
    }

    /**
     * Get hipercifosis
     *
     * @return bool
     */
    public function getHipercifosis()
    {
        return $this->hipercifosis;
    }

    /**
     * Set rectificacionDorsal
     *
     * @param boolean $rectificacionDorsal
     *
     * @return historiaFisioterapia
     */
    public function setRectificacionDorsal($rectificacionDorsal)
    {
        $this->rectificacionDorsal = $rectificacionDorsal;

        return $this;
    }

    /**
     * Get rectificacionDorsal
     *
     * @return bool
     */
    public function getRectificacionDorsal()
    {
        return $this->rectificacionDorsal;
    }

    /**
     * Set hiperlordosisLumbar
     *
     * @param boolean $hiperlordosisLumbar
     *
     * @return historiaFisioterapia
     */
    public function setHiperlordosisLumbar($hiperlordosisLumbar)
    {
        $this->hiperlordosisLumbar = $hiperlordosisLumbar;

        return $this;
    }

    /**
     * Get hiperlordosisLumbar
     *
     * @return bool
     */
    public function getHiperlordosisLumbar()
    {
        return $this->hiperlordosisLumbar;
    }

    /**
     * Set rectificacionLumbar
     *
     * @param boolean $rectificacionLumbar
     *
     * @return historiaFisioterapia
     */
    public function setRectificacionLumbar($rectificacionLumbar)
    {
        $this->rectificacionLumbar = $rectificacionLumbar;

        return $this;
    }

    /**
     * Get rectificacionLumbar
     *
     * @return bool
     */
    public function getRectificacionLumbar()
    {
        return $this->rectificacionLumbar;
    }

    /**
     * Set escoliosis
     *
     * @param boolean $escoliosis
     *
     * @return historiaFisioterapia
     */
    public function setEscoliosis($escoliosis)
    {
        $this->escoliosis = $escoliosis;

        return $this;
    }

    /**
     * Get escoliosis
     *
     * @return bool
     */
    public function getEscoliosis()
    {
        return $this->escoliosis;
    }

    /**
     * Set anteropulsionHombros
     *
     * @param boolean $anteropulsionHombros
     *
     * @return historiaFisioterapia
     */
    public function setAnteropulsionHombros($anteropulsionHombros)
    {
        $this->anteropulsionHombros = $anteropulsionHombros;

        return $this;
    }

    /**
     * Get anteropulsionHombros
     *
     * @return bool
     */
    public function getAnteropulsionHombros()
    {
        return $this->anteropulsionHombros;
    }

    /**
     * Set hombroAscendido
     *
     * @param boolean $hombroAscendido
     *
     * @return historiaFisioterapia
     */
    public function setHombroAscendido($hombroAscendido)
    {
        $this->hombroAscendido = $hombroAscendido;

        return $this;
    }

    /**
     * Get hombroAscendido
     *
     * @return bool
     */
    public function getHombroAscendido()
    {
        return $this->hombroAscendido;
    }

    /**
     * Set hiperextensionCodo
     *
     * @param boolean $hiperextensionCodo
     *
     * @return historiaFisioterapia
     */
    public function setHiperextensionCodo($hiperextensionCodo)
    {
        $this->hiperextensionCodo = $hiperextensionCodo;

        return $this;
    }

    /**
     * Get hiperextensionCodo
     *
     * @return bool
     */
    public function getHiperextensionCodo()
    {
        return $this->hiperextensionCodo;
    }

    /**
     * Set aumentoValgoCodo
     *
     * @param boolean $aumentoValgoCodo
     *
     * @return historiaFisioterapia
     */
    public function setAumentoValgoCodo($aumentoValgoCodo)
    {
        $this->aumentoValgoCodo = $aumentoValgoCodo;

        return $this;
    }

    /**
     * Get aumentoValgoCodo
     *
     * @return bool
     */
    public function getAumentoValgoCodo()
    {
        return $this->aumentoValgoCodo;
    }

    /**
     * Set anteversionFemoral
     *
     * @param boolean $anteversionFemoral
     *
     * @return historiaFisioterapia
     */
    public function setAnteversionFemoral($anteversionFemoral)
    {
        $this->anteversionFemoral = $anteversionFemoral;

        return $this;
    }

    /**
     * Get anteversionFemoral
     *
     * @return bool
     */
    public function getAnteversionFemoral()
    {
        return $this->anteversionFemoral;
    }

    /**
     * Set genuValgoNoFisiologico
     *
     * @param boolean $genuValgoNoFisiologico
     *
     * @return historiaFisioterapia
     */
    public function setGenuValgoNoFisiologico($genuValgoNoFisiologico)
    {
        $this->genuValgoNoFisiologico = $genuValgoNoFisiologico;

        return $this;
    }

    /**
     * Get genuValgoNoFisiologico
     *
     * @return bool
     */
    public function getGenuValgoNoFisiologico()
    {
        return $this->genuValgoNoFisiologico;
    }

    /**
     * Set genuVaroNoFisiologico
     *
     * @param boolean $genuVaroNoFisiologico
     *
     * @return historiaFisioterapia
     */
    public function setGenuVaroNoFisiologico($genuVaroNoFisiologico)
    {
        $this->genuVaroNoFisiologico = $genuVaroNoFisiologico;

        return $this;
    }

    /**
     * Get genuVaroNoFisiologico
     *
     * @return bool
     */
    public function getGenuVaroNoFisiologico()
    {
        return $this->genuVaroNoFisiologico;
    }

    /**
     * Set genuRecurvatum
     *
     * @param boolean $genuRecurvatum
     *
     * @return historiaFisioterapia
     */
    public function setGenuRecurvatum($genuRecurvatum)
    {
        $this->genuRecurvatum = $genuRecurvatum;

        return $this;
    }

    /**
     * Get genuRecurvatum
     *
     * @return bool
     */
    public function getGenuRecurvatum()
    {
        return $this->genuRecurvatum;
    }

    /**
     * Set rotacionTibial
     *
     * @param boolean $rotacionTibial
     *
     * @return historiaFisioterapia
     */
    public function setRotacionTibial($rotacionTibial)
    {
        $this->rotacionTibial = $rotacionTibial;

        return $this;
    }

    /**
     * Get rotacionTibial
     *
     * @return bool
     */
    public function getRotacionTibial()
    {
        return $this->rotacionTibial;
    }

    /**
     * Set piePlano
     *
     * @param boolean $piePlano
     *
     * @return historiaFisioterapia
     */
    public function setPiePlano($piePlano)
    {
        $this->piePlano = $piePlano;

        return $this;
    }

    /**
     * Get piePlano
     *
     * @return bool
     */
    public function getPiePlano()
    {
        return $this->piePlano;
    }

    /**
     * Set pieCavo
     *
     * @param boolean $pieCavo
     *
     * @return historiaFisioterapia
     */
    public function setPieCavo($pieCavo)
    {
        $this->pieCavo = $pieCavo;

        return $this;
    }

    /**
     * Get pieCavo
     *
     * @return bool
     */
    public function getPieCavo()
    {
        return $this->pieCavo;
    }

    /**
     * Set analisisPostura
     *
     * @param string $analisisPostura
     *
     * @return historiaFisioterapia
     */
    public function setAnalisisPostura($analisisPostura)
    {
        $this->analisisPostura = $analisisPostura;

        return $this;
    }

    /**
     * Get analisisPostura
     *
     * @return string
     */
    public function getAnalisisPostura()
    {
        return $this->analisisPostura;
    }

    /**
     * Set sentadillaProfunda
     *
     * @param boolean $sentadillaProfunda
     *
     * @return historiaFisioterapia
     */
    public function setSentadillaProfunda($sentadillaProfunda)
    {
        $this->sentadillaProfunda = $sentadillaProfunda;

        return $this;
    }

    /**
     * Get sentadillaProfunda
     *
     * @return bool
     */
    public function getSentadillaProfunda()
    {
        return $this->sentadillaProfunda;
    }

    /**
     * Set bisagra
     *
     * @param boolean $bisagra
     *
     * @return historiaFisioterapia
     */
    public function setBisagra($bisagra)
    {
        $this->bisagra = $bisagra;

        return $this;
    }

    /**
     * Get bisagra
     *
     * @return bool
     */
    public function getBisagra()
    {
        return $this->bisagra;
    }

    /**
     * Set cadenaCineticaSuperior
     *
     * @param boolean $cadenaCineticaSuperior
     *
     * @return historiaFisioterapia
     */
    public function setCadenaCineticaSuperior($cadenaCineticaSuperior)
    {
        $this->cadenaCineticaSuperior = $cadenaCineticaSuperior;

        return $this;
    }

    /**
     * Get cadenaCineticaSuperior
     *
     * @return bool
     */
    public function getCadenaCineticaSuperior()
    {
        return $this->cadenaCineticaSuperior;
    }

    /**
     * Set abdomen
     *
     * @param string $abdomen
     *
     * @return historiaFisioterapia
     */
    public function setAbdomen($abdomen)
    {
        $this->abdomen = $abdomen;

        return $this;
    }

    /**
     * Get abdomen
     *
     * @return string
     */
    public function getAbdomen()
    {
        return $this->abdomen;
    }

    /**
     * Set sentadilla
     *
     * @param string $sentadilla
     *
     * @return historiaFisioterapia
     */
    public function setSentadilla($sentadilla)
    {
        $this->sentadilla = $sentadilla;

        return $this;
    }

    /**
     * Get sentadilla
     *
     * @return string
     */
    public function getSentadilla()
    {
        return $this->sentadilla;
    }

    /**
     * Set cuadrupizq
     *
     * @param string $cuadrupizq
     *
     * @return historiaFisioterapia
     */
    public function setCuadrupizq($cuadrupizq)
    {
        $this->cuadrupizq = $cuadrupizq;

        return $this;
    }

    /**
     * Get cuadrupizq
     *
     * @return string
     */
    public function getCuadrupizq()
    {
        return $this->cuadrupizq;
    }

    /**
     * Set cuadrupder
     *
     * @param string $cuadrupder
     *
     * @return historiaFisioterapia
     */
    public function setCuadrupder($cuadrupder)
    {
        $this->cuadrupder = $cuadrupder;

        return $this;
    }

    /**
     * Get cuadrupder
     *
     * @return string
     */
    public function getCuadrupder()
    {
        return $this->cuadrupder;
    }

    /**
     * Set apoyoUnipodalIzq
     *
     * @param string $apoyoUnipodalIzq
     *
     * @return historiaFisioterapia
     */
    public function setApoyoUnipodalIzq($apoyoUnipodalIzq)
    {
        $this->apoyoUnipodalIzq = $apoyoUnipodalIzq;

        return $this;
    }

    /**
     * Get apoyoUnipodalIzq
     *
     * @return string
     */
    public function getApoyoUnipodalIzq()
    {
        return $this->apoyoUnipodalIzq;
    }

    /**
     * Set apoyoUnipodalDer
     *
     * @param string $apoyoUnipodalDer
     *
     * @return historiaFisioterapia
     */
    public function setApoyoUnipodalDer($apoyoUnipodalDer)
    {
        $this->apoyoUnipodalDer = $apoyoUnipodalDer;

        return $this;
    }

    /**
     * Get apoyoUnipodalDer
     *
     * @return string
     */
    public function getApoyoUnipodalDer()
    {
        return $this->apoyoUnipodalDer;
    }

    /**
     * Set bajo
     *
     * @param boolean $bajo
     *
     * @return historiaFisioterapia
     */
    public function setBajo($bajo)
    {
        $this->bajo = $bajo;

        return $this;
    }

    /**
     * Get bajo
     *
     * @return bool
     */
    public function getBajo()
    {
        return $this->bajo;
    }

    /**
     * Set medio
     *
     * @param boolean $medio
     *
     * @return historiaFisioterapia
     */
    public function setMedio($medio)
    {
        $this->medio = $medio;

        return $this;
    }

    /**
     * Get medio
     *
     * @return bool
     */
    public function getMedio()
    {
        return $this->medio;
    }

    /**
     * Set alto
     *
     * @param boolean $alto
     *
     * @return historiaFisioterapia
     */
    public function setAlto($alto)
    {
        $this->alto = $alto;

        return $this;
    }

    /**
     * Get alto
     *
     * @return bool
     */
    public function getAlto()
    {
        return $this->alto;
    }

    /**
     * Set muyAlto
     *
     * @param boolean $muyAlto
     *
     * @return historiaFisioterapia
     */
    public function setMuyAlto($muyAlto)
    {
        $this->muyAlto = $muyAlto;

        return $this;
    }

    /**
     * Get muyAlto
     *
     * @return bool
     */
    public function getMuyAlto()
    {
        return $this->muyAlto;
    }

    /**
     * Set prescFrecuencia
     *
     * @param string $prescFrecuencia
     *
     * @return historiaFisioterapia
     */
    public function setPrescFrecuencia($prescFrecuencia)
    {
        $this->prescFrecuencia = $prescFrecuencia;

        return $this;
    }

    /**
     * Get prescFrecuencia
     *
     * @return string
     */
    public function getPrescFrecuencia()
    {
        return $this->prescFrecuencia;
    }

    /**
     * Set prescTiempo
     *
     * @param string $prescTiempo
     *
     * @return historiaFisioterapia
     */
    public function setPrescTiempo($prescTiempo)
    {
        $this->prescTiempo = $prescTiempo;

        return $this;
    }

    /**
     * Get prescTiempo
     *
     * @return string
     */
    public function getPrescTiempo()
    {
        return $this->prescTiempo;
    }

    /**
     * Set entreeCardiovascular
     *
     * @param string $entreeCardiovascular
     *
     * @return historiaFisioterapia
     */
    public function setEntreeCardiovascular($entreeCardiovascular)
    {
        $this->entreeCardiovascular = $entreeCardiovascular;

        return $this;
    }

    /**
     * Get entreeCardiovascular
     *
     * @return string
     */
    public function getEntreeCardiovascular()
    {
        return $this->entreeCardiovascular;
    }

    /**
     * Set movilidadFlexibilidad
     *
     * @param string $movilidadFlexibilidad
     *
     * @return historiaFisioterapia
     */
    public function setMovilidadFlexibilidad($movilidadFlexibilidad)
    {
        $this->movilidadFlexibilidad = $movilidadFlexibilidad;

        return $this;
    }

    /**
     * Get movilidadFlexibilidad
     *
     * @return string
     */
    public function getMovilidadFlexibilidad()
    {
        return $this->movilidadFlexibilidad;
    }

    /**
     * Set calsesGrupales
     *
     * @param string $calsesGrupales
     *
     * @return historiaFisioterapia
     */
    public function setCalsesGrupales($calsesGrupales)
    {
        $this->calsesGrupales = $calsesGrupales;

        return $this;
    }

    /**
     * Get calsesGrupales
     *
     * @return string
     */
    public function getCalsesGrupales()
    {
        return $this->calsesGrupales;
    }

    /**
     * Set entreMuscular
     *
     * @param string $entreMuscular
     *
     * @return historiaFisioterapia
     */
    public function setEntreMuscular($entreMuscular)
    {
        $this->entreMuscular = $entreMuscular;

        return $this;
    }

    /**
     * Get entreMuscular
     *
     * @return string
     */
    public function getEntreMuscular()
    {
        return $this->entreMuscular;
    }

    /**
     * Set conclusiones
     *
     * @param string $conclusiones
     *
     * @return historiaFisioterapia
     */
    public function setConclusiones($conclusiones)
    {
        $this->conclusiones = $conclusiones;

        return $this;
    }

    /**
     * Get conclusiones
     *
     * @return string
     */
    public function getConclusiones()
    {
        return $this->conclusiones;
    }

    /**
     * Set medico
     *
     * @param boolean $medico
     *
     * @return historiaFisioterapia
     */
    public function setMedico($medico)
    {
        $this->medico = $medico;

        return $this;
    }

    /**
     * Get medico
     *
     * @return bool
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * Set fisioterapeuta
     *
     * @param boolean $fisioterapeuta
     *
     * @return historiaFisioterapia
     */
    public function setFisioterapeuta($fisioterapeuta)
    {
        $this->fisioterapeuta = $fisioterapeuta;

        return $this;
    }

    /**
     * Get fisioterapeuta
     *
     * @return bool
     */
    public function getFisioterapeuta()
    {
        return $this->fisioterapeuta;
    }

    /**
     * Set nutricion
     *
     * @param boolean $nutricion
     *
     * @return historiaFisioterapia
     */
    public function setNutricion($nutricion)
    {
        $this->nutricion = $nutricion;

        return $this;
    }

    /**
     * Get nutricion
     *
     * @return bool
     */
    public function getNutricion()
    {
        return $this->nutricion;
    }

    /**
     * Set entrePersonalizado
     *
     * @param boolean $entrePersonalizado
     *
     * @return historiaFisioterapia
     */
    public function setEntrePersonalizado($entrePersonalizado)
    {
        $this->entrePersonalizado = $entrePersonalizado;

        return $this;
    }

    /**
     * Get entrePersonalizado
     *
     * @return bool
     */
    public function getEntrePersonalizado()
    {
        return $this->entrePersonalizado;
    }

    /**
     * Set esteticista
     *
     * @param boolean $esteticista
     *
     * @return historiaFisioterapia
     */
    public function setEsteticista($esteticista)
    {
        $this->esteticista = $esteticista;

        return $this;
    }

    /**
     * Get esteticista
     *
     * @return bool
     */
    public function getEsteticista()
    {
        return $this->esteticista;
    }

    /**
     * Set masajeDeportivo
     *
     * @param boolean $masajeDeportivo
     *
     * @return historiaFisioterapia
     */
    public function setMasajeDeportivo($masajeDeportivo)
    {
        $this->masajeDeportivo = $masajeDeportivo;

        return $this;
    }

    /**
     * Get masajeDeportivo
     *
     * @return bool
     */
    public function getMasajeDeportivo()
    {
        return $this->masajeDeportivo;
    }

    /**
     * Set acondiFuncional
     *
     * @param boolean $acondiFuncional
     *
     * @return historiaFisioterapia
     */
    public function setAcondiFuncional($acondiFuncional)
    {
        $this->acondiFuncional = $acondiFuncional;

        return $this;
    }

    /**
     * Get acondiFuncional
     *
     * @return bool
     */
    public function getAcondiFuncional()
    {
        return $this->acondiFuncional;
    }

    /**
     * Set rehabFuncional
     *
     * @param boolean $rehabFuncional
     *
     * @return historiaFisioterapia
     */
    public function setRehabFuncional($rehabFuncional)
    {
        $this->rehabFuncional = $rehabFuncional;

        return $this;
    }

    /**
     * Get rehabFuncional
     *
     * @return bool
     */
    public function getRehabFuncional()
    {
        return $this->rehabFuncional;
    }

    /**
     * Set paciente
     *
     * @param \AppBundle\Entity\Paciente $paciente
     *
     * @return historiaFisioterapia
     */
    public function setPaciente(\AppBundle\Entity\Paciente $paciente = null)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return \AppBundle\Entity\Paciente
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * Set fechaControl
     *
     * @param \DateTime $fechaControl
     *
     * @return historiaFisioterapia
     */
    public function setFechaControl($fechaControl)
    {
        $this->fechaControl = $fechaControl;

        return $this;
    }

    /**
     * Get fechaControl
     *
     * @return \DateTime
     */
    public function getFechaControl()
    {
        return $this->fechaControl;
    }

    /**
     * Set otro
     *
     * @param string $otro
     *
     * @return historiaFisioterapia
     */
    public function setOtro($otro)
    {
        $this->otro = $otro;

        return $this;
    }

    /**
     * Get otro
     *
     * @return string
     */
    public function getOtro()
    {
        return $this->otro;
    }

    /**
     * Add notasdevolucion
     *
     * @param \AppBundle\Entity\Notadevolucionfisio $notasdevolucion
     *
     * @return historiaFisioterapia
     */
    public function addNotasdevolucion(\AppBundle\Entity\Notadevolucionfisio $notasdevolucion)
    {
        $this->notasdevolucion[] = $notasdevolucion;

        return $this;
    }

    /**
     * Remove notasdevolucion
     *
     * @param \AppBundle\Entity\Notadevolucionfisio $notasdevolucion
     */
    public function removeNotasdevolucion(\AppBundle\Entity\Notadevolucionfisio $notasdevolucion)
    {
        $this->notasdevolucion->removeElement($notasdevolucion);
    }

    /**
     * Get notasdevolucion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotasdevolucion()
    {
        return $this->notasdevolucion;
    }

    public function __toString()
    {
        return $this->consecutivo;
    }
}
