<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * historialRecargas
 *
 * @ORM\Table(name="historial_recargas")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\historialRecargasRepository")
 */
class historialRecargas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="valorRecarga", type="integer")
     */
    private $valorRecarga;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="paciente", type="string", length=255)
     */
    private $paciente;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valorRecarga
     *
     * @param integer $valorRecarga
     *
     * @return historialRecargas
     */
    public function setValorRecarga($valorRecarga)
    {
        $this->valorRecarga = $valorRecarga;

        return $this;
    }

    /**
     * Get valorRecarga
     *
     * @return int
     */
    public function getValorRecarga()
    {
        return $this->valorRecarga;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return historialRecargas
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set paciente
     *
     * @param string $paciente
     *
     * @return historialRecargas
     */
    public function setPaciente($paciente)
    {
        $this->paciente = $paciente;

        return $this;
    }

    /**
     * Get paciente
     *
     * @return string
     */
    public function getPaciente()
    {
        return $this->paciente;
    }
}
