<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * historiasfnumber
 *
 * @ORM\Table(name="historiasfnumber")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\historiasfnumberRepository")
 */
class historiasfnumber
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="historian", type="string", length=255)
     */
    private $historian;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set historian
     *
     * @param string $historian
     *
     * @return historiasfnumber
     */
    public function setHistorian($historian)
    {
        $this->historian = $historian;

        return $this;
    }

    /**
     * Get historian
     *
     * @return string
     */
    public function getHistorian()
    {
        return $this->historian;
    }
}
