<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * historiasnumber
 *
 * @ORM\Table(name="historiasnumber")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\historiasnumberRepository")
 */
class historiasnumber
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="historian", type="string", length=255)
     */
    private $historian;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set historian
     *
     * @param string $historian
     *
     * @return historiasnumber
     */
    public function setHistorian($historian)
    {
        $this->historian = $historian;

        return $this;
    }

    /**
     * Get historian
     *
     * @return string
     */
    public function getHistorian()
    {
        return $this->historian;
    }
}
