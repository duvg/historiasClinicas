<?php
/**
 * Created by PhpStorm.
 * User: duvg
 * Date: 6/12/17
 * Time: 06:24 AM
 */

namespace AppBundle\EventListener;

use AppBundle\Controller\ClientesController;
use AppBundle\Controller\PacientesController;
use AppBundle\Entity\Arl;
use AppBundle\Entity\Empleado;
use AppBundle\Entity\Encuesta;
use AppBundle\Entity\Eps;
use AppBundle\Entity\historiasnumber;
use AppBundle\Entity\Items;
use AppBundle\Entity\Municipio;
use AppBundle\Entity\Plan;
use AppBundle\Entity\Post;
use AppBundle\Entity\Pqrs;
use AppBundle\Entity\Proveedor;
use AppBundle\Entity\SolicitudServicio;
use AppBundle\Entity\TipoItem;
use AppBundle\Entity\TipoPqrs;
use AppBundle\Entity\TipoUsuario;
use AppBundle\Entity\User;
use AppBundle\Repository\TratamientoRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use AppBundle\Entity\Servicio;
use AppBundle\Entity\Test;
use AppBundle\Service\FileUploader;

class BrochureUploadListener
{
    private $uploader;


    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public  function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if($entity instanceof Servicio )
        {
            return;
        }
        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        if($entity instanceof Servicio)
        {
            return;
        }

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {

        if(($entity instanceof Servicio)
            or ($entity instanceof Afiliado)
            or ($entity instanceof Arl)
            or ($entity instanceof Cargo)
            or ($entity instanceof Cliente)
            or ($entity instanceof Efp)
            or ($entity instanceof Encuesta)
            or ($entity instanceof Empleado)
            or ($entity instanceof Eps)
            or ($entity instanceof Historia)
            or ($entity instanceof historiasnumber)
            or ($entity instanceof Items)
            or ($entity instanceof Municipio)
            or ($entity instanceof Paciente)
            or ($entity instanceof Plan)
            or ($entity instanceof Pqrs)
            or ($entity instanceof Proveedor)
            or ($entity instanceof SolicitudServicio)
            or ($entity instanceof Test)
            or ($entity instanceof TipoItem)
            or ($entity instanceof TipoPqrs)
            or ($entity instanceof TipoUsuario)
            or ($entity instanceof User)
            or ($entity instanceof Post)
            or ($entity instanceof Tratamiento)
            )
        {
            return;
        }
        if(!($entity instanceof Servicio)
            or !($entity instanceof Afiliado)
            or !($entity instanceof Arl)
            or !($entity instanceof Cargo)
            or !($entity instanceof Cliente)
            or !($entity instanceof Efp)
            or !($entity instanceof Encuesta)
            or !($entity instanceof Empleado)
            or !($entity instanceof Eps)
            or !($entity instanceof Historia)
            or !($entity instanceof historiasnumber)
            or !($entity instanceof Items)
            or !($entity instanceof Municipio)
            or !($entity instanceof Paciente)
            or !($entity instanceof Plan)
            or !($entity instanceof Pqrs)
            or !($entity instanceof Proveedor)
            or !($entity instanceof SolicitudServicio)
            or !($entity instanceof Test)
            or !($entity instanceof TipoItem)
            or !($entity instanceof TipoPqrs)
            or !($entity instanceof TipoUsuario)
            or !($entity instanceof User)
            or !($entity instanceof Post)
            or ($entity instanceof Tratamiento)
        )
        {
            return;
        }

        $file = $entity->getImagen();

        if($file instanceof UploadedFile)
        {
            $filename = $this->uploader->upload($file);

            $entity->setImagen($filename);
        }
    }

}