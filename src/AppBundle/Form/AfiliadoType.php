<?php

namespace AppBundle\Form;

use AppBundle\Entity\Departamento;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;


class AfiliadoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoDocumento', ChoiceType::class, array(
                'choices' => array(
                    'Registro Civil' => 'RC',
                    'Tarjeta de Identidad'  => 'TI' ,
                    'Cedula de Ciudadania' => 'CC' ,
                    'Cedula de extrajeria' => 'CE',
                )
            ))
            ->add('ndocumento')
            ->add('nombre')
            ->add('apellidos')
            ->add('email')
            ->add('direccion')
            ->add('telefono')
            ->add('plan', EntityType::class, array(
                'class' => 'AppBundle:Planes',
                'placeholder' => 'Seleccione un plans',
            ))
            ->add('sexo', ChoiceType::class, array(
                'choices' => array(
                    '' => null,
                    'Masculino' => 'M',
                    'Femenino'  => 'F',
                )
            ))
            ->add('edad')
            ->add('fechaNacimiento',DatetimeType::class, array(
                'widget' => 'single_text',
                'input'  => 'datetime',
                'format' => 'dd/MM/yyyy kk:mm',
                'html5'  => false

            ))
            ->add('usuario', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'placeholder' => '',
                'label' => 'Usuario'
            ))
            ->add('departamento', EntityType::class, array(
                'class' => 'AppBundle:Departamento',
                'placeholder' => '',

            ))
            ->add('guardar', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary', 'tabindex' => '13')
            ))
            ->add('reset', ResetType::class, array(
                'attr' => array('class' => 'btn btn-warning', 'tabindex' => '13')
            ));

        $formModifier = function (FormInterface $form, Departamento $departamento = null){
            $municipios = null === $departamento ? array() : $departamento->getMunicipios();

            $form->add('municipio', EntityType::class, array(
                'class' => 'AppBundle\Entity\Municipio',
                'placeholder' => '',
                'choices' => $municipios,
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier){
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getDepartamento());
            }
        );

        $builder->get('departamento')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier){
                $departamento = $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $departamento);
            }
        );




    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Afiliado'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_afiliado';
    }


}
