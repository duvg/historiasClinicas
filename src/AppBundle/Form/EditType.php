<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cedula')
            ->add('nombre')
            ->add('apellidos')
            ->add('telefono')

            

            ->add('roles',  ChoiceType::class, array(
                'choices' => array(
                    'Usuario' => 'ROLE_USER',
                    'Recepcionista' => 'ROLE_RECEPCION',
                    'Medico' => 'ROLE_MEDIC',
                    'Cafeteria' => 'ROLE_CAFE',
                    'Fisioterapeuta' => 'ROLE_FISIOTERAPY',
                    'Administrador' => 'ROLE_ADMIN' 
                ),
                'label' => 'Roles',
                'expanded' => true,
                'multiple' => true,
                'mapped' => true,
                'attr' => array(
                    'class' => 'boxeds'
                )
                
            ))
            ->add('guardar', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary', 'tabindex' => '13')
            ));

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}