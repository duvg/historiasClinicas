<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EncuestaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellidos')
            ->add('email')
            ->add('telefono')
            ->add('respuesta1', TextareaType::class, array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 8
                )
            ))
            ->add('respuesta2', TextareaType::class, array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 8
                )
            ))
            ->add('respuesta3',TextareaType::class, array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 8
                )
            ))
            ->add('respuesta4', ChoiceType::class, array(
                'choices' => array(
                    'excelente' => 'Excelente',
                    'bueno' => 'Bueno',
                    'malo' => 'Malo',
                )
            ))
            ->add('respuesta5', ChoiceType::class, array(
                'choices' => array(
                    'definitivamente_si' => 'Definitivamente si',
                    'si' => 'Si',
                    'no' => 'No',
                    'definitivamente_no' => 'Definitivamente no'

                )
            ))
            ->add('enviar', SubmitType::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Encuesta'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_encuesta';
    }


}
