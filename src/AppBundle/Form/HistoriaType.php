<?php

namespace AppBundle\Form;

use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;


class HistoriaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('consecutivo', null, array(
                'label' => 'Historia N°',
                'attr' => array(
                    'readonly'=>true
                )
            ))
            ->add('paciente', EntityType::class, array(
                'class' => 'AppBundle\Entity\Paciente',
                'label' => 'Paciente'
            ))
            ->add('motivoconsulta')
            ->add('enfermedadactual')
            ->add('antalcohol',ChoiceType::class, array(
                'label' => 'Alcohol',
                'choices' => array(
                    'Si' => '1',
                    'No' => '0',
                ),
                'multiple' => false,
                'expanded' => 'true'
            ))
            ->add('antcigarrillos',ChoiceType::class, array(
                'label' => 'Cigarrillos',
                'choices' => array(
                    'Si' => '1',
                    'No' => '0',
                ),
                'multiple' => false,
                'expanded' => 'true'
            ))
            ->add('otros')


            ->add('patologicos',null, array(
                'label' => 'Patologicos'
            ))
            ->add('ginecologicos', ChoiceType::class, array(
                'choices' => array(
                    '' => null,
                    'G' => 'G',
                    'P' => 'P',
                    'C' => 'C',
                    'A' => 'A',
                    'V' => 'V'
                ))
            )
            ->add('quirurgicos',null, array(
                'label' => 'Quirurgicos'
            ))
            ->add('alergicos',null, array(
                'label' => 'Alergicos'
            ))
            ->add('menarquia',DatetimeType::class, array(
                'widget' => 'single_text',
                'input'  => 'datetime',
                'format' => 'dd/MM/yyyy kk:mm',
                'html5'  => false

            ))

            ->add('fechaControl',DatetimeType::class, array(
                'widget' => 'single_text',
                'input'  => 'datetime',
                'format' => 'dd/MM/yyyy kk:mm',
                'html5'  => false

            ))

            ->add('fum',DatetimeType::class, array(
                'widget' => 'single_text',
                'input'  => 'datetime',
                'format' => 'dd/MM/yyyy kk:mm',
                'html5'  => false

            ))
            ->add('tA',DatetimeType::class, array(
                'widget' => 'single_text',
                'input'  => 'datetime',
                'format' => 'dd/MM/yyyy kk:mm',
                'html5'  => false

            ))
            ->add('fcardiaca',null, array(
                'label' => 'FC'
            ))
            ->add('frespiratoria',null, array(
                'label' => 'FR'
            ))
            ->add('temperaturac',null, array(
                'label' => 'T°C'
            ))
            ->add('peso',TextType::class, array(
                'label' => 'Peso'
            ))
            ->add('talla',TextType::class, array(
                'label' => 'Talla'
            ))
            ->add('imc',TextType::class, array(
                'label' => 'IMC'
            ))
            ->add('estadogeneral',null, array(
                'label' => 'Estado General'
            ))
            ->add('cabeza',null, array(
                'label' => 'Cabeza'
            ))
            ->add('ojos',null, array(
                'label' => 'Ojos'
            ))
            ->add('boca',null, array(
                'label' => 'Boca'
            ))
            ->add('cuello',null, array(
                'label' => 'Cuello'
            ))
            ->add('torax',null, array(
                'label' => 'Torax'
            ))
            ->add('cardiovascular',null, array(
                'label' => 'Cardiovascular'
            ))
            ->add('abdomen',null, array(
                'label' => 'Abdomen'
            ))
            ->add('extremidades',null, array(
                'label' => 'Extremidades'
            ))
            ->add('gU',null, array(
                'label' => 'G/U'
            ))
            ->add('neurologico',null, array(
                'label' => 'Neurologico'
            ))
            ->add('iDX1', EntityType::class, array(
                'class' => 'AppBundle\Entity\Cie10',
                'label' => 'IDX1'
            ))
            ->add('iDX2',EntityType::class, array(
                'class' => 'AppBundle\Entity\Cie10',
                'label' => 'IDX2'
            ))
            ->add('plantratamiento',null, array(
                'label' => 'Plan de TipoTratamiento'
            ))








            ->add('guardar', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary', 'tabindex' => '13')
            ))
            ->add('reset', ResetType::class, array(
                'attr' => array('class' => 'btn btn-warning', 'tabindex' => '13')
            ));


    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Historia'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_historias';
    }


}
