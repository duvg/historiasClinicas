<?php

namespace AppBundle\Form;

use AppBundle\Entity\Departamento;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;


class PqrsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder
            ->add('nombre')
            ->add('tipoDocuemnto',ChoiceType::class, array(
                'choices' => array(
                    ''   => 'Seleccione...',
                    'RI' => 'Registro Civil',
                    'TI' => 'Tarjeta de Identidad',
                    'CC' => 'CC'
                )
            ))
            ->add('nidenficacion')
            ->add('afiliado', ChoiceType::class, array(
                'choices' => array(
                    'Si' => 1,
                    'No' => 0
                ),
                'label' => 'Esta afiliado',
                'required' => true,
                'choices_as_values' => true,
                'expanded' => true
            ))
            ->add('email')
            ->add('direccion')
            ->add('telefono')
            ->add('celular')
            /*
            ->add('tipodocumentoAfe', 'choice', array(
                'choices' => array(
                    'RI' => 'Registro Civil',
                    'TI' => 'Tarjeta de Identidad',
                    'CC' => 'CC'
                )
            ))
            ->add('ndocumentoAfe')
            ->add('nombreAfe')
            ->add('direccionAfe')
            ->add('telefonoAfe')*/
            ->add('lugarServicio')
            ->add('funcionario')
            ->add('asunto')
            ->add('mensaje', TextareaType::class,array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 7,
                    'placeholder' => 'Mensaje que deseas enviar a casamayorga'
                )
            ))
            ->add('fecha', DateType::class, array(
                'widget' => 'single_text'
            ))
            ->add('departamento', EntityType::class, array(
                'class' => 'AppBundle:Departamento',
                'placeholder' => '',

            ))
            ->add('tiposolicitud')
            ->add('enviar', SubmitType::class);

        $formModifier = function (FormInterface $form, Departamento $departamento = null){
            $municipios = null === $departamento ? array() : $departamento->getMunicipios();

            $form->add('municipio', EntityType::class, array(
                'class' => 'AppBundle\Entity\Municipio',
                'placeholder' => '',
                'choices' => $municipios,
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier){
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getDepartamento());
            }
        );

        $builder->get('departamento')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier){
                $departamento = $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $departamento);
            }
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Pqrs'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_pqrs';
    }


}
