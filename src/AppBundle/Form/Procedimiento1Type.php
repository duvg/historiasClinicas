<?php

namespace AppBundle\Form;

use AppBundle\Entity\Departamento;
use AppBundle\Entity\TipoTratamiento;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;


class Procedimiento1Type extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('paciente', EntityType::class, array(
                'class' => 'AppBundle:Paciente',
                'placeholder' => '',

            ))
            ->add('tipoTratamiento', EntityType::class, array(
                'class' => 'AppBundle:TipoTratamiento',
                'placeholder' => '',

            ))

            ->add('tratamiento', EntityType::class, array(
                'class' => 'AppBundle:Tratamiento',
                'placeholder' => '',

            ))
            ->add('notas', TextareaType::class, array(
                'attr' => array(
                    'cols' => 90,
                    'rows' => 10,
                    'placeholder' => 'Notas'
                )
            ))
            ->add('guardar', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary', 'tabindex' => '13')
            ))
            ->add('reset', ResetType::class, array(
                'attr' => array('class' => 'btn btn-warning', 'tabindex' => '13')
            ));








    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Procedimiento'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_procedimiento';
    }


}
