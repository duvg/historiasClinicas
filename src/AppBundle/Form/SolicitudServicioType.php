<?php
/**
 * Created by PhpStorm.
 * User: duvg
 * Date: 4/12/17
 * Time: 11:12 PM
 */

namespace AppBundle\Form;

use AppBundle\Entity\Departamento;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;


class SolicitudServicioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('servicio', EntityType::class, array(
                'label' => 'Nombre del Servicio',
                'class' => 'AppBundle\Entity\Servicio'

            ))
            ->add('cantidad', null, array(
                'label' => 'Cantidad'
            ))
            /*
            ->add('usuario', EntityType::class, array(
                'class' => 'AppBundle\Entity\User',
                'data' => 2
            ))*/
            ->add('guardar', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary', 'tabindex' => '13')
            ))
            ->add('reset', ResetType::class, array(
                'attr' => array('class' => 'btn btn-warning', 'tabindex' => '13')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SolicitudServicio'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_solicitudservicio';
    }

}