<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class historiaFisioterapiaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('consecutivo')
            ->add('nombreResponsable', null, array(
                'label' => 'Nombre del Responsable'
            ))
            ->add('telefonoResponsable', null, array(
                'label' => 'Telefono Responsable'
            ))
            ->add('eapb', null, array(
                'label' => 'EAPB'
            ))
            ->add('prepagada', null, array(
                'label' => 'PREPAGADA'
            ))
            ->add('planComplementarios', null, array(
                'label' => 'PLAN COMPLEMENTARIOS'
            ))
            ->add('aseguradoras', null, array(
                'label' => 'ASEGURADORAS'
            ))
            ->add('postquirurgico', null, array(
                'label' => 'POST QUIRURGICOS'
            ))
            ->add('estetico', null, array(
                'label' => 'ESTETICOS'
            ))
            ->add('tonificar', null, array(
                'label' => 'TONIFICAR'
            ))
            ->add('salud', null, array(
                'label' => 'SALUD'
            ))
            ->add('deportivo', null, array(
                'label' => 'DEPORTIVO'
            ))
            ->add('rehabilitacion', null, array(
                'label' => 'REHABILITACIÓN'
            ))
            ->add('patologicos', null, array(
                'label' => 'PATOLOGICOS'
            ))
            ->add('quirurgicos', null, array(
                'label' => 'QUIRURGICOS'
            ))
            ->add('gineFum',null, array(
                'label' => 'Fum'
            ))
            ->add('gineG',null, array(
                'label' => 'G'
            ))
            ->add('gineP',null, array(
                'label' => 'P'
            ))
            ->add('gineC',null, array(
                'label' => 'C'
            ))
            ->add('gineV',null, array(
                'label' => 'V'
            ))
            ->add('gineA',null, array(
                'label' => 'A'
            ))
            ->add('farmacologicos', null, array(
                'label' => 'FARMACOLÓGICOS'
            ))
            ->add('osteomusculares', null, array(
                'label' => 'OSTEOMUSCULARES'
            ))
            ->add('antecedentesfamiliates', null, array(
                'label' => 'ANTECEDENTES FAMILIARES'
            ))
            ->add('sedentario', null, array(
                'label' => 'SEDENTARIO'
            ))
            ->add('tipoEjercicio', null, array(
                'label' => 'EJERCICIO'
            ))
            ->add('frEjercicio', null, array(
                'label' => 'FR EJERCICIO'
            ))
            ->add('duracionEjercicio' , null, array(
                'label' => 'FURACION EJ'
            ))
            ->add('intensidadEjercicio' , null, array(
                'label' => 'INTENSIDAD EJ'
            ))
            ->add('tipoDeporte', null, array(
                'label' => 'DEPORTE'
            ))
            ->add('frDeporte', null, array(
                'label' => 'FR DEPORTE'
            ))
            ->add('duracionDeporte', null, array(
                'label' => 'DURACION DEP'
            ))
            ->add('intensidadDeporte', null, array(
                'label' => 'INTENSIDAD DEP'
            ))
            ->add('pregunta1chk', null, array(
                'label' => '¿Ha sentido dolor en el pecho cuando realiza actividad fisica?'
                
            ))
            ->add('pregunta1rp',null,  array(
                'label' => ''
            ))
            ->add('pregunta2chk',null,  array(
                'label' => '¿Ha perdido el balance por causa de desvanecimiento o ha perdido el conocimiento?'   
            ))
            ->add('pregunta2rp',null,  array(
                'label' => ''
            ))
            ->add('pregunta3chk',null,  array(
                'label' => '¿Ha sentido plapitaciones o ahogo excesivo aun en reposo o con actividades leves?'   
            ))
            ->add('pregunta3rp',null,  array(
                'label' => ''
            ))
            ->add('pregunta4chk',null,  array(
                'label' => '¿Ha presentado problemas articulares u óseos que se empeoren con la actividad fisica?'   
            ))
            ->add('pregunta4rp',null,  array(
                'label' => ''
            ))
            ->add('pregunta5chk',null,  array(
                'label' => '¿Usted conoce alguna razón por la que no pudiera realizar actividad fisica?'   
            ))
            ->add('pregunta5rp',null,  array(
                'label' => ''
            ))
            ->add('pregunta6chk',null,  array(
                'label' => 'Observaciones de las respuestas afirmativas'   
            ))
            ->add('pregunta6rp',null,  array(
                'label' => ''
            ))
            ->add('tensionArterial',null,  array(
                'label' => 'TENSIÓN ARTERIAL'
            ))
            ->add('frecuenciaCardiaca',null,  array(
                'label' => 'FRECUENCIA CARDICA'
            ))
            ->add('talla',null,  array(
                'label' => 'TALLA'
            ))
            ->add('pesoKg',null,  array(
                'label' => 'PESO KG'
            ))
            ->add('indiceMasaCorporal',null,  array(
                'label' => 'INDICE MASA CORPORTAL'
            ))
            ->add('perimetroAbdominal',null,  array(
                'label' => 'PERIMETRO ABDOMINAL'
            ))
            ->add('grasa',null,  array(
                'label' => 'GRASO'
            ))
            ->add('muscular',null,  array(
                'label' => 'MUSCULAR'
            ))
            ->add('se',null,  array(
                'label' => 'SE'
            ))
            ->add('t',null,  array(
                'label' => 'T'
            ))
            ->add('si',null,  array(
                'label' => 'SI'
            ))
            ->add('abd',null,  array(
                'label' => 'ADB'
            ))
            ->add('m',null,  array(
                'label' => 'M'
            ))
            ->add('p',null,  array(
                'label' => 'P'
            ))
            ->add('brder',null,  array(
                'label' => 'BR. DER'
            ))
            ->add('brizq',null,  array(
                'label' => 'BR. IZQ'
            ))
            ->add('pec',null,  array(
                'label' => 'PEC'
            ))
            ->add('cint',null,  array(
                'label' => 'CINT'
            ))
            ->add('cad',null,  array(
                'label' => 'CAD'
            ))
            ->add('musder',null,  array(
                'label' => 'MUS DER'
            ))
            ->add('musizq',null,  array(
                'label' => 'MUS IZQ'
            ))
            ->add('piernader',null,  array(
                'label' => 'PERNA DER'
            ))
            ->add('piernaizq',null,  array(
                'label' => 'PIERNA IZQ'
            ))
            ->add('obesidad',null,  array(
                'label' => 'OBESIDAD'
            ))
            ->add('tabaquismo',null,  array(
                'label' => 'TABAQUISMO'
            ))
            ->add('hipertension',null,  array(
                'label' => 'HIERTENSIÓN'
            ))
            ->add('prediabetes',null,  array(
                'label' => 'PREDIABETES O DIABETES'
            ))
            ->add('dislipidemia',null,  array(
                'label' => 'DISLIPIDEMIA'
            ))
            ->add('antecedentesFamiliares',null,  array(
                'label' => 'ANTECEDENTES FAMILIARES'
            ))
            ->add('sedentarismo',null,  array(
                'label' => 'SEDENTARISMO'
            ))
            ->add('a1',null, array(
                'label' => 'A1'
            ))
            ->add('a2',null, array(
                'label' => 'A2'
            ))
            ->add('a3',null, array(
                'label' => 'A3'
            ))
            ->add('b',null, array(
                'label' => 'B'
            ))
            ->add('c',null, array(
                'label' => 'C'
            ))
            ->add('d',null, array(
                'label' => 'D'
            ))
            ->add('antepulsionCabeza',null,  array(
                'label' => 'ANTEPULSIÓN CABEZA'
            ))
            ->add('rectificacionCervical',null,  array(
                'label' => 'RECTIFICACIÓN CERVICAL'
            ))
            ->add('hiperlordosisCervical',null,  array(
                'label' => 'HIPERLORDOSIS CERVICAL'
            ))
            ->add('hipercifosis',null,  array(
                'label' => 'HIPERCIFOSIS'
            ))
            ->add('rectificacionDorsal',null,  array(
                'label' => 'RECTIFICACIÓN DORSAL'
            ))
            ->add('hiperlordosisLumbar',null,  array(
                'label' => 'HIPERLORDOSIS LUMBAR'
            ))
            ->add('rectificacionLumbar',null,  array(
                'label' => 'RECTIFICACIÓN LUMBAR '
            ))
            ->add('escoliosis',null,  array(
                'label' => 'ESCOLIOSIS'
            ))
            ->add('anteropulsionHombros',null,  array(
                'label' => 'ANTEROPULSION HOMBROS'
            ))
            ->add('hombroAscendido',null,  array(
                'label' => 'HOMBRO ASCENDIDO'
            ))
            ->add('hiperextensionCodo',null,  array(
                'label' => 'HIPEREXTENSIÓN CODO'
            ))
            ->add('aumentoValgoCodo',null,  array(
                'label' => 'AUMENTO VALGO EN CODO'
            ))
            ->add('anteversionFemoral',null,  array(
                'label' => 'ANTEVERSION FEMORAL'
            ))
            ->add('genuValgoNoFisiologico',null,  array(
                'label' => 'GENU VALGO NO FISIOLOGICO'
            ))
            ->add('genuVaroNoFisiologico',null,  array(
                'label' => 'GENU VARO NO FISIOLOGICO '
            ))
            ->add('genuRecurvatum',null,  array(
                'label' => 'GENU RECURVATUM'
            ))
            ->add('rotacionTibial',null,  array(
                'label' => 'ROTACIÓN TIBIAL'
            ))
            ->add('piePlano',null,  array(
                'label' => 'PIE PLANO'
            ))
            ->add('pieCavo',null,  array(
                'label' => 'PIE CAVO'
            ))
            ->add('analisisPostura',null,  array(
                'label' => false
            ))
            ->add('sentadillaProfunda',ChoiceType::class,  array(
                'label' => 'SENTADILLA PROFUNDA',
                'choices' => array(
                    'SL' => true,
                    'NSL' => false
                ),
                'expanded' => true
            ))
            ->add('bisagra',ChoiceType::class,  array(
                'label' => 'BISAGRA',
                'choices' => array(
                    'SL' => true,
                    'NSL' => false
                ),
                'expanded' => true
                
            ))
            ->add('cadenaCineticaSuperior',ChoiceType::class,  array(
                'label' => 'CADENA CINETICA SUPERIOR',
                'choices' => array(
                    'SL' => true,
                    'NSL' => false
                ),
                'expanded' => true
            ))
            ->add('abdomen',ChoiceType::class,  array(
                'label' => 'ABDOMEN',
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'expanded' => true
            ))
            ->add('sentadilla',ChoiceType::class,  array(
                'label' => 'SENTADILLA',
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'expanded' => true
            ))
            ->add('cuadrupizq',ChoiceType::class,  array(
                'label' => 'CUADUP. IZQ',
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'expanded' => true
            ))
            ->add('cuadrupder',ChoiceType::class,  array(
                'label' => 'CUADRUP. DER',
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'expanded' => true
            ))
            ->add('apoyoUnipodalIzq',ChoiceType::class,  array(
                'label' => 'APOYO UNIPODAL IZQ',
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'expanded' => true
            ))
            ->add('apoyoUnipodalDer',ChoiceType::class,  array(
                'label' => 'APOYO UNIPODAL DER',
                'choices' => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3'
                ),
                'expanded' => true
            ))
            ->add('bajo',null,  array(
                'label' => 'BAJO'
            ))
            ->add('medio',null,  array(
                'label' => 'MEDIO'
            ))
            ->add('alto',null,  array(
                'label' => 'ALTO'
            ))
            ->add('muyAlto',null,  array(
                'label' => 'MUY ALTO'
            ))
            ->add('prescFrecuencia',null,  array(
                'label' => 'FRECUENCIA'
            ))
            ->add('prescTiempo',null,  array(
                'label' => 'TIEMPO'
            ))
            ->add('entreeCardiovascular',null,  array(
                'label' => 'ENTRENAMIENTO CARDIOVASCULAR'
            ))
            ->add('movilidadFlexibilidad',null,  array(
                'label' => 'MOVILIDAD Y FLEXIBILIDAD'
            ))
            ->add('calsesGrupales',null,  array(
                'label' => 'CLASES GRUPALES'
            ))
            ->add('entreMuscular',null,  array(
                'label' => 'ENTRENAMIENTO MUSCULAR'
            ))
            ->add('conclusiones',null,  array(
                'label' => 'CONCLUSIONES'
            ))
            ->add('medico',null,  array(
                'label' => 'MEDICO'
            ))
            ->add('fisioterapeuta',null,  array(
                'label' => 'FISIOTERAPIA'
            ))
            ->add('nutricion',null,  array(
                'label' => 'NUTRICION'
            ))
            ->add('entrePersonalizado',null,  array(
                'label' => 'ENTRENAMIENTO PERSONALIZADO'
            ))
            ->add('esteticista',null,  array(
                'label' => 'ESTETICISTA'
            ))
            ->add('masajeDeportivo',null,  array(
                'label' => 'MASAJE DEPORTIVO'
            ))
            ->add('acondiFuncional',null,  array(
                'label' => 'ACONDICIONAMIENTO FUNCIONAL'
            ))
            ->add('rehabFuncional',null,  array(
                'label' => 'REHABILITACION FUNCIONAL'
            ))
            ->add('otro')
            ->add('fechaControl',DatetimeType::class, array(
                'widget' => 'single_text',
                'input'  => 'datetime',
                'format' => 'dd/MM/yyyy kk:mm',
                'html5'  => false

            ))
            ->add('paciente')
            ->add('guardar', SubmitType::class, array(
                'attr' => array('class' => 'btn btn-primary', 'tabindex' => '13')
            ))
            ->add('reset', ResetType::class, array(
                'attr' => array('class' => 'btn btn-warning', 'tabindex' => '13')
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\historiaFisioterapia'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_historiafisioterapia';
    }


}
