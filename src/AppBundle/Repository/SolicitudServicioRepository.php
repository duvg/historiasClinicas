<?php

namespace AppBundle\Repository;

/**
 * ServiciosRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SolicitudServicioRepository extends \Doctrine\ORM\EntityRepository
{
    public function findServiciosEnabled()
    {
        $em = $this->getEntityManager();

        $dql = "SELECT s
                FROM AppBundle:SolicitudServicio s
                WHERE s.enabled = true
            ";
        $consulta = $em->createQuery($dql);
        return $consulta->getResult();
    }

    //retorna la cantidad de los registros en la base de datos
    public function getCantidadSolicitudes()
    {
        $em = $this->getEntityManager();

        $dql = "SELECT count(a.id)
                FROM AppBundle:SolicitudServicio a";
        $consulta = $em->createQuery($dql)
            ->getSingleScalarResult();
        return $consulta;

    }
}
