<?php

namespace ShoppingCartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * asdadasd
     */
    public function indexAction()
    {
        return $this->render('ShoppingCartBundle:Default:index.html.twig');
    }
}
